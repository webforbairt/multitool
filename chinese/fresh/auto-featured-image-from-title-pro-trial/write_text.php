<?php
        $auto_image_top_bottom_padding = $auto_image_top_padding + $auto_image_bottom_padding;
        $auto_image_left_right_padding = $auto_image_left_padding + $auto_image_right_padding;
        // Transform the text according to the options
        if($auto_image_text_transform == 'uppercase'){
            $auto_image_transformed_post_text = strtoupper($auto_image_text_to_write);
            }
        elseif($auto_image_text_transform == 'lowercase'){
            $auto_image_transformed_post_text = strtolower($auto_image_text_to_write);
            }
        elseif($auto_image_text_transform == 'capitalize'){
            $auto_image_transformed_post_text = ucwords($auto_image_text_to_write);
            }
        else{
            $auto_image_transformed_post_text = $auto_image_text_to_write;
            }

        $auto_image_transformed_post_text = str_replace('  ', ' ', $auto_image_transformed_post_text);
        $auto_image_transformed_post_text = str_replace('&#160;',' ',$auto_image_transformed_post_text);
        $auto_image_transformed_post_text = str_replace(' ',' ',$auto_image_transformed_post_text);

  		$auto_image_transformed_post_text = str_replace("\r", '', $auto_image_transformed_post_text);
  		$auto_image_transformed_post_text = str_replace('&#13;', '', $auto_image_transformed_post_text);
		if($auto_image_remove_linebreaks == 'yes'){
			$auto_image_transformed_post_text = str_replace("\n", ' ', $auto_image_transformed_post_text);
			}
		else{
			$auto_image_transformed_post_text = str_replace("+", ' #10;', $auto_image_transformed_post_text);
			}
        $words = explode(" ", $auto_image_transformed_post_text);

//    imagealphablending($new_featured_img, true);
//    imagesavealpha($new_featured_img, true);
//    $trans_layer_overlay = imagecolorallocatealpha($new_featured_img, 220, 220, 220, 127);
//    imagefill($new_featured_img, 0, 0, $trans_layer_overlay);

        $auto_image_fontsize = $auto_image_fontsize + 3;

        do {
            $auto_image_fontsize = $auto_image_fontsize - 3;

		    // Unset variables if this is a subsequent attempt at writing the text
            if(isset($auto_image_text_x)){
                unset($auto_image_text_x);
                unset($auto_image_text_xx);
                unset($auto_image_text_y);
                unset($row);
                }

            // Position the text (the whole string)
            $auto_image_text_array = imagettfbbox($auto_image_fontsize, 0, $font, $auto_image_transformed_post_text);

            if($auto_image_text_x_position == 'left'){
                $auto_image_text_x[] = 0;
                $auto_image_text_xx[] = $auto_image_text_array[2];
                }
            elseif($auto_image_text_x_position == 'right'){
                $auto_image_text_x[] = $auto_image_width - $auto_image_text_array[2];
                $auto_image_text_xx[] = $auto_image_text_array[2];
                }
            else{
                $auto_image_text_x[] = ($auto_image_width - $auto_image_text_array[2]) / 2;
                $auto_image_text_xx[] = $auto_image_text_array[2];
                }

            $auto_image_text_y[] = abs($auto_image_text_array[5]);

		    $string = '';
            $tmp_string = '';
		    $before_break = '';
		    $after_break = '';

            $auto_image_text_array['height'] = abs($auto_image_text_array[7]) - abs($auto_image_text_array[1]);
            if($auto_image_text_array[3] > 0) {
                $auto_image_text_array['height'] = abs($auto_image_text_array[7] - $auto_image_text_array[1]) - 1;
                }
            $lineheight = $auto_image_text_array['height'] + 10;

            $ny = 0;
			for($i = 0; $i < count($words) || $before_break != ''; $i++) {

			    if($before_break != ''){
				    $tmp_string = $after_break;
				    $before_break = '';
                    }

			    // Add a word to the tmp string
		  		if($i>=count($words)){
				    $words[$i] = '';
				    }
                $tmp_string .= $words[$i]." ";

			    // Remove a line break if it begins the string
                if(substr($tmp_string, 0, 4) == '#10;'){
                    $tmp_string = substr($tmp_string, 4);
                    }

                // Check width of the last string to see if it fits within image
                $dim = imagettfbbox($auto_image_fontsize, 0, $font, rtrim($tmp_string));
				if (strpos($tmp_string, '#10;') !== false) {
					 $dim = imagettfbbox($auto_image_fontsize, 0, $font, rtrim($tmp_string_author_str));
				}
                // Check to see if there is a line break in the tmp string
                $before_break = strstr($tmp_string, '#10;', true);
                $after_break = strstr($tmp_string, '#10;');

				if($dim[4] < ($auto_image_width-$auto_image_left_right_padding)) {
				  //				if($dim[4] < ($auto_image_width)) {
                    // If it fits, save it as a row
			        if($before_break != ''){
				        $string = rtrim($before_break);
                        $row[$ny] = rtrim($before_break);

                        $auto_image_text_array = imagettfbbox($auto_image_fontsize, 0, $font, rtrim($string));

                        if($auto_image_text_x_position == 'left'){
                            $auto_image_text_x[$ny] = 0;
			                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
                            }
                        elseif($auto_image_text_x_position == 'right'){
			                $auto_image_text_x[$ny] = $auto_image_width - $auto_image_text_array[2];
			                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
			                }
			            else{
			                $auto_image_text_x[$ny] = ($auto_image_width - $auto_image_text_array[2]) / 2;
			                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
                            }

                        $auto_image_text_y[$ny+1] = $auto_image_text_y[$ny] + $lineheight;
                        $ny++;
                        }
			        else{
                        $string = rtrim($tmp_string);
                        $row[$ny] = rtrim($tmp_string);
					    }
					}
				else {
                    $tmp_string = '';
		            $before_break = '';
		            $after_break = '';
                    
				    // If it doesn't fit, get the width of the whole string
                    $auto_image_text_array = imagettfbbox($auto_image_fontsize, 0, $font, rtrim($string));

                        if($auto_image_text_x_position == 'left'){
                            $auto_image_text_x[$ny] = 0;
			                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
                            }
                        elseif($auto_image_text_x_position == 'right'){
			                $auto_image_text_x[$ny] = $auto_image_width - $auto_image_text_array[2];
			                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
			                }
			            else{
			                $auto_image_text_x[$ny] = ($auto_image_width - $auto_image_text_array[2]) / 2;
			                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
                            }

				    $row[$ny] = $string;
				    $string = '';
                    $auto_image_text_y[$ny+1] = $auto_image_text_y[$ny] + $lineheight;
				    $i--;
                    $ny++;
 	                }
			    }

            $auto_image_text_array = imagettfbbox($auto_image_fontsize, 0, $font, $string);

            if($auto_image_text_x_position == 'left'){
                $auto_image_text_x[$ny] = 0;
                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
                }
            elseif($auto_image_text_x_position == 'right'){
                $auto_image_text_x[$ny] = $auto_image_width - $auto_image_text_array[2];
                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
                }
            else{
                $auto_image_text_x[$ny] = ($auto_image_width - $auto_image_text_array[2]) / 2;
                $auto_image_text_xx[$ny] = $auto_image_text_array[2];
                }

            $rowsoftext = count($row);
            $bottom_of_text = ($lineheight*$rowsoftext)-10;
            $longest_row_x = min($auto_image_text_x);
			$longest_row_xx = max($auto_image_text_xx);
            } while (($bottom_of_text > ($auto_image_height - $auto_image_top_bottom_padding)) || ($longest_row_xx > ($auto_image_width - $auto_image_left_right_padding)));

		if($auto_image_text_y_position == 'top'){
            $offset = $auto_image_top_padding;
            }
        elseif($auto_image_text_y_position == 'bottom'){
            $offset = $auto_image_height - $bottom_of_text - $auto_image_bottom_padding;
            }
        else{
            $offset = ($auto_image_height - $auto_image_top_bottom_padding - $bottom_of_text)/2 + $auto_image_top_padding;
            }

		if($auto_image_resize == 'text'){

  		    if($auto_image_resize_to_text == 'horizontally' || $auto_image_resize_to_text == 'both'){
				$new_width = $longest_row_xx + $auto_image_left_right_padding;
				$auto_image_width = $new_width;
				}

		    if($auto_image_resize_to_text == 'vertically' || $auto_image_resize_to_text == 'both'){
				$new_height = $bottom_of_text + $auto_image_top_bottom_padding;
				$auto_image_height = $new_height;
				}
		  
			for($i = 0; $i < $rowsoftext; $i++) {
				if($auto_image_text_y_position == 'top'){
					$auto_image_text_y[$i] = $auto_image_text_y[$i] + $auto_image_top_padding;
					$offset = 0;
					}
				elseif($auto_image_text_y_position == 'bottom'){
		            $offset = $new_height - $bottom_of_text - $auto_image_bottom_padding;
				    }
				else{
					$auto_image_text_y[$i] = ($new_height - $bottom_of_text)/2 + $auto_image_text_y[$i];
					$offset = 0;
    	            }

			    if($auto_image_text_x_position == 'left'){
					$auto_image_text_x[$i] = $auto_image_text_x[$i] + $auto_image_left_padding;
	   	            }
				elseif($auto_image_text_x_position == 'right'){
					$auto_image_text_x[$i] = $new_width - $auto_image_text_xx[$i] - $auto_image_right_padding;
	   	            }
				else{
					$auto_image_text_x[$i] = ($new_width - $auto_image_text_xx[$i])/2;
	   	            }
				}

            // Resize background to text and squish to fit
            $auto_image = imagecreatetruecolor( $new_width, $new_height );
            imagecopyresampled($auto_image, $new_featured_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            $new_featured_img = $auto_image;
		  
	        $text_color = imagecolorallocatealpha( $new_featured_img, $text["red"], $text["green"], $text["blue"], 0);
    	    $border_color = imagecolorallocatealpha( $new_featured_img, $border["red"], $border["green"], $border["blue"], 0);
        	$shadow_color = imagecolorallocatealpha( $new_featured_img, $shadow["red"], $shadow["green"], $shadow["blue"], 0);
			}
		else{
			for($i = 0; $i < $rowsoftext; $i++) {
				if($auto_image_text_x_position == 'left'){
	                $auto_image_text_x[$i] = $auto_image_text_x[$i] + $auto_image_left_padding;
	                }
	            elseif($auto_image_text_x_position == 'right'){
	                $auto_image_text_x[$i] = $auto_image_text_x[$i] - $auto_image_right_padding - 2;
	                }
	            else{
			        $auto_image_text_x[$i] = $auto_image_text_x[$i] + $auto_image_left_padding - $auto_image_right_padding;
	                }
			    }
			}

		$i = 0;
        $row = apply_filters('afift_trial_before_write_rows', $row);
        $auto_image_text_x = apply_filters('afift_pro_before_write_rows', $auto_image_text_x);
		if($auto_author_name =='yes'){
		$title_name_box = imagettfbbox($auto_image_fontsize, 0, $font,  rtrim($row[$i]));
		$title_height_single = abs($title_name_box[5]);
		$title_height = $title_height_single;
		$row_count_t = sizeof($row);
		$row_count = $row_count_t - 1;
		//$author_position_y = $author_position_y_default
		$author_position_y = $offset + $title_height + $auto_image_text_y[$row_count];
		if($row_count < 1){
		$author_position_y = $offset + $title_height * 2;
		}
		$both_side = $auto_image_text_x[$i]*2;
		$text_length = $new_width - $both_side;
		$author_position_x = $auto_image_text_x[$i];
		$author_id = $post->post_author;
		$author_name = get_the_author_meta('display_name',$author_id);
		$auto_author_name_box = imagettfbbox($auto_image_author_fontsize, 0, $font, $author_name);
		$auto_author_name_box_width = $auto_author_name_box[2];
		$auto_author_name_box_half = $auto_author_name_box[2]/2;
		$text_length_half = $text_length/2;
			if($auto_image_text_x_position == 'center'){
				
				$author_position_x = $auto_image_text_x[$i]+$text_length_half-$auto_author_name_box_half;
			}
		imagettftext($new_featured_img, $auto_image_author_fontsize , 0, $author_position_x, $author_position_y, $text_color, $font, $author_name);
		}
        while ($i < $rowsoftext){
            if($auto_image_shadow=='yes'){
                imagettftext($new_featured_img, $auto_image_fontsize, 0, $auto_image_text_x[$i]+2, $auto_image_text_y[$i]+$offset+2, $shadow_color, $font, rtrim($row[$i]));
                }
            if(isset($auto_image_border) && ($auto_image_border=='yes')){
                imagettftext($new_featured_img, $auto_image_fontsize, 0, $auto_image_text_x[$i]+1, $auto_image_text_y[$i]+$offset, $border_color, $font, rtrim($row[$i]));
                imagettftext($new_featured_img, $auto_image_fontsize, 0, $auto_image_text_x[$i], $auto_image_text_y[$i]+$offset+1, $border_color, $font, rtrim($row[$i]));
                imagettftext($new_featured_img, $auto_image_fontsize, 0, $auto_image_text_x[$i]-1, $auto_image_text_y[$i]+$offset, $border_color, $font, rtrim($row[$i]));
                imagettftext($new_featured_img, $auto_image_fontsize, 0, $auto_image_text_x[$i], $auto_image_text_y[$i]+$offset-1, $border_color, $font, rtrim($row[$i]));
                }
            imagettftext($new_featured_img, $auto_image_fontsize, 0, $auto_image_text_x[$i], $auto_image_text_y[$i]+$offset, $text_color, $font, rtrim($row[$i]));
            $i++;
            }
?>