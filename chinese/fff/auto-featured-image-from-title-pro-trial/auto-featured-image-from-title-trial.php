<?php
/*
Plugin Name: Auto Featured Image from Title Trial
Version: 1.0
Description: Automatically generates an image from the post title and sets it as the featured image
Author: Chris Huff
Author URI: http://designsbychris.com
Plugin URI: http://designsbychris.com/auto-featured-image-from-title
License: GPLv2 or later
*/
// Set up a few global variables
global $afift_uploads_path;
$afift_uploads_path = str_replace('auto-featured-image-from-title-pro-trial/','auto-featured-image-from-title-uploads/',plugin_dir_path( __FILE__ ));

global $afift_images_path;
$afift_images_path = $afift_uploads_path . 'images/';

global $afift_fonts_path;
$afift_fonts_path = $afift_uploads_path . 'fonts/';

// Set options if the plugins was just activated
function afift_set_settings_trial(){

    global $afift_uploads_path;
    global $afift_images_path;
    global $afift_fonts_path;

    // Deactivate free version
    $free_version_path = str_replace('-pro-trial','',plugin_dir_path( __FILE__ )) . 'auto-featured-image-from-title.php';
    deactivate_plugins($free_version_path);

    // Run only if the plugin was just installed or updated
    add_option('auto_image_pro',0);
    if(get_option('auto_image_pro')=='2.9')
    return;

    // Set all the options if they don't exist yet
    add_option('auto_image_post_types',get_post_types('','names'));
    $categories = get_categories();
    $categories_names = array();
    foreach($categories as $category){
        $categories_names[] = $category->name;
	    }
    add_option('auto_image_categories',$categories_names);
    add_option('auto_image_default_disable',"");
    add_option('auto_image_admin_only',"yes");
    add_option('auto_image_hooks',array('wp_insert_post'));
    add_option('auto_image_write_text',"yes");
    add_option('auto_image_text',"title");
    add_option('auto_image_content_length',55);
    add_option('auto_image_custom_field',"");
    add_option('auto_image_before_text',"");
    add_option('auto_image_after_text',"");
    add_option('auto_image_remove_linebreaks',"yes");
    add_option('auto_author_name',"no");
    add_option('auto_image_text_transform',"none");
    add_option('auto_image_text_x_position',"center");
    add_option('auto_image_text_y_position',"center");
    if(!get_option('auto_image_top_bottom_padding')){
        add_option('auto_image_top_padding',10);
        add_option('auto_image_bottom_padding',10);
	    }
    else{
        add_option('auto_image_top_padding',get_option('auto_image_top_bottom_padding'));
        add_option('auto_image_bottom_padding',get_option('auto_image_top_bottom_padding'));
	    }
    if(!get_option('auto_image_left_right_padding')){
        add_option('auto_image_left_padding',10);
        add_option('auto_image_right_padding',10);
	    }
    else{
	    add_option('auto_image_left_padding',get_option('auto_image_left_right_padding'));
        add_option('auto_image_right_padding',get_option('auto_image_left_right_padding'));
	    }
    add_option('auto_image_resize','crop');
    add_option('auto_image_resize_to_text','both');
    add_option('auto_image_width',640);
    add_option('auto_image_height',360);
    add_option('auto_image_filetype','jpg');
    add_option('auto_image_quality',95);
    add_option('auto_image_bg_image_from_category',"");
    add_option('auto_image_bg_image_from_post',"");
    add_option('auto_image_bg_image',"Landscape/sunset.jpg");
    add_option('auto_image_bg_color',"#b5b5b5");
    add_option('auto_image_fontface',"ChunkFive.ttf");
    add_option('auto_image_fontsize',30);
    add_option('auto_image_text_color',"#fff76d");
    add_option('auto_image_border',"no");
    add_option('auto_image_border_color',"#000000");
    add_option('auto_image_shadow',"yes");
    add_option('auto_image_shadow_color',"#000000");
    add_option('auto_image_flickr_api',"");
    add_option('auto_image_flickr_license',"4,5,7");
    add_option('auto_image_flickr_user',"");
    add_option('auto_image_unsplash_api',"");
    add_option('auto_image_unsplash_credit',"no");
    add_option('auto_image_default_search_words',"");
    add_option('auto_image_set_first',"");

    // Move the uploads directory
    $afift_old_uploads_path = plugin_dir_path( __FILE__ ) . 'uploads/';
    if(!file_exists($afift_uploads_path)){
        mkdir($afift_uploads_path, 0755);
        rename($afift_old_uploads_path, $afift_uploads_path);

        // Create a couple directories if uploads directory failed to move
        if(!file_exists($afift_images_path . '/Miscellaneous')){
            mkdir($afift_images_path . '/Miscellaneous', 0755);
            }
        if(!file_exists($afift_fonts_path)){
            mkdir($afift_fonts_path, 0755);
            }
        }
    elseif(file_exists($afift_old_uploads_path)){
        function afift_recursiveRemoveDirectory($directory){
            foreach(glob("{$directory}/*") as $file){
                if(is_dir($file)) {
                    afift_recursiveRemoveDirectory($file);
                    }
                else {
                    unlink($file);
                    }
                }
            rmdir($directory);
            }
        afift_recursiveRemoveDirectory($afift_old_uploads_path);
        }

    // Update options if upgrading from previous versions
    if(get_option('auto_image_fontface')=='chunkfive.ttf'){
        update_option('auto_image_fontface', 'ChunkFive.ttf');
        }
    if(get_option('auto_image_bg_image')=='sunset.jpg'){
        update_option('auto_image_bg_image', 'Landscape/sunset.jpg');
        }
    if(get_option('auto_image_bg_image')=='flower.jpg'){
        update_option('auto_image_bg_image', 'Flower/flower.jpg');
        }
    if(get_option('auto_image_bg_image')=='clouds.jpg'){
        update_option('auto_image_bg_image', 'Landscape/clouds.jpg');
        }
    if(get_option('auto_image_bg_image')=='grass-hill.jpg'){
        update_option('auto_image_bg_image', 'Landscape/grass-hill.jpg');
        }
    if(get_option('auto_image_bg_image')=='blank.jpg'){
        update_option('auto_image_bg_image', '');
        }
    if(get_option('auto_image_bg_image')=='Miscellaneous/blank.jpg'){
        update_option('auto_image_bg_image', '');
        }
    if(get_option('auto_image_bg_image')=='bokeh.jpg'){
        update_option('auto_image_bg_image', 'Miscellaneous/bokeh.jpg');
        }
    if(get_option('auto_image_bg_image')=='book.jpg'){
        update_option('auto_image_bg_image', 'Miscellaneous/book.jpg');
        }
    if(get_option('auto_image_bg_image')=='grunge.jpg'){
        update_option('auto_image_bg_image', 'Miscellaneous/grunge.jpg');
        }
    if(get_option('auto_image_bg_image')=='bricks.jpg'){
        update_option('auto_image_bg_image', 'Miscellaneous/bricks.jpg');
        }
    if(get_option('auto_image_bg_image')=='wood.jpg'){
        update_option('auto_image_bg_image', 'Miscellaneous/wood.jpg');
        }
    if(get_option('auto_image_pro') == 0){
	    if(get_option('auto_image_fontface')=='Windsong.ttf'){
	        update_option('auto_image_fontface', 'AguafinaScript.ttf');
	        }
	    if(get_option('auto_image_fontface')=='CaviarDreams.ttf'){
	        update_option('auto_image_fontface', 'Railway.ttf');
	        }
        }
    if(get_option('auto_image_text')!='content'){
        update_option('auto_image_content_length',500);
        }
    update_option('auto_image_pro', '2.9');
    }

add_action('admin_init', 'afift_set_settings_trial');
add_action('init', 'stern_taxi_fare_register_shortcodes1');
function stern_taxi_fare_register_shortcodes1() {
    add_shortcode('chinese_system', 'chinese_system11');
}

function chinese_system11($atts) {
	global $post;
	$post_id = $post->ID;
	auto_featured_image_from_title_trial($post_id);
}
// This is where the magic happens!
function auto_featured_image_from_title_trial ($post_id) {

    // Frequently used variables
    global $afift_images_path;
    global $afift_fonts_path;
    $post = get_post( $post_id );

    // Don't run if the post doesn't even have an ID yet
    if (!isset($post->ID) )
    return;

    // Check to see if the post already has a featured image
    if (has_post_thumbnail($post->ID))
    return;

    // If post is in the trash, don't generate an image
    $post_status = get_post_status($post->ID);
    if($post_status == 'trash')
    return;

    // If the post meta says not to, guess what, don't generate an image
    $disable_post = get_post_meta($post->ID, 'afift-disable', true);
    if(!isset($_GET['override_disable'])){
        if($disable_post == "yes")
       	return;
		}

    // Try to prevent the script from timing out or running out of memory
    set_time_limit(0);
    wp_cache_flush();

    // Get options from database
    $auto_image_post_types = get_option('auto_image_post_types');
    $auto_image_categories = get_option('auto_image_categories');
    $auto_image_default_disable = get_option('auto_image_default_disable');
    $auto_image_admin_only = get_option('auto_image_admin_only');
    $auto_image_hooks = get_option('auto_image_hooks');
    $auto_image_text = get_option('auto_image_text');
    $auto_image_content_length = get_option('auto_image_content_length');
    $auto_image_custom_field = get_option('auto_image_custom_field');
    $auto_image_before_text = get_option('auto_image_before_text');
    $auto_image_after_text = get_option('auto_image_after_text');
    $auto_image_remove_linebreaks = get_option('auto_image_remove_linebreaks');
    $auto_author_name = get_option('auto_author_name');
    $auto_image_text_transform = get_option('auto_image_text_transform');
    $auto_image_text_x_position = get_option('auto_image_text_x_position');
    $auto_image_text_y_position = get_option('auto_image_text_y_position');
    $auto_image_top_padding = get_option('auto_image_top_padding');
    $auto_image_bottom_padding = get_option('auto_image_bottom_padding');
    $auto_image_left_padding = get_option('auto_image_left_padding');
    $auto_image_right_padding = get_option('auto_image_right_padding');
    $auto_image_top_bottom_padding = get_option('auto_image_top_bottom_padding');
    $auto_image_left_right_padding = get_option('auto_image_left_right_padding');
    $auto_image_write_text = get_option('auto_image_write_text');
    $auto_image_resize = get_option('auto_image_resize');
    $auto_image_resize_to_text = get_option('auto_image_resize_to_text');
    $auto_image_width = get_option('auto_image_width');
    $auto_image_height = get_option('auto_image_height');
    $auto_image_filetype = get_option('auto_image_filetype');
    $auto_image_quality = get_option('auto_image_quality');
    $auto_image_bg_image_from_category = get_option('auto_image_bg_image_from_category');
    $auto_image_bg_image_from_post = get_option('auto_image_bg_image_from_post');
    $auto_image_bg_image = get_option('auto_image_bg_image');
    $auto_image_bg_color = get_option('auto_image_bg_color');
    $auto_image_fontface = get_option('auto_image_fontface');
    $auto_image_fontsize = get_option('auto_image_fontsize');
    $auto_image_text_color = get_option('auto_image_text_color');
    $auto_image_border = get_option('auto_image_border');
    $auto_image_border_color = get_option('auto_image_border_color');
    $auto_image_shadow = get_option('auto_image_shadow');
    $auto_image_shadow_color = get_option('auto_image_shadow_color');
    $auto_image_flickr_api = get_option('auto_image_flickr_api');
    $auto_image_flickr_license = get_option('auto_image_flickr_license');
    $auto_image_flickr_user = get_option('auto_image_flickr_user');
    $auto_image_unsplash_api = get_option('auto_image_unsplash_api');
    $auto_image_unsplash_credit = get_option('auto_image_unsplash_credit');
    $auto_image_default_search_words = get_option('auto_image_default_search_words');
    $auto_image_set_first = get_option('auto_image_set_first');

    $auto_image_text_color = apply_filters('afift_pro_after_get_options', $auto_image_text_color, $post_id);

    // Don't run if default disable is enabled, unless enabled by the specific post
    if(($auto_image_default_disable=='yes') && ($disable_post!='no'))
    return;

    // Don't run on unwanted post types
    if(empty($auto_image_post_types))
    return;
    if(!in_array($post->post_type, $auto_image_post_types, true))
    return;

    // Don't run on unwanted categories
    if (((!is_array($auto_image_categories) && $auto_image_categories!='all_the_cats')) || (is_array($auto_image_categories) && !in_array('all_the_cats', $auto_image_categories, true))){
        if($post->post_type != 'page'){
        	$test_cat = 'nope';
        	if(is_array($auto_image_post_types)){
    	        foreach($auto_image_post_types as $post_type){
        	        if($post->post_type == $post_type){
    	                foreach(wp_get_post_categories($post->ID) as $post_category){
                        	$category = get_category($post_category);
                    	    if(is_array($auto_image_categories)){
                	  	        if (in_array($category->name, $auto_image_categories, true)){
            	                    $test_cat = 'yep';
        	    				    }
    	                        }
            	        	elseif($category->name == $auto_image_categories){
                    	        $test_cat = 'yep';
            					}
            			    }
        				}
    			    }
    			}
        	elseif($auto_image_post_types == $post->post_type){
        	    $test_cat = 'yep';
    		    }
        	if($test_cat == 'nope')
        	return;
    		}
	    }

    // Make sure the post text has been given to the post
    $auto_image_post_title = html_entity_decode(strip_tags(get_the_title($post->ID)),ENT_QUOTES,'UTF-8');
	if($auto_author_name =='yes'){
	$author_id = $post->post_author;
	$author_name = get_the_author_meta('display_name',$author_id);
    $auto_image_post_title = $auto_image_post_title.'+'.$author_name;
    }
    if($auto_image_text=='content'){
		$auto_image_post_content = html_entity_decode(strip_tags($post->post_content));
	  	$auto_image_post_text = $auto_image_post_content;
        }
    elseif($auto_image_text=='excerpt'){
        $auto_image_post_excerpt = html_entity_decode(get_the_excerpt());
	    $auto_image_post_text = $auto_image_post_excerpt;
        }
    elseif($auto_image_text=='date'){
        $auto_image_post_text = html_entity_decode(get_the_date());
        }
    elseif($auto_image_text=='custom_field'){
        $auto_image_post_custom_field = html_entity_decode(get_post_meta($post->ID, $auto_image_custom_field, true));
        $auto_image_post_text = $auto_image_post_custom_field;
        }
    else {
        $auto_image_post_text = $auto_image_post_title;
        }
    if (( $auto_image_post_text == '' ) || ( $auto_image_post_text == 'Auto Draft' ))
    return;

    if(strlen($auto_image_post_text) > $auto_image_content_length){
        $auto_image_post_text = substr($auto_image_post_text, 0, $auto_image_content_length);
        $auto_image_post_text .= '...';
        }
  
    // Separate hexidecimal colors into red, green, and blue strings
    if(!function_exists('afift_hex2rgbcolors')){
        function afift_hex2rgbcolors($c){
            $c = str_replace("#", "", $c);
            if(strlen($c) == 3){
                $r = hexdec( $c[0] . $c[1] );
                $g = hexdec( $c[1] . $c[1] );
                $b = hexdec( $c[2] . $c[1] );
                }
            elseif (strlen($c) == 6 ){
                $r = hexdec( $c[0] . $c[2] );
                $g = hexdec( $c[2] . $c[2] );
                $b = hexdec( $c[4] . $c[2] );
                }
            else{
                $r = 'ff';
                $g = 'ff';
                $b = '00';
                }
            return Array("red" => $r, "green" => $g, "blue" => $b);
            }
        }

	if(!isset($bg)){$bg = '';}
    $auto_image_bg_color = apply_filters('afift_background_color', $bg, $post);
    if($auto_image_bg_color == ''){$auto_image_bg_color = get_option('auto_image_bg_color');}
    $bg = afift_hex2rgbcolors($auto_image_bg_color);
    $text = afift_hex2rgbcolors($auto_image_text_color);
    $border = afift_hex2rgbcolors($auto_image_border_color);
    $shadow = afift_hex2rgbcolors($auto_image_shadow_color);

    // Set the background image
    if(strpos($auto_image_bg_image,'random_') !== false){
        if($auto_image_bg_image == 'random_afift_bg'){
            // Use a random installed photo
			$directories = glob($afift_images_path . "*", GLOB_ONLYDIR);
			$directory = $directories[array_rand($directories)];
			$auto_image_bg_images = glob($directory . '/' . "*{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}", GLOB_BRACE);
			$backgroundimg = $auto_image_bg_images[array_rand($auto_image_bg_images)];
            }
        elseif($auto_image_bg_image == 'random_flickr_bg'){
            // Use a random Flickr photo
            $commonWords = array('I', 'a', 'about', 'an', 'are', 'as', 'at', 'be', 'by', 'for', 'from', 'how', 'in', 'is', 'it', 'of', 'on', 'or', 'that', 'the', 'this', 'to', 'was', 'what', 'when', 'where', 'who', 'will', 'with', 'the'); 
            $flickr_search_words = preg_replace("/\b$commonWords\b/i", "", $auto_image_post_text);
            $regex = array('/[^\p{L}\p{N}\s]/u', '/\s/');
            $repl  = array('', ' ');
            $flickr_search_words = preg_replace($regex, $repl, $flickr_search_words);
	        $flickr_user = "";
//          if($auto_image_flickr_user != ""){$flickr_user = "&user_id=" . $auto_image_flickr_user;}

            $done = false;
            $tries = 0;
            while(!$done && $tries < 5){
                $tries++;
                $n = rand(1,50);

                if($tries < 4){
                    $search = 'http://flickr.com/services/rest/?method=flickr.photos.search&api_key=' . $auto_image_flickr_api . '&text=' . urlencode($flickr_search_words) . '&per_page=' . $n . '&format=php_serial&sort=relevance&license=' . $auto_image_flickr_license . $flickr_user;
                    }
                else {
                    // Try two more times to find an image, this time using tags
                    $flickr_search_words = str_replace(' ',',',$flickr_search_words);
                    $search = 'http://flickr.com/services/rest/?method=flickr.photos.search&api_key=' . $auto_image_flickr_api . '&tags=' . urlencode($flickr_search_words) . '&per_page=' . $n . '&format=php_serial&sort=relevance&license=' . $auto_image_flickr_license . $flickr_user;
                    }

                $result = file_get_contents($search);
                $data = unserialize($result);

                if(array_key_exists('photos',$data)) {
                    if (is_array($data['photos']['photo'])){
                        foreach($data['photos']['photo'] as $photo) {
                            $backgroundimg = 'http://farm' . $photo["farm"] . '.static.flickr.com/' . $photo["server"] . '/' . $photo["id"] . '_' . $photo["secret"] . '.jpg';
                            }
                        if(@getimagesize($backgroundimg)){
                            $done = true;
                            }
                        }
                    }
                }

            if(!$done){
                // If the Flickr search fails 3x, use a random installed photo instead
				$directories = glob($afift_images_path . "*", GLOB_ONLYDIR);
				$directory = $directories[array_rand($directories)];
				$auto_image_bg_images = glob($directory . '/' . "*{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}", GLOB_BRACE);
				$backgroundimg = $auto_image_bg_images[array_rand($auto_image_bg_images)];
                }
            }
		elseif($auto_image_bg_image == 'random_unsplash_bg'){
            // Use an Unsplash photo

    		// Check to see whether to use post title, default, or post-specific search terms to find an image
		  	$auto_image_post_search_words = get_post_meta($post->ID, 'afift-image-search-words', true);
		  
		  	if($auto_image_post_search_words != ''){
				$unsplash_search_words = $auto_image_post_search_words;
				}
		    elseif($auto_image_default_search_words != ''){
			    $unsplash_search_words = $auto_image_default_search_words;
				}
		    else {
			    $commonWords = array('I', 'a', 'about', 'an', 'are', 'as', 'at', 'be', 'by', 'for', 'from', 'how', 'in', 'is', 'it', 'of', 'on', 'or', 'that', 'the', 'this', 'to', 'was', 'what', 'when', 'where', 'who', 'will', 'with', 'the'); 
    	        $unsplash_search_words = preg_replace("/\b$commonWords\b/i", "", $auto_image_post_text);
        	    $regex = array('/[^\p{L}\p{N}\s]/u', '/\s/');
            	$repl  = array('', ' ');
            	$unsplash_search_words = preg_replace($regex, $repl, $unsplash_search_words);
				}

            $search = 'https://api.unsplash.com/search/photos?page=1&query=' . $unsplash_search_words . '&client_id=' . $auto_image_unsplash_api;

            $result = file_get_contents($search);
            $data = json_decode($result, true);
		    if($data['total_pages'] > 0){
			    $n_page = rand(1,$data['total_pages']);
	            $search = 'https://api.unsplash.com/search/photos?page=' . $n_page .  '&query=' . $unsplash_search_words . '&client_id=' . $auto_image_unsplash_api;
	            $result = file_get_contents($search);
	            $data = json_decode($result, true);
				$count_n = count($data['results']);
			    $n = rand(0,($count_n-1));
				$backgroundimg = $data['results'][$n]['urls']['full'];
				if($auto_image_unsplash_credit == 'yes'){
					$caption = 'Photo by: <a href="https://unsplash.com/@' . $data['results'][$n]['user']['username'] . '?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">' . $data['results'][$n]['user']['name'] . '</a> / <a href="https://unsplash.com?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Unsplash</a>';
					}
				}
			else {
			    // If the Unsplash search fails, use a random installed photo instead
				$directories = glob($afift_images_path . "*", GLOB_ONLYDIR);
				$directory = $directories[array_rand($directories)];
				$auto_image_bg_images = glob($directory . '/' . "*{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}", GLOB_BRACE);
				$backgroundimg = $auto_image_bg_images[array_rand($auto_image_bg_images)];
	  		    }
            }
        else {
            // Use a random installed photo of a certain category
		    $auto_image_bg_image = str_replace('random_','',$auto_image_bg_image);
            $auto_image_bg_images = scandir($afift_images_path . $auto_image_bg_image);
            do {
                $backgroundimg = $auto_image_bg_images[array_rand($auto_image_bg_images)];
                }
            while (is_dir($afift_images_path . $auto_image_bg_image . '/' . $backgroundimg));
            $backgroundimg = $afift_images_path . $auto_image_bg_image . '/' . $backgroundimg;
            }
        }
    else{
    	// Use the exact photo selected
        $backgroundimg = $afift_images_path . $auto_image_bg_image;
        }
    if($auto_image_bg_image_from_post == "yes"){
        // Use the first image in the post, if there is one
        $first_img = '';
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
        if($output != ''){
            $backgroundimg = $matches [1] [0];
            }
        }
    if($auto_image_bg_image_from_category == "yes"){
	    // Use the a random image from the same category, if it can find a matching category
        $post_cats = get_the_category($post->ID);
        if(!empty($post_cats)){
            $afift_image_subdirs = glob($afift_images_path . '*' , GLOB_ONLYDIR);
            foreach ( $afift_image_subdirs as $afift_image_subdir ) {
                $afift_image_cat = str_replace($afift_images_path, '', $afift_image_subdir);
                foreach ($post_cats as $post_cat){
				    $post_cat_name = strtolower(esc_html( $post_cat->name ));
                    if ( strtolower($afift_image_cat) == $post_cat_name ) {
                        $auto_image_bg_images = scandir($afift_images_path . $afift_image_cat);
                        do {
                            $backgroundimg = $auto_image_bg_images[array_rand($auto_image_bg_images)];
                            }
					  while (is_dir($afift_images_path . $afift_image_cat . '/' . $backgroundimg));
                        $backgroundimg = $afift_images_path . $afift_image_cat . '/' . $backgroundimg;
                        break;
                        }
				    }
				}
            }
        }

    // Make sure the background image selected actually exists
  //    if (stream_resolve_include_path($backgroundimg) == false){
        // Unless we're using a flickr image, which would fail the test
  //    if($auto_image_bg_image != 'random_flickr_bg'){
  //        $backgroundimg = $afift_images_path . 'Miscellaneous/blank.jpg';
  //        }
  //    }

    // Start generating the image
    if($auto_image_bg_image==''){
        $new_featured_img = imagecreatetruecolor($auto_image_width, $auto_image_height);
        $background_color = imagecolorallocate( $new_featured_img, $bg["red"], $bg["green"], $bg["blue"]);
        imagefill($new_featured_img, 0, 0, $background_color);
        $new_featured_image = $new_featured_img;
	    $width = $auto_image_width;
	    $height = $auto_image_height;
	    $new_width = $auto_image_width;
	    $new_height = $auto_image_height;

		if($auto_image_resize == 'text'){
			if($auto_image_resize_to_text == 'horizontally'){
			  //				$auto_image_width = 99999;
    			$auto_image_width = get_option('auto_image_width');
			    $new_width = $auto_image_width;
				$new_height = $auto_image_height;
			    }
			elseif($auto_image_resize_to_text == 'vertically'){
				$new_width = $auto_image_width;
			  //				$auto_image_height = 99999;
			    $auto_image_height = get_option('auto_image_height');
			  	$new_height = $auto_image_height;
			    }
			elseif($auto_image_resize_to_text == 'both'){
			  //				$auto_image_width = 99999;
			    $auto_image_width = get_option('auto_image_width');
			  //			    $auto_image_height = 99999;
			    $auto_image_height = get_option('auto_image_height');
			    }
			}
		}
    else {
        $ext = strtolower(pathinfo($backgroundimg, PATHINFO_EXTENSION));
        if($ext == 'png'){
            $new_featured_img = imagecreatefrompng($backgroundimg);
            }
        elseif($ext == 'gif'){
            $new_featured_img = imagecreatefromgif($backgroundimg);
            }
        else{
            $new_featured_img = imagecreatefromjpeg($backgroundimg);
            }

	    $width = imagesx($new_featured_img);
        $height = imagesy($new_featured_img);

        $new_featured_image = $new_featured_img;
	  
        if($auto_image_resize != 'no'){
		    $original_aspect = $width / $height;
	        $auto_image_aspect = $auto_image_width / $auto_image_height;

	        if ( $original_aspect >= $auto_image_aspect ){
	            // If original image is wider than new generated image (in aspect ratio sense)
	            $new_width = $width / ($height / $auto_image_height);
	            $new_height = $auto_image_height;
	            }
	        else {
	            // If new generated image is wider than original image
	            $new_width = $auto_image_width;
	            $new_height = $height / ($width / $auto_image_width);
	            }

	        if($auto_image_resize == 'crop'){
		        // Resize and crop
		        $auto_image = imagecreatetruecolor( $auto_image_width, $auto_image_height );
		        imagecopyresampled(
		            $auto_image,
		            $new_featured_img,
		            // Center the image horizontally
		            0 - ($new_width - $auto_image_width) / 2,
		            // Center the image vertically
		            0 - ($new_height - $auto_image_height) / 2,
		            0, 0,
		            $new_width, $new_height,
		            $width, $height);
		        $new_featured_img = $auto_image;
		        }
	  		elseif($auto_image_resize == 'squish') {
			    // Resize and squish to fit
		        $auto_image = imagecreatetruecolor( $auto_image_width, $auto_image_height );
		        imagecopyresampled($auto_image, $new_featured_img, 0, 0, 0, 0, $auto_image_width, $auto_image_height, $width, $height);
		        $new_featured_img = $auto_image;
				}
	  		elseif($auto_image_resize == 'shrink') {
			    // Resize and shrink to fit
		        if ( $original_aspect < $auto_image_aspect ){
		            // If new generated image is wider than original image (in aspect ratio sense)
		            $new_height = $auto_image_height;
		            $new_width = $width / ($height / $auto_image_height);
		            }
		        else {
		            // If original image is wider than new generated image
		            $new_width = $auto_image_width;
		            $new_height = $height / ($width / $auto_image_width);
		            }

 		        $auto_image = imagecreatetruecolor( $auto_image_width, $auto_image_height );
			    $background_color = imagecolorallocate( $auto_image, $bg["red"], $bg["green"], $bg["blue"]);
        	    imagefill($auto_image, 0, 0, $background_color);
			    imagecopyresampled(
		            $auto_image,
		            $new_featured_img,
		            // Center the image horizontally
		            0 - ($new_width - $auto_image_width) / 2,
		            // Center the image vertically
		            0 - ($new_height - $auto_image_height) / 2,
		            0, 0,
		            $new_width, $new_height,
		            $width, $height);
		        $new_featured_img = $auto_image;
				}
	  		elseif($auto_image_resize == 'text'){
			    if($auto_image_resize_to_text == 'horizontally'){
				  //					$auto_image_width = 99999;
	  			    $auto_image_width = get_option('auto_image_width');
					$new_width = $auto_image_width;
					$new_height = $auto_image_height;
				    }
			    elseif($auto_image_resize_to_text == 'vertically'){
					$new_width = $auto_image_width;
				  //					$auto_image_height = 99999;
				    $auto_image_height = get_option('auto_image_height');
					$new_height = $auto_image_height;
				    }
			    elseif($auto_image_resize_to_text == 'both'){
				  //					$auto_image_width = 99999;
				    $auto_image_width = get_option('auto_image_width');
				  //					$auto_image_height = 99999;
				    $auto_image_height = get_option('auto_image_height');
				    }
				}
	  		else {
			    // Just resize
		        $auto_image = imagecreatetruecolor( $new_width, $new_height );
		        imagecopyresampled($auto_image, $new_featured_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		        $new_featured_img = $auto_image;
				$auto_image_width = $new_width;
				$auto_image_height = $new_height;
				}
		    }
	  	else {
		    // Don't resize
			$auto_image_width = $width;
			$auto_image_height = $height;
		    $new_width = $width;
		    $new_height = $height;
			}
		}

    if($auto_image_write_text=='yes'){
        $text_color = imagecolorallocatealpha( $new_featured_img, $text["red"], $text["green"], $text["blue"], 0);
        $border_color = imagecolorallocatealpha( $new_featured_img, $border["red"], $border["green"], $border["blue"], 0);
        $shadow_color = imagecolorallocatealpha( $new_featured_img, $shadow["red"], $shadow["green"], $shadow["blue"], 0);

        // Select font
        if($auto_image_fontface == 'random_afift_font'){
            // Use a random installed font
            $auto_image_fonts = scandir($afift_fonts_path);

            do {
                $auto_image_fontface = $auto_image_fonts[array_rand($auto_image_fonts)];
                $font = $afift_fonts_path . $auto_image_fontface;
                } while (is_dir($font));
            }
        else {
            $font = $afift_fonts_path . $auto_image_fontface;
            }

        $auto_image_text_to_write = $auto_image_before_text . $auto_image_post_text . $auto_image_after_text;

        $auto_image_text_to_write = apply_filters('afift_pro_before_write_text', $auto_image_text_to_write);

        include('write_text.php');
        }

   $post_id = $post->ID;
    $new_featured_img = apply_filters('afift_pro_after_image_created', $new_featured_img, $post_id, $auto_image_width, $auto_image_height);
	
    if($auto_image_post_title == ''){
        $auto_image_post_title = 'image';
	    }
  
    // Save the image
    $attachment_array = array(
        'title'          => $auto_image_post_title,
        'alt'            => $auto_image_post_title,
        'caption'        => $auto_image_post_text,
        'description'    => $auto_image_post_text,
        'filename'       => $auto_image_post_title,
        'filename_spaces' => '-'
        );
    $attachment_array = apply_filters('afift_pro_before_save_image', $attachment_array, $post_id);
    $regex = array('/[^\p{L}\-\.\p{N}\s]/u', '/\s/');
    $repl  = array('', $attachment_array['filename_spaces']);
    $post_slug = strtolower(preg_replace($regex, $repl, $attachment_array['filename']));
    $post_slug = preg_replace('#[ -]+#', $attachment_array['filename_spaces'], $post_slug);
    $post_slug = substr($post_slug, 0, 195);
 
    $upload_dir = wp_upload_dir();
    $slug_n = '';
    while(file_exists($upload_dir['path'] . '/' . $post_slug . $slug_n . '.' . $auto_image_filetype)){
        if($slug_n == ''){
            $slug_n = 1;
            }
        else {
            $slug_n++;
            }
        }
    $newimg = $upload_dir['path'] . '/' . $post_slug . $slug_n . '.' . $auto_image_filetype;

    if($auto_image_filetype == 'jpg'){
        imagejpeg( $new_featured_img, $newimg, $auto_image_quality );
	    }
    else{
        imagepng( $new_featured_img, $newimg, 0 );
	    }

    if($auto_image_write_text=='yes'){
        imagecolordeallocate( $new_featured_img, $text_color );
        imagecolordeallocate( $new_featured_img, $border_color );
        imagecolordeallocate( $new_featured_img, $shadow_color );
	    }
	if($auto_image_bg_image==''){
        imagecolordeallocate( $new_featured_img, $background_color );
        }

    // Process the image into the Media Library
    $newimg_url = $upload_dir['url'] . '/' . $post_slug . $slug_n . '.' . $auto_image_filetype;
	print_r('<img src="'.$newimg_url.'">');
	
	/// Set feature image

    if($auto_image_filetype=='jpg'){
		$mime_type = 'jpeg';
	    }
	else{
		$mime_type = 'png';
	    }

    if(isset($caption)){
		$post_excerpt = $caption;
		}
	else {
		$post_excerpt = $attachment_array['caption'];
		}
  
    $attachment = array(
        'guid'           => $newimg_url, 
        'post_mime_type' => 'image/' . $mime_type,
        'post_title'     => $attachment_array['title'],
	    'post_excerpt'   => $post_excerpt,
        'post_content'   => $attachment_array['description'],
        'post_status'    => 'inherit'
        );
    $attach_id = wp_insert_attachment( $attachment, $newimg, $post->ID );
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    $attach_data = wp_generate_attachment_metadata( $attach_id, $newimg );
    wp_update_attachment_metadata( $attach_id, $attach_data );
    update_post_meta( $attach_id, '_wp_attachment_image_alt', wp_slash($attachment_array['alt']) );

    // Set the image as the featured image
    set_post_thumbnail( $post->ID, $attach_id );
    }


function test_future_post($post_id){
// Create post object
}

// Check when to run
$auto_image_admin_only = get_option('auto_image_admin_only');
if((($auto_image_admin_only == 'yes') && (is_admin())) || ($auto_image_admin_only == '')){
    $auto_image_hooks = get_option('auto_image_hooks');
    if($auto_image_hooks != ''){
        foreach ( $auto_image_hooks as $hook ) {
            add_action( $hook, 'auto_featured_image_from_title_trial', 99999 );
//            add_action( $hook, 'test_future_post', 99999 );
            }
	    }
	}

// filter to add Unsplash Photo credit
function afift_add_featured_image_caption($html, $post_id, $post_thumbnail_id, $size, $attr) {
	if(get_option('auto_image_unsplash_credit') == 'yes'){
	    $out = '';
	    $thumbnail_image = get_posts(array('p' => $post_thumbnail_id, 'post_type' => 'attachment'));
		if(isset($thumbnail_image[0]->post_excerpt) && $thumbnail_image[0]->post_excerpt != ''){
		    if(substr( $thumbnail_image[0]->post_excerpt, 0, 5 ) === "Photo"){
			    if ($thumbnail_image && isset($thumbnail_image[0])) {
			        $image = wp_get_attachment_image_src($post_thumbnail_id, $size);
			        if($thumbnail_image[0]->post_excerpt) 
			        $out .= $html;
			        if($thumbnail_image[0]->post_excerpt) 
					  $out .= '</a><p class="wp-caption-text thumb-caption-text">'.$thumbnail_image[0]->post_excerpt.'</p>';
				    }
				return $out;
				}
			}
		}
	return $html;
	}
if(!is_admin()){
	add_filter( 'post_thumbnail_html', 'afift_add_featured_image_caption', 10, 5 );
	}

// Adds check boxes to edit post screens
function afift_meta_box_trial($object){
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
    ?>
        <div>
            <label for="afift-disable">
			  <input name="afift-disable" type="hidden" value="no">
			  <input name="afift-disable" type="checkbox" value="yes"<?php
                if((!is_array(get_post_custom_keys($object->ID))) || (is_array(get_post_custom_keys($object->ID)) && (!in_array('afift-disable', get_post_custom_keys($object->ID))))){
				    if(get_option('auto_image_default_disable') == 'yes'){
					    echo ' checked';
					    }
				    }
                else{
                    $checkbox_value = get_post_meta($object->ID, "afift-disable", true);
                    if($checkbox_value == "yes"){
					    echo ' checked';
				        }
				    }
			    ?>>Disable for this post</label>
            <?php
            $afift_disable_set_first = get_option('auto_image_set_first');
            if($afift_disable_set_first == 'yes'){ ?>

            <br />
            <label for="afift-disable-set-first"><input name="afift-disable-set-first" type="checkbox" value="yes"<?php
                $checkbox_value = get_post_meta($object->ID, "afift-disable-set-first", true);
                if($checkbox_value == "yes"){
                    echo ' checked';
				    } ?>>Disable setting from first image for this post</label><?php
			    }
  			$auto_image_bg_image = get_option('auto_image_bg_image');
            if($auto_image_bg_image == 'random_unsplash_bg'){ ?>
			<p><label for="afift-image-search-words">Image Search Term(s):</label>
        	<input name="afift-image-search-words" type="text" size="15" id="afift-image-search-words" value="<?php echo get_post_meta($object->ID, 'afift-image-search-words', true); ?>" /></p>
			<?php } ?>

        </div>
    <?php  
}

function add_afift_meta_box_trial(){
	$auto_image_post_types = get_option('auto_image_post_types');
   	if(is_array($auto_image_post_types)){
        foreach($auto_image_post_types as $post_type){
			add_meta_box("afift-disable", "Auto Featured Image", "afift_meta_box_trial", $post_type, "side", "low", "high");
		    }
		}
   	elseif($auto_image_post_types != ''){
		add_meta_box("afift-disable", "Auto Featured Image", "afift_meta_box_trial", $auto_image_post_types, "side", "low", "high");
	    }
    }

add_action("add_meta_boxes", "add_afift_meta_box_trial");

// Saves values from check boxes when saving a post
function save_afift_meta_box_trial($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $meta_box_checkbox_value = "";
    $meta_box_set_first_checkbox_value = "";

    if(isset($_POST["afift-disable"]))
    {
        $meta_box_checkbox_value = $_POST["afift-disable"];
    }   
    update_post_meta($post_id, "afift-disable", $meta_box_checkbox_value);

    if(isset($_POST["afift-disable-set-first"]))
    {
        $meta_box_set_first_checkbox_value = $_POST["afift-disable-set-first"];
    }   
    update_post_meta($post_id, "afift-disable-set-first", $meta_box_set_first_checkbox_value);

    if(isset($_POST["afift-image-search-words"]))
    {
        $meta_box_search_words_value = $_POST["afift-image-search-words"];
    }   
    update_post_meta($post_id, "afift-image-search-words", $meta_box_search_words_value);
}

add_action("save_post", "save_afift_meta_box_trial", 10, 3);

// Set the first image in the post as the featured image, if there isn't one yet
function afift_set_first_image_trial($post_id) {

    $post = get_post( $post_id );

    // Make sure the featured image isn't set yet
    if (get_the_post_thumbnail($post->ID) != '')
    return;

    // Make sure the option to do this is set to yes
    $auto_image_set_first = get_option('auto_image_set_first');
    if($auto_image_set_first != 'yes')
	return;

    // Make sure the option isn't disabled in post meta
    $disable_set_first = get_post_meta($post->ID, 'afift-disable-set-first', true);
    if($disable_set_first == "yes"){
	    return;
        }
  
    // Don't run on unwanted post types
    if(empty($auto_image_post_types))
    return;
    if (!in_array($post->post_type, get_option('auto_image_post_types'), true))
    return;

    // Don't run on unwanted categories
    $cat = 'nope';
    $auto_image_categories = get_option('auto_image_categories');
    foreach(wp_get_post_categories($post->ID) as $category){
        if (in_array($category, $auto_image_categories, true)){
            $cat = 'yep';
		    }
	    }
    if($cat == 'nope')
	return;

    // Use the first image in the post, if there is one
    $first_img = '';
    $backgroundimg = '';
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    if($output != ''){
        $backgroundimg = $matches [1] [0];
        }

	if ( !$backgroundimg || strlen( $backgroundimg ) == 0 )
		return false;

    $auto_image_resize = get_option('auto_image_resize');
    $auto_image_width = get_option('auto_image_width');
    $auto_image_height = get_option('auto_image_height');
    $auto_image_quality = get_option('auto_image_quality');
  
    $ext = strtolower(pathinfo($backgroundimg, PATHINFO_EXTENSION));
    if($ext == 'png'){
        $new_featured_img = imagecreatefrompng($backgroundimg);
        }
    elseif($ext == 'gif'){
        $new_featured_img = imagecreatefromgif($backgroundimg);
        }
    else{
        $new_featured_img = imagecreatefromjpeg($backgroundimg);
        }
    $width = imagesx($new_featured_img);
    $height = imagesy($new_featured_img);

    if($auto_image_resize != 'no'){
	    $original_aspect = $width / $height;
        $auto_image_aspect = $auto_image_width / $auto_image_height;

        if ( $original_aspect >= $auto_image_aspect ){
            // If original image is wider than new generated image (in aspect ratio sense)
            $new_height = $auto_image_height;
            $new_width = $width / ($height / $auto_image_height);
            }
        else {
            // If new generated image is wider than original image
            $new_width = $auto_image_width;
            $new_height = $height / ($width / $auto_image_width);
            }

        if($auto_image_resize == 'crop'){
	        // Resize and crop
	        $auto_image = imagecreatetruecolor( $auto_image_width, $auto_image_height );
	        imagecopyresampled(
	            $auto_image,
	            $new_featured_img,
	            // Center the image horizontally
	            0 - ($new_width - $auto_image_width) / 2,
	            // Center the image vertically
	            0 - ($new_height - $auto_image_height) / 2,
	            0, 0,
	            $new_width, $new_height,
	            $width, $height);
	        $new_featured_img = $auto_image;
	        }
  		elseif($auto_image_resize == 'squish') {
		    // Resize and squish to fit
	        $auto_image = imagecreatetruecolor( $auto_image_width, $auto_image_height );
	        imagecopyresampled($auto_image, $new_featured_img, 0, 0, 0, 0, $auto_image_width, $auto_image_height, $width, $height);
	        $new_featured_img = $auto_image;
			}
  		elseif($auto_image_resize == 'shrink') {
		    // Resize and shrink to fit
	        if ( $original_aspect < $auto_image_aspect ){
	            // If new generated image is wider than original image (in aspect ratio sense)
	            $new_height = $auto_image_height;
	            $new_width = $width / ($height / $auto_image_height);
	            }
	        else {
	            // If original image is wider than new generated image
	            $new_width = $auto_image_width;
	            $new_height = $height / ($width / $auto_image_width);
	            }

		    $auto_image = imagecreatetruecolor( $auto_image_width, $auto_image_height );
            $background_color = imagecolorallocate( $auto_image, $bg["red"], $bg["green"], $bg["blue"]);
       	    imagefill($auto_image, 0, 0, $background_color);
		    imagecopyresampled(
	            $auto_image,
	            $new_featured_img,
	            // Center the image horizontally
	            0 - ($new_width - $auto_image_width) / 2,
	            // Center the image vertically
	            0 - ($new_height - $auto_image_height) / 2,
	            0, 0,
	            $new_width, $new_height,
	            $width, $height);
	        $new_featured_img = $auto_image;
			}
  		else {
		    // Just resize
			$new_width = $auto_image_width;
			$new_height = $auto_image_height;
	        $auto_image = imagecreatetruecolor( $new_width, $new_height );
	        imagecopyresampled($auto_image, $new_featured_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	        $new_featured_img = $auto_image;
		  //			$auto_image_width = $new_width;
		  //			$auto_image_height = $new_height;
			}
	    }
  	else {
	    // Don't resize
		$auto_image_width = $width;
		$auto_image_height = $height;
		$new_width = $auto_image_width;
		$new_height = $auto_image_height;
		}

    $auto_image_post_title = html_entity_decode(strip_tags(get_the_title($post->ID)),ENT_QUOTES,'UTF-8');
    if($auto_image_post_title == ''){
        $auto_image_post_title = 'image';
	    }
	  
    // Save the image
    $attachment_array = array(
        'title'          => $auto_image_post_title,
        'alt'            => $auto_image_post_title,
        'caption'        => $auto_image_post_title,
        'description'    => $auto_image_post_title,
        'filename'       => $auto_image_post_title,
        'filename_spaces' => '-'
        );
    $attachment_array = apply_filters('afift_pro_before_save_image', $attachment_array, $post_id);
    $regex = array('/[^\p{L}\p{N}\s]/u', '/\s/');
    $repl  = array('', $attachment_array['filename_spaces']);
    $post_slug = strtolower(preg_replace($regex, $repl, $attachment_array['filename']));
    $upload_dir = wp_upload_dir();
    $slug_n = ''; 
    while(file_exists($upload_dir['path'] . '/' . $post_slug . $slug_n . '.jpg')){ 
        if($slug_n == ''){ 
            $slug_n = 1; 
            } 
        else { 
            $slug_n++; 
            } 
        } 
    $newimg = $upload_dir['path'] . '/' . $post_slug . $slug_n . '.jpg';
    imagejpeg( $new_featured_img, $newimg, $auto_image_quality );

    // Process the image into the Media Library
    $newimg_url = $upload_dir['url'] . '/' . $post_slug . $slug_n . '.jpg';
    $attachment = array(
        'guid'           => $newimg_url, 
        'post_mime_type' => 'image/jpeg',
        'post_title'     => $attachment_array['title'],
        'post_excerpt'   => $attachment_array['caption'],
        'post_content'   => $attachment_array['description'],
        'post_status'    => 'inherit'
        );
    $attach_id = wp_insert_attachment( $attachment, $newimg, $post->ID );
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    $attach_data = wp_generate_attachment_metadata( $attach_id, $newimg );
    wp_update_attachment_metadata( $attach_id, $attach_data );
    update_post_meta( $attach_id, '_wp_attachment_image_alt', wp_slash($attachment_array['alt']) );

    // Set the image as the featured image
    set_post_thumbnail( $post->ID, $attach_id );
    }

// Check when to run
$auto_image_admin_only = get_option('auto_image_admin_only');
if((($auto_image_admin_only == 'yes') && (is_admin())) || ($auto_image_admin_only == '')){
    $auto_image_hooks = get_option('auto_image_hooks');
    if($auto_image_hooks != ''){
        foreach ( $auto_image_hooks as $hook ) {
            add_action( $hook, 'afift_set_first_image_trial', 999999 );
            }
	    }
	}

// Load the color picker on the admin page
function afift_enqueue_color_picker_trial( $hook_suffix ) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'auto_featured_image', plugins_url('colorpicker.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
    }

add_action( 'admin_enqueue_scripts', 'afift_enqueue_color_picker_trial' );

// Set up the admin page
function afift_settings_trial() {

    //create new top-level menu
    add_options_page('Auto Featured Image', 'Auto Featured Image', 'manage_options', 'auto-featured-image-from-title-pro-trial.php', 'afift_settings_page_trial');
    add_filter( "plugin_action_links", "afift_settings_link_trial", 10, 2 );
    //call register settings function
    add_action( 'admin_init', 'register_auto_featured_image_trial' );
}

function afift_settings_link_trial($links, $file) {
    static $this_plugin;
        if (!$this_plugin) $this_plugin = plugin_basename(__FILE__);
        if ($file == $this_plugin){
    $afift_settings_link = '<a href="options-general.php?page=auto-featured-image-from-title-pro-trial.php">'.__("Settings", "auto-featured-image-from-title-pro-trial").'</a>';
        array_unshift($links, $afift_settings_link);
        }
    return $links;
    }

function register_auto_featured_image_trial() {
    //register our settings
    register_setting( 'auto_featured_image_group', 'auto_image_post_types' );
    register_setting( 'auto_featured_image_group', 'auto_image_categories' );
    register_setting( 'auto_featured_image_group', 'auto_image_default_disable' );
    register_setting( 'auto_featured_image_group', 'auto_image_admin_only' );
    register_setting( 'auto_featured_image_group', 'auto_image_hooks' );
    register_setting( 'auto_featured_image_group', 'auto_image_write_text' );
    register_setting( 'auto_featured_image_group', 'auto_image_text' );
    register_setting( 'auto_featured_image_group', 'auto_image_content_length' );
    register_setting( 'auto_featured_image_group', 'auto_image_custom_field' );
    register_setting( 'auto_featured_image_group', 'auto_image_before_text' );
    register_setting( 'auto_featured_image_group', 'auto_image_after_text' );
    register_setting( 'auto_featured_image_group', 'auto_image_remove_linebreaks' );
    register_setting( 'auto_featured_image_group', 'auto_author_name' );
    register_setting( 'auto_featured_image_group', 'auto_image_text_transform' );
    register_setting( 'auto_featured_image_group', 'auto_image_text_x_position' );
    register_setting( 'auto_featured_image_group', 'auto_image_text_y_position' );
    register_setting( 'auto_featured_image_group', 'auto_image_top_padding' );
    register_setting( 'auto_featured_image_group', 'auto_image_bottom_padding' );
    register_setting( 'auto_featured_image_group', 'auto_image_left_padding' );
    register_setting( 'auto_featured_image_group', 'auto_image_right_padding' );
    register_setting( 'auto_featured_image_group', 'auto_image_resize' );
    register_setting( 'auto_featured_image_group', 'auto_image_resize_to_text' );
    register_setting( 'auto_featured_image_group', 'auto_image_width' );
    register_setting( 'auto_featured_image_group', 'auto_image_height' );
    register_setting( 'auto_featured_image_group', 'auto_image_filetype' );
    register_setting( 'auto_featured_image_group', 'auto_image_quality' );
    register_setting( 'auto_featured_image_group', 'auto_image_bg_color' );
    register_setting( 'auto_featured_image_group', 'auto_image_bg_image_from_category' );
    register_setting( 'auto_featured_image_group', 'auto_image_bg_image_from_post' );
    register_setting( 'auto_featured_image_group', 'auto_image_bg_image' );
    register_setting( 'auto_featured_image_group', 'auto_image_fontface' );
    register_setting( 'auto_featured_image_group', 'auto_image_fontsize' );
    register_setting( 'auto_featured_image_group', 'auto_image_text_color' );
    register_setting( 'auto_featured_image_group', 'auto_image_border' );
    register_setting( 'auto_featured_image_group', 'auto_image_border_color' );
    register_setting( 'auto_featured_image_group', 'auto_image_shadow' );
    register_setting( 'auto_featured_image_group', 'auto_image_shadow_color' );
    register_setting( 'auto_featured_image_group', 'auto_image_flickr_api' ); 
    register_setting( 'auto_featured_image_group', 'auto_image_flickr_license' );
    register_setting( 'auto_featured_image_group', 'auto_image_flickr_user' );
    register_setting( 'auto_featured_image_group', 'auto_image_unsplash_api' ); 
    register_setting( 'auto_featured_image_group', 'auto_image_unsplash_credit' ); 
    register_setting( 'auto_featured_image_group', 'auto_image_default_search_words' ); 
    register_setting( 'auto_featured_image_group', 'auto_image_set_first' );

    do_action('afift_pro_after_register_settings');
    }

add_action('admin_menu', 'afift_settings_trial');

function afift_css_head_trial() {
    if ((isset($_GET['page'])) && ($_GET['page'] == 'auto-featured-image-from-title-pro-trial.php')){ ?>
        <style type="text/css">
        #afift {margin-right:300px;}
        #afift_settings, #afift_info {background-color:#fff;border:#ccc 1px solid; padding:15px;}
        #afift_settings {float:left;width:100%;}
        #afift_info {float:right;margin-right:-280px;width:200px;}
        #afift_settings label {display:table;}
        #afift_settings .bg_group {text-align:center;padding:10px;width:240px;float:left;}
        .helpicon {position:relative;top:3px;}
        .helptip {position:relative;color:#00f;}
        .helptip span {display:none;}
        span.helptip span {width:400px;color:#000;}
        .helptip span img {display:block;}
        .helptip:hover span {display:block;position:absolute;top:0px;left:50px;background-color:#fff;border:#aaa 1px solid;padding:5px;z-index:9999;}
        span.helptip:hover span {left:15px;}
        #afift h2 {clear:both;}
        #afift input[type=submit] {clear:both;display:block;margin-bottom:30px;}
        .afift_alert {display:block;padding:5px;margin-bottom:10px;background-color:#ffa;border:solid 1px #999;font-weight:bold;}
        .bg_group_text input, input[type=checkbox] {float:left;position:relative;top:3px;}
        #afift_settings h3 {clear:both;display:block;}
		.nav-tab {margin-left:0;}
		.nav-tab-active, .nav-tab-active:hover {background-color:#fff;border-bottom:0px;}
        input[type=range] {width: 200px;}
        #rangevalue {display:inline;position:relative;top:-9px;border:solid 1px #999;padding:3px;background-color:#ccc;}
        .settings_group {background-color:#ddd;padding:10px;clear:both;}
		.settings_heading, .settings_heading_active {font-size:1.6em;color:#000;display:block;margin:20px 0;text-decoration:none;outline:0;}
		.settings_heading:before, .settings_heading_active:before {content:'';display:inline-block;position:relative;left:3px;border:6px solid transparent;border-left-color:#000;}
		.settings_heading_active:before {left:0;top:3px;border-color:#000 transparent transparent;}
        </style>
        <?php }
    }

add_action('admin_head', 'afift_css_head_trial');

function afift_bulk_run_trial($post_id) {

    $post = get_post( $post_id );

    // Don't run if not called
    if(!isset($_GET['afift_bulk']))
	return;

    if(isset($_GET['unset_images'])){
	    global $wpdb;
	    $wpdb->query( "DELETE FROM $wpdb->postmeta WHERE meta_key = '_thumbnail_id'" );
  		}

    $auto_image_post_types = get_option('auto_image_post_types');
    if(is_array($auto_image_post_types)){
	    $post_types = $auto_image_post_types;
        }
	else{
	    $post_types = array($auto_image_post_types);
        }

    // Set first extra query argument
    if(isset($_GET['afift_bulk_pages']) && ($_GET['afift_bulk_pages'] == 'yes')){
        $extra_arg_key = 'order';
	    $extra_arg_value = 'DESC';
	    $post_types = 'page';
		}
    else{
	    $extra_arg_key = 'cat';
	    $auto_image_categories = get_option('auto_image_categories');
	    if(is_array($auto_image_categories)){
            $pos = array_search('page', $post_types);
            unset($post_types[$pos]);
	        $cats = get_cat_ID( $auto_image_categories[0]);
		    $num_of_cats = count($auto_image_categories);
		    $i = 1;
		    while($i<$num_of_cats){
	            $cats .= ',' . get_cat_ID( $auto_image_categories[$i]);
	            $i++;
			    }
	        $extra_arg_value = '\'' . $cats . '\'';
		}
	    else {
		    $extra_arg_value = get_cat_ID( $auto_image_categories);
		    }
        }

    // Set second extra query argument
    if(!isset($_GET['override_disable'])){
        $extra_arg_key_2 = 'meta_query';
	    $extra_arg_value_2 = array(
		'relation' => 'OR',
		  //		array(
		  //			'key' => 'afift-disable',
		  //			'value' => 'yes',
		  //			'compare' => '!='
		  //            ),
		  //		array(
		  //			'key' => 'afift-disable',
		  //			'compare' => 'NOT EXISTS'
		  //            )
        array(
            'key' => 'afift-disable',
            'value' => 'no',
            'compare' => 'LIKE'
        ),
        array(
            'key' => 'afift-disable',
            'compare' => 'NOT EXISTS'
        )
		);
		}
    else{
	    $extra_arg_key_2 = 'orderby';
	    $extra_arg_value_2 = 'id';
		}
  
    $args = array(
	    'post_type' => $post_types,
	    'post_status' => array('publish', 'pending', 'draft', 'future', 'private'),
        'meta_query' => array(
		array(
			'key' => '_thumbnail_id',
			'value' => '?',
			'compare' => 'NOT EXISTS'
            )
	    ),
        $extra_arg_key => $extra_arg_value,
	  //	  	$extra_arg_key_2 => $extra_arg_value_2,
	    'posts_per_page' => 5,
    );

    $all_posts = new WP_Query($args);

    if(!isset($GLOBALS['num'])){$GLOBALS['num']=0;}

    while ($all_posts->have_posts()) {
        $all_posts->the_post();
        $post = $all_posts->post;
        if(!has_post_thumbnail($post->ID)){
            auto_featured_image_from_title_trial($post->ID);
            $GLOBALS['num']++;
            if($GLOBALS['num'] % 5 == 0){
                break;
                }
            }
        }
    if($GLOBALS['num'] != 5){$GLOBALS['done'] = 'done';}
    }

add_action('admin_init', 'afift_bulk_run_trial');

function afift_settings_page_trial() {

    global $afift_uploads_path;
    global $afift_images_path;
    global $afift_fonts_path;

    if((isset($GLOBALS['num'])) && ($GLOBALS['num'] % 5 == 0) && (!isset($GLOBALS['done'])) && (!isset($_GET['one-batch']))){ ?>
        <script type="text/javascript">
        <!--
		  setTimeout("location.href = '<?php echo site_url(); ?>/wp-admin/options-general.php?page=auto-featured-image-from-title-pro-trial.php&afift_bulk=1&tab=bulk<?php if(isset($_GET['afift_bulk_pages'])){echo '&afift_bulk_pages=' . $_GET['afift_bulk_pages'];} ?><?php if(isset($_GET['override_disable'])){echo '&override_disable=yes';} ?>&refresh=<?php echo $_GET['refresh'] ?>'", <?php echo $_GET['refresh']*1000 ?>);

        </script>
        <span class="afift_alert">Please wait...refreshing to generate more images.</span>
<?php }
    
    // Set Settings tab
    if(isset($_GET['tab'])){$tab=$_GET['tab'];}else{$tab='settings';}

    ?>

<div id="afift">

<h2>Auto Featured Image From Title Settings</h2>

<a class="nav-tab<?php if($tab=='settings'){echo ' nav-tab-active';} ?>" href="options-general.php?page=auto-featured-image-from-title-pro-trial.php">Settings</a>
<a class="nav-tab<?php if($tab=='fonts'){echo ' nav-tab-active';} ?>" href="options-general.php?page=auto-featured-image-from-title-pro-trial.php&tab=fonts">Manage Fonts</a>
<a class="nav-tab<?php if($tab=='images'){echo ' nav-tab-active';} ?>" href="options-general.php?page=auto-featured-image-from-title-pro-trial.php&tab=images">Manage Images</a>
<a class="nav-tab<?php if($tab=='bulk'){echo ' nav-tab-active';} ?>" href="options-general.php?page=auto-featured-image-from-title-pro-trial.php&tab=bulk">Bulk Process</a>
<a class="nav-tab<?php if($tab=='debug'){echo ' nav-tab-active';} ?>" href="options-general.php?page=auto-featured-image-from-title-pro-trial.php&tab=debug">Debug</a>

<br />

<div id="afift_settings">

<?php if($tab=='settings'){ ?>

<script type='text/javascript'>
<!--
function fix_input() {
    var x = document.getElementsByClassName('text_input'),
        i = x.length;
    while(i--) {
        x[i].value = x[i].value.replace(/ /g, '&#160;');
        }
    return true;
    }

function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e.style.display == 'block'){
        e.style.display = 'none';
        this.className = "settings_heading";
	    }
    else {
	    e.style.opacity = 0;
        e.style.display = 'block';

        (function fade() {
            var val = parseFloat(e.style.opacity);
            if (!((val += .05) > 1)) {
                e.style.opacity = val;
                requestAnimationFrame(fade);
                }
            })();
	    }
    }
function toggleClass(el){
	if(el.className == "settings_heading"){
		el.className = "settings_heading_active";
	} else {
		el.className = "settings_heading";
	}
}
//-->
</script>

<form method="post" action="options.php" onsubmit="fix_input()">
    <?php settings_fields( 'auto_featured_image_group' ); ?>

  <a href="#/" onclick="toggle_visibility('rules');toggleClass(this);" class="settings_heading">Image Generation Rules</a>

    <div id="rules" style="display:none">
  
        <p><label for="auto_image_post_types[]">Auto Generate Images for Post Types: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>CTRL/CMD+click to select multiple post types</span></span></label>
        <select name="auto_image_post_types[]" id="auto_image_post_types[]" multiple="multiple">
            <?php
            $post_types = get_post_types( '', 'names' );
            $auto_image_post_types = get_option('auto_image_post_types');
            foreach ( $post_types as $post_type ) {
                if (in_array($post_type, $auto_image_post_types, true)) {
                    $selected=' selected="selected"';
                    }
                else {
                    $selected='';
                    }
                echo '<option value="' . $post_type . '"' . $selected . '>' . $post_type . '</option>';
                }
            ?>
        </select></p>
  
        <p><label for="auto_image_categories[]">Auto Generate Images for Categories: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>CTRL/CMD+click to select multiple categories</span></span></label>
        <select name="auto_image_categories[]" id="auto_image_categories[]" multiple="multiple">
            <?php
            $auto_image_categories = get_option('auto_image_categories'); ?>
		    <option value="all_the_cats" <?php if (((!is_array($auto_image_categories) && $auto_image_categories=='all_the_cats')) || (is_array($auto_image_categories) && in_array('all_the_cats', $auto_image_categories, true))){echo 'selected="selected"';} ?>>ALL CATEGORIES</option>
            <?php $categories = get_categories();
				foreach ( $categories as $category ) {
                if (in_array($category->name, $auto_image_categories, true)) {
                    $selected=' selected="selected"';
                    }
                else {
                    $selected='';
                    }
                echo '<option value="' . $category->name . '"' . $selected . '>' . $category->name . '</option>';
                }
            ?>
        </select></p>
  
        <p><input type="checkbox" name="auto_image_default_disable" value="yes"<?php if(get_option("auto_image_default_disable")=='yes'){ echo ' checked="checked"';} ?> /><label for="auto_image_default_disable">Disable auto image generation by default <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>If you enable this, you will need to uncheck "Disable for this post" on the Edit Post screen of each post you want the plugin to create an image for.</span></span></label></p>

        <p><input type="checkbox" name="auto_image_admin_only" value="yes"<?php if(get_option("auto_image_admin_only")=='yes'){ echo ' checked="checked"';} ?> /><label for="auto_image_admin_only">Only run when logged in as admin <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>You <strong><em>may</em></strong> need to turn this off if your implementation includes front-end or user submitted posts</span></span></label></p>

        <p><label for="auto_image_hooks[]">Run on Wordpress Hooks: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span><strong>ADVANCED!!!</strong> Allows you some control over which Wordpress hook(s) will generate featured images, which may be necessary to customize depending on your implementation (usually pertaining to front-end or user submitted posts). <strong>WP_INSERT_POST is sufficient for 99% of implementations!!!</strong> But if your featured image isn't being generated correctly, because of data not yet being available from the database or from the submission, this might be the setting to change. CTRL/CMD+click to select multiple hooks.</span></span></label>
        <select name="auto_image_hooks[]" id="auto_image_hooks[]" multiple="multiple">
            <?php
            $hooks = array('wp_insert_post','save_post','edit_post','pre_post_update', 'post_updated','publish_post','the_post','pre_get_posts','xmlrpc_publish_post','publish_phone','publish_future_post');
            $auto_image_hooks = get_option('auto_image_hooks');
            foreach ( $hooks as $hook ) {
                if (in_array($hook, $auto_image_hooks, true)) {
                    $selected=' selected="selected"';
                    }
                else {
                    $selected='';
                    }
                echo '<option value="' . $hook . '"' . $selected . '>' . $hook . '</option>';
                }
            ?>
        </select></p>

    <p><input type="checkbox" name="auto_image_set_first" value="yes"<?php if(get_option("auto_image_set_first")=='yes'){ echo ' checked="checked"';} ?> /><label for="auto_image_set_first">Set the first image in a post as the featured image <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Sometimes this is necessary when a generated image is created, but for some reason it was not set as the post's featured image. You may want to use this bulk utility FIRST to generate featured images before turning this on. This will apply to ALL posts and pages that do not have featured images set, and may not give you the results you might expect!</span></span></label></p>
	  
    </div>

    <a href="#/" onclick="toggle_visibility('attributes');toggleClass(this);" class="settings_heading">Image Attribute Settings</a>

    <div id="attributes" style="display:none">

<script type='text/javascript'>
function showResizeField(obj) {
    if(obj[obj.selectedIndex].value == 'text') {
        document.getElementById('resize_to_text_div').style.opacity = 0;
        document.getElementById('resize_to_text_div').style.display = 'block';

        (function fade() {
            var val = parseFloat(document.getElementById('resize_to_text_div').style.opacity);
            if (!((val += .05) > 1)) {
                document.getElementById('resize_to_text_div').style.opacity = val;
                requestAnimationFrame(fade);
                }
            })();
        }
    else {
        document.getElementById('resize_to_text_div').style.display = 'none';
        }
    }

function showQualityField(obj) {
    if(obj[obj.selectedIndex].value == 'jpg') {
        document.getElementById('quality_div').style.opacity = 0;
        document.getElementById('quality_div').style.display = 'block';

        (function fade() {
            var val = parseFloat(document.getElementById('quality_div').style.opacity);
            if (!((val += .05) > 1)) {
                document.getElementById('quality_div').style.opacity = val;
                requestAnimationFrame(fade);
                }
            })();
        }
    else {
        document.getElementById('quality_div').style.display = 'none';
        }
    }
  
</script>
	  
    <p><label for="auto_image_resize">Resize BG Image By: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Determines how the background image (set below) is resized in order to fit the specified dimensions.</span></span></label>
    <select name="auto_image_resize" id="auto_image_resize" onChange="showResizeField(this)">
        <option value='yes'<?php if((get_option('auto_image_resize'))=='yes'){ echo " selected";} ?>>Resize</option>
        <option value='crop'<?php if((get_option('auto_image_resize'))=='crop'){ echo " selected";} ?>>Resize &amp; Crop</option>
	  <option value='squish'<?php if((get_option('auto_image_resize'))=='squish'){ echo " selected";} ?>>Squish/Stretch to Fit</option>
        <option value='shrink'<?php if((get_option('auto_image_resize'))=='shrink'){ echo " selected";} ?>>Shrink to Fit</option>
        <option value='text'<?php if((get_option('auto_image_resize'))=='text'){ echo " selected";} ?>>Resize to Fit Text</option>
        <option value='no'<?php if((get_option('auto_image_resize'))=='no'){ echo " selected";} ?>>Do not resize</option>
    </select></p>

        <div id="resize_to_text_div"<?php if((get_option('auto_image_resize'))!='text'){ echo ' style="display:none;"';} ?>>
		  <p><label for="auto_image_resize_to_text">As Needed, Resize Image: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Option to determine how to resize the image to fit the text. Depending on how this is set, it will then use the Image Width and/or Image Height settings as MAXIMUM width and/or height for the image.</span></span></label>
            <select name="auto_image_resize_to_text" id="auto_image_resize_to_text">
                <option value='horizontally'<?php if((get_option('auto_image_resize_to_text'))=='horizontally'){ echo " selected";} ?>>Horizontally</option>
                <option value='vertically'<?php if((get_option('auto_image_resize_to_text'))=='vertically'){ echo " selected";} ?>>Vertically</option>
                <option value='both'<?php if((get_option('auto_image_resize_to_text'))=='both'){ echo " selected";} ?>>Both</option>
	        </select>
	    </div>
	  
        <p><label for="auto_image_width">Image Width:</label>
        <input name="auto_image_width" type="text" size="5" id="auto_image_width" value="<?php form_option('auto_image_width'); ?>" /></p>
        <p><label for="auto_image_height">Image Height:</label>
        <input name="auto_image_height" type="text" size="5" id="auto_image_height" value="<?php form_option('auto_image_height'); ?>" /></p>
        <p><label for="auto_image_filetype">Image Filetype: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Choose JPG to be able to reduce file size by lowering image quality, or PNG to retain maximum image quality.</span></span></label>
            <select name="auto_image_filetype" id="auto_image_filetype" onChange="showQualityField(this)">
                <option value='jpg'<?php if((get_option('auto_image_filetype'))=='jpg'){ echo " selected";} ?>>JPG</option>
                <option value='png'<?php if((get_option('auto_image_filetype'))=='png'){ echo " selected";} ?>>PNG</option>
	        </select>

		<div id="quality_div"<?php if((get_option('auto_image_filetype'))!='jpg'){ echo ' style="display:none;"';} ?>>
			<p><label for="auto_image_quality">Image Quality: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>(a higher number means higher quality, but a bigger file size)</span></span></label>
	        <input name="auto_image_quality" type="range" id="auto_image_quality" value="<?php form_option('auto_image_quality'); ?>" min="1" max="100" onchange="rangevalue.value=value" />
	        <output id="rangevalue"><?php form_option('auto_image_quality'); ?></output></p>
	    </div>
		  
    </div>

    <a href="#/" onclick="toggle_visibility('text');toggleClass(this);" class="settings_heading">Text Settings</a>

    <div id="text" style="display:none">

    <p><label for="auto_image_write_text">Write Text onto Generated Image:</label>
    <select name="auto_image_write_text" id="auto_image_write_text" onChange="toggle_visibility('text_settings');">
        <option value='yes'<?php if((get_option('auto_image_write_text'))=='yes'){ echo " selected";} ?>>Yes</option>
        <option value='no'<?php if((get_option('auto_image_write_text'))=='no'){ echo " selected";} ?>>No</option>
    </select></p>

    <div id="text_settings"<?php if((get_option('auto_image_write_text'))=='no'){ echo ' style="display:none;"';}else{echo 'style="display:block;"';} ?>>

<script type='text/javascript'>
function showCustomField(obj) {
    if(obj[obj.selectedIndex].value == 'custom_field') {
        document.getElementById('custom_field_div').style.opacity = 0;
        document.getElementById('custom_field_div').style.display = 'block';

        (function fade() {
            var val = parseFloat(document.getElementById('custom_field_div').style.opacity);
            if (!((val += .05) > 1)) {
                document.getElementById('custom_field_div').style.opacity = val;
                requestAnimationFrame(fade);
                }
            })();
        }
    else {
        document.getElementById('custom_field_div').style.display = 'none';
        }
    }
</script>

        <p><label for="auto_image_text">Text to Write onto Generated Images:</label>
        <select name="auto_image_text" id="auto_image_text" onChange="showCustomField(this)">
            <option value='title'<?php if((get_option('auto_image_text'))=='title'){ echo " selected";} ?>>Post Title</option>
            <option value='excerpt'<?php if((get_option('auto_image_text'))=='excerpt'){ echo " selected";} ?>>Post Excerpt</option>
            <option value='content'<?php if((get_option('auto_image_text'))=='content'){ echo " selected";} ?>>Post Content</option>
<!--            <option value='author'<?php if((get_option('auto_image_text'))=='author'){ echo " selected";} ?>>Post Author</option> -->
            <option value='date'<?php if((get_option('auto_image_text'))=='date'){ echo " selected";} ?>>Post Date</option>
            <option value='custom_field'<?php if((get_option('auto_image_text'))=='custom_field'){ echo " selected";} ?>>Custom Field...</option>
        </select></p>

        <div id="custom_field_div"<?php if((get_option('auto_image_text'))!='custom_field'){ echo ' style="display:none;"';} ?>>
            <p><label for="auto_image_custom_field">Custom Field Key:</label>
            <input name="auto_image_custom_field" type="text" size="10" id="auto_image_custom_field" value="<?php form_option('auto_image_custom_field'); ?>" /></p>
        </div>

        <div id="content_length_div">
            <p><label for="auto_image_content_length">Maximum Text Length: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Set to the maximum number of characters you want to write to the image. The higher you make this number, the smaller you will want to make the font size, or else the plugin will slow down and likely timeout.</span></span></label>
            <input name="auto_image_content_length" type="text" size="10" id="auto_image_content_length" value="<?php form_option('auto_image_content_length'); ?>" /></p>
        </div>

        <p><label for="auto_image_before_text">Additional Character(s) Before Text: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>You may need to use &amp;#160; to retain spaces at the beginning or the end</span></span></label>
        <input name="auto_image_before_text" type="text" size="20" id="auto_image_before_text" class="text_input" value="<?php form_option('auto_image_before_text'); ?>" /></p>
        <p><label for="auto_image_after_text">Additional Character(s) After Text: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>You may need to use &amp;#160; to retain spaces at the beginning or the end</span></span></label>
        <input name="auto_image_after_text" type="text" size="20" id="auto_image_after_text" class="text_input" value="<?php form_option('auto_image_after_text'); ?>" /></p>

        <p><label for="auto_image_remove_linebreaks">Remove Linebreaks: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Decide whether you would like to keep or remove the linebreaks when writing text to the featured images.</span></span></label>
        <select name="auto_image_remove_linebreaks" id="auto_image_remove_linebreaks">
            <option value='yes'<?php if((get_option('auto_image_remove_linebreaks'))=='yes'){ echo " selected";} ?>>Yes</option>
            <option value='no'<?php if((get_option('auto_image_remove_linebreaks'))=='no'){ echo " selected";} ?>>No</option>
        </select></p>
		
		<p><label for="auto_author_name">Show Author Name <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Decide whether you would like to show author name to the featured images.</span></span></label>
        <select name="auto_author_name" id="auto_author_name">
            <option value='yes'<?php if((get_option('auto_author_name'))=='yes'){ echo " selected";} ?>>Yes</option>
            <option value='no'<?php if((get_option('auto_author_name'))=='no'){ echo " selected";} ?>>No</option>
        </select></p>

	    <p><label for="auto_image_text_x_position">Horizontal Text Position:</label>
        <select name="auto_image_text_x_position" id="auto_image_text_x_position">
            <option value='left'<?php if((get_option('auto_image_text_x_position'))=='left'){ echo " selected";} ?>>Left</option>
            <option value='center'<?php if((get_option('auto_image_text_x_position'))=='center'){ echo " selected";} ?>>Center</option>
            <option value='right'<?php if((get_option('auto_image_text_x_position'))=='right'){ echo " selected";} ?>>Right</option>
        </select></p>

        <p><label for="auto_image_text_y_position">Vertical Text Position:</label>
        <select name="auto_image_text_y_position" id="auto_image_text_y_position">
            <option value='top'<?php if((get_option('auto_image_text_y_position'))=='top'){ echo " selected";} ?>>Top</option>
            <option value='center'<?php if((get_option('auto_image_text_y_position'))=='center'){ echo " selected";} ?>>Center</option>
            <option value='bottom'<?php if((get_option('auto_image_text_y_position'))=='bottom'){ echo " selected";} ?>>Bottom</option>
        </select></p>

        <p><label for="auto_image_top_padding">Top Text Padding (in pixels):</label>
        <input name="auto_image_top_padding" type="text" size="5" id="auto_image_top_padding" value="<?php form_option('auto_image_top_padding'); ?>" /></p>
        <p><label for="auto_image_bottom_padding">Bottom Text Padding (in pixels):</label>
        <input name="auto_image_bottom_padding" type="text" size="5" id="auto_image_bottom_padding" value="<?php form_option('auto_image_bottom_padding'); ?>" /></p>
        <p><label for="auto_image_left_padding">Left Text Padding (in pixels):</label>
        <input name="auto_image_left_padding" type="text" size="5" id="auto_image_left_padding" value="<?php form_option('auto_image_left_padding'); ?>" /></p>
        <p><label for="auto_image_right_padding">Right Text Padding (in pixels):</label>
        <input name="auto_image_right_padding" type="text" size="5" id="auto_image_right_padding" value="<?php form_option('auto_image_right_padding'); ?>" /></p>

        <p><label for="auto_image_text_transform">Transform Text:</label>
        <select name="auto_image_text_transform" id="auto_image_text_transform">
            <option value='none'<?php if((get_option('auto_image_text_transform'))=='none'){ echo " selected";} ?>>None</option>
            <option value='uppercase'<?php if((get_option('auto_image_text_transform'))=='uppercase'){ echo " selected";} ?>>Uppercase</option>
            <option value='lowercase'<?php if((get_option('auto_image_text_transform'))=='lowercase'){ echo " selected";} ?>>Lowercase</option>
            <option value='capitalize'<?php if((get_option('auto_image_text_transform'))=='capitalize'){ echo " selected";} ?>>Capitalize</option>
        </select></p>

        <p><label for="auto_image_text_color">Text Color:</label>
        <input name="auto_image_text_color" type="text" value="<?php form_option('auto_image_text_color'); ?>" class="my-color-field" /></p>

    <?php do_action( 'afift_pro_after_text_color_setting' ); ?>

    <p><label for="auto_image_fontface">Font: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /> <span>Select "RANDOM" to use a random font from all installed fonts</span></span> <small class="helptip">Show fonts<span>
    <?php
    $afift_font_image_path = $afift_fonts_path . 'images';
    if(!file_exists($afift_font_image_path)){
        mkdir($afift_font_image_path, 0755);
        }

    $file_display = array('ttf');

    $dir_contents = scandir( $afift_fonts_path );
    foreach ( $dir_contents as $file ) {
        $file_type = strtolower( end(( explode('.', $file ) )) );
        if ( ($file !== '.') && ($file !== '..') && (in_array( $file_type, $file_display)) ) {
            if(!file_exists($afift_font_image_path . '/' . $file . '.jpg')){

                $words = $file;
                $bbox = imagettfbbox(30, 0, $afift_fonts_path . $file, $words);

                //calculate x baseline
                if($bbox[0] >= -1) {
                    $bbox['x'] = abs($bbox[0] + 1) * -1;
                    }
                else {
                    //$bbox['x'] = 0;
                    $bbox['x'] = abs($bbox[0] + 2);
                    }

                //calculate actual text width
                $bbox['width'] = abs($bbox[2] - $bbox[0]);
                if($bbox[0] < -1) {
                    $bbox['width'] = abs($bbox[2]) + abs($bbox[0]) - 1;
                    }

                //calculate y baseline
                $bbox['y'] = abs($bbox[5] + 1);

                //calculate actual text height
                $bbox['height'] = abs($bbox[7]) - abs($bbox[1]);
                if($bbox[3] > 0) {
                    $bbox['height'] = abs($bbox[7] - $bbox[1]) - 1;
                    }

                $new_font_img = imagecreatetruecolor($bbox['width'], $bbox['height']);
                $font_bg_color = imagecolorallocate( $new_font_img, 255, 255, 255);
                imagefill($new_font_img, 0, 0, $font_bg_color);
                $font_color = imagecolorallocate( $new_font_img, 0, 0, 0);
                imagettftext($new_font_img, 30, 0, $bbox['x'], $bbox['y'], $font_color, $afift_fonts_path . '/' . $file, $words);

                // Save the image
                $font_img_url = $afift_font_image_path . '/' . $file . '.jpg';
                imagejpeg( $new_font_img, $font_img_url);
                imagecolordeallocate( $new_font_img, $font_color );
                }
            echo '<img src="',plugins_url(),'/auto-featured-image-from-title-uploads/fonts/images/',$file,'.jpg" alt="', $file, '"/>';
            }
        }
    ?>
    </span></small></label>
        <select name="auto_image_fontface" id="auto_image_fontface">
        <?php
        $file_display = array('ttf');

        $dir_contents = scandir( $afift_fonts_path );
        foreach ( $dir_contents as $file ) {
            $file_type = strtolower( end(( explode('.', $file ) )) );
            if ( ($file !== '.') && ($file !== '..') && (in_array( $file_type, $file_display)) ) {
                if((get_option('auto_image_fontface'))==$file){$selected=' selected';}else{$selected='';}
                echo '<option value="', $file, '"',$selected,'>',$file,'</option>';
                }
            }
        ?>
        <option value="random_afift_font"<?php if(get_option("auto_image_fontface")=='random_afift_font'){ echo ' selected';} ?>>RANDOM</option>
        </select></p>

        <p><label for="auto_image_fontsize">Font Size: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>In pixels. If you have a lot of text to write to each image, you may want to decrease this considerably, or else the plugin will attempt to decrease it for you, and the script will timeout.</span></span></label>
        <input name="auto_image_fontsize" type="text" size="5" id="auto_image_fontsize" value="<?php form_option('auto_image_fontsize'); ?>" /></p>

        <p><label for="auto_image_border">Apply Text Border:</label>
        <select name="auto_image_border" id="auto_image_border" onChange="toggle_visibility('border_color');">
            <option value='yes'<?php if((get_option('auto_image_border'))=='yes'){ echo " selected";} ?>>Yes</option>
            <option value='no'<?php if((get_option('auto_image_border'))=='no'){ echo " selected";} ?>>No</option>
        </select></p>

    <div id="border_color"<?php if((get_option('auto_image_border'))=='no'){ echo ' style="display:none;"';}else{echo 'style="display:block;"';} ?>>
        <p><label for="auto_image_border_color">Border Color:</label>
        <input name="auto_image_border_color" type="text" value="<?php form_option('auto_image_border_color'); ?>" class="my-color-field" /></p>
    </div>

        <p><label for="auto_image_shadow">Apply Text Shadow:</label>
        <select name="auto_image_shadow" id="auto_image_shadow" onChange="toggle_visibility('shadow_color');">
            <option value='yes'<?php if((get_option('auto_image_shadow'))=='yes'){ echo " selected";} ?>>Yes</option>
            <option value='no'<?php if((get_option('auto_image_shadow'))=='no'){ echo " selected";} ?>>No</option>
        </select></p>

    <div id="shadow_color"<?php if((get_option('auto_image_shadow'))=='no'){ echo ' style="display:none;"';}else{echo 'style="display:block;"';} ?>>
        <p><label for="auto_image_shadow_color">Shadow Color:</label>
        <input name="auto_image_shadow_color" type="text" value="<?php form_option('auto_image_shadow_color'); ?>" class="my-color-field" /></p>
    </div>

    </div>

    </div>
	  
    <?php do_action( 'afift_pro_after_text_settings' ); ?>

    <a href="#/" onclick="toggle_visibility('backgrounds');toggleClass(this);" class="settings_heading">Background Settings</a>

    <div id="backgrounds" style="display:none">

    <p><span class="bg_group_text"><input type="checkbox" name="auto_image_bg_image_from_category" id="bg_group_from_category" value="yes"<?php if(get_option("auto_image_bg_image_from_category")=='yes'){ echo ' checked="checked"';} ?> /><label for="bg_group_from_category">Use a random image from the same category (if there is one) <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Attempts to match the post's category with a category of background images, and if there is a match, uses a random image from that category as the  background for the auto generated featured image. If it cannot find a category match, it will fall back to other background settings below.</span></span></label></span></p>

    <p><span class="bg_group_text"><input type="checkbox" name="auto_image_bg_image_from_post" id="bg_group_from_post" value="yes"<?php if(get_option("auto_image_bg_image_from_post")=='yes'){ echo ' checked="checked"';} ?> /><label for="bg_group_from_post">Use the first image in the post (if there is one) <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Opts to use the first image in a post as the background for the auto generated featured image. If it cannot find an image in the post, it will fall back to other background settings below.</span></span></label></span></p>

    <p><span class="bg_group_text"><input type="radio" name="auto_image_bg_image" id="bg_group_random" value="random_afift_bg"<?php if(get_option("auto_image_bg_image")=='random_afift_bg'){ echo ' checked="checked"';} ?> /><label for="bg_group_random">Use a Random Background Image from all installed images</label></span></p>

    <?php
    $file_display = array('jpg', 'jpeg', 'png', 'gif');

    $afift_image_subdirs = glob($afift_images_path . '*' , GLOB_ONLYDIR);
    foreach ( $afift_image_subdirs as $afift_image_subdir ) {
        if ( file_exists( $afift_image_subdir ) == false ) {
            echo 'Directory \'', $afift_images_path, '\' not found!';}
        else {
            $afift_image_subdir_folder = str_replace($afift_images_path, '', $afift_image_subdir);
            if($afift_image_subdir_folder != 'fonts'){
                echo '<h3>', $afift_image_subdir_folder ,' Backgrounds</h3>';
                echo '<span class="bg_group_text"><input type="radio" name="auto_image_bg_image" id="bg_group_random_', $afift_image_subdir_folder ,'" value="random_', $afift_image_subdir_folder ,'"'.((get_option("auto_image_bg_image")=='random_' . $afift_image_subdir_folder)?' checked="checked"':"").' /><label for="bg_group_random_', $afift_image_subdir_folder ,'">Random image from ', $afift_image_subdir_folder, ' Backgrounds</label></span>';
                $dir_contents = scandir( $afift_image_subdir );
                foreach ( $dir_contents as $file ) {
                    $file_type = strtolower( end(( explode('.', $file ) )) );
                    if ( ($file !== '.') && ($file !== '..') && (in_array( $file_type, $file_display)) ) {
                        if(!file_exists($afift_images_path . $afift_image_subdir_folder . '/resized/' . $file . '.240x120.jpg')){
                            $afift_image_filepath = $afift_images_path . $afift_image_subdir_folder . '/' . $file;
                            $ext = strtolower(pathinfo($afift_image_filepath, PATHINFO_EXTENSION));
                            if($ext == 'png'){
                                $new_featured_img = imagecreatefrompng($afift_image_filepath);
                                }
                             elseif($ext == 'gif'){
                                 $new_featured_img = imagecreatefromgif($afift_image_filepath);
                                 }
                             else{
                                 $new_featured_img = imagecreatefromjpeg($afift_image_filepath);
                                 }
                            $width = imagesx($new_featured_img);
                            $height = imagesy($new_featured_img);

                            $original_aspect = $width / $height;

                            if ( $original_aspect >= 2 ){
                                // If image is wider than thumbnail (in aspect ratio sense)
                                $new_height = 120;
                                $new_width = $width / ($height / 120);
                                }
                            else {
                                // If the thumbnail is wider than the image
                                $new_width = 240;
                                $new_height = $height / ($width / 240);
                                }

                            $auto_image = imagecreatetruecolor( 240, 120 );

                            // Resize and crop
                            imagecopyresampled(
                                $auto_image,
                                $new_featured_img,
                                // Center the image horizontally
                                0 - ($new_width - 240) / 2,
                                // Center the image vertically
                                0 - ($new_height - 120) / 2,
                                0, 0,
                                $new_width, $new_height,
                                $width, $height);

                            // Save the image
                            $afift_resized_image_path = $afift_images_path . $afift_image_subdir_folder . '/resized/';
                            if(!file_exists($afift_resized_image_path)){
                                mkdir($afift_resized_image_path, 0755);
                                }
                            $newimg = $afift_resized_image_path . basename( $file) . '.240x120.jpg';
                            imagejpeg( $auto_image, $newimg, 75 );
                            }

					  //                        if($file!=''){
                            echo '<span class="bg_group"><label for="' . $afift_image_subdir_folder . '/', $file ,'"><img src="', plugins_url() . '/auto-featured-image-from-title-uploads/images/' . $afift_image_subdir_folder . '/resized/' . $file , '.240x120.jpg" width="240" height="120" alt="', $file, '"/></label><input type="radio" id="' . $afift_image_subdir_folder . '/', $file ,'" name="auto_image_bg_image" value="' . $afift_image_subdir_folder . '/', $file ,'"'.((get_option("auto_image_bg_image")==$afift_image_subdir_folder . "/" . $file)?'checked="checked"':"").' /></span>';
					  //						    }
                        }
                    }
                }
            }
        }
    ?>
    </p>

    <div class="settings_group">
    <h3>Flickr Background (experimental)</h3>
    <p><span class="bg_group_text"><input type="radio" name="auto_image_bg_image" id="random_flickr_bg" value="random_flickr_bg"<?php if(get_option("auto_image_bg_image")=='random_flickr_bg'){ echo ' checked="checked"';} ?> /><label for="random_flickr_bg">Random Flickr Background Image (searches for one based on your post title)</label></span></p>

    <p><label for="auto_image_flickr_api">Flickr API Key (<a href="https://www.flickr.com/services/api/keys/apply/">get yours here</a>):</label> 
    <input name="auto_image_flickr_api" type="text" id="auto_image_flickr_api" value="<?php form_option('auto_image_flickr_api'); ?>" /></p> 

    <p><label for="auto_image_flickr_license">Only use Flickr photos that are commercially licensed:</label> 
    <select name="auto_image_flickr_license" id="auto_image_flickr_license"> 
        <option value='4,5,7'<?php if((get_option('auto_image_flickr_license'))=='4,5,7'){ echo " selected";} ?>>Yes</option> 
        <option value='1,2,4,5,7'<?php if((get_option('auto_image_flickr_license'))=='1,2,4,5,7'){ echo " selected";} ?>>No</option> 
    </select></p>

<!--
    <p><label for="auto_image_flickr_user">Only from Flickr User: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Enter the username of a Flickr user you would like to limit possible images to. Leave blank to search all Flickr images.</span></span></label> 
    <input name="auto_image_flickr_user" type="text" id="auto_image_flickr_user" value="<?php form_option('auto_image_flickr_user'); ?>" /></p> 
-->

    </div>
  
    <div class="settings_group">
    <h3>Unsplash Background (experimental)</h3>
    <p><span class="bg_group_text"><input type="radio" name="auto_image_bg_image" id="random_unsplash_bg" value="random_unsplash_bg"<?php if(get_option("auto_image_bg_image")=='random_unsplash_bg'){ echo ' checked="checked"';} ?> /><label for="random_unsplash_bg">Random Unsplash Background Image (searches for one based on your post title or search terms you specify)</label></span></p>
    <p><label for="auto_image_unsplash_api">Unsplash Application ID (<a href="https://unsplash.com/developers">get yours here</a>):</label> 
    <input name="auto_image_unsplash_api" type="text" id="auto_image_unsplash_api" value="<?php form_option('auto_image_unsplash_api'); ?>" /></p> 
	<p><label for="auto_image_unsplash_credit">Give credit to Unsplash Photographer: <span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Adds and/or replaces the featured image excerpt/caption with a link to the photographer's profile. This is necessary if you generate more than 50 featured images per hour, as you will need to apply to remove Unsplash's image search limit.</span></span></label>
    <select name="auto_image_unsplash_credit" id="auto_image_unsplash_credit"> 
        <option value='yes'<?php if((get_option('auto_image_unsplash_credit'))=='yes'){ echo " selected";} ?>>Yes</option> 
        <option value='no'<?php if((get_option('auto_image_unsplash_credit'))=='no'){ echo " selected";} ?>>No</option> 
    </select></p>
	<p><label for="auto_image_default_search_words">Default Image Search Term(s):</label> 
    <input name="auto_image_default_search_words" type="text" id="auto_image_default_search_words" value="<?php form_option('auto_image_default_search_words'); ?>" /></p> 
  </div>
  
    <p><span class="bg_group_text"><input type="radio" name="auto_image_bg_image" id="No_BG_Img" value=""<?php if(get_option("auto_image_bg_image")==''){ echo ' checked="checked"';} ?> /><label for="No_BG_Img">Use a background color instead of an image</label></span></p>
    <p><label for="auto_image_bg_color">Background Color:</label>
        <input name="auto_image_bg_color" type="text" value="<?php form_option('auto_image_bg_color'); ?>" class="my-color-field" /></p>

    </div>
  
    <p><input type="submit" value="Save Changes" /></p>

</form>

<?php }elseif($tab=='fonts'){

if(isset($_FILES['upload_font'])){
    $afift_font = $_FILES['upload_font'];
    $afift_font_full_path = $afift_fonts_path . basename( $_FILES['upload_font']['name'] );

    $allowed = array('ttf');
    $ext = strtolower(pathinfo($afift_font_full_path, PATHINFO_EXTENSION));
    if(!in_array($ext,$allowed) ) {
        echo '<span class="afift_alert">Your font <em>must</em> be a ttf font.</span>';
        }
    elseif(file_exists($afift_font_full_path)){
        echo '<span class="afift_alert">Upload failed. A font with that name already exists. If you would still like to upload this font, rename the file and try again.</span>';
        }
    elseif(move_uploaded_file( $_FILES['upload_font']['tmp_name'], $afift_font_full_path )){
        $afift_alert="Fonts Uploaded.";
        }
    else {
        echo '<span class="afift_alert">Could not upload file. Something went wrong. Make sure the fonts directory is writable and try again. If this error persists, contact <a href="http://designsbychris.com">the plugin author</a>.';
        }
    }
elseif(isset($_GET['delete'])){
    if(unlink($afift_fonts_path . $_GET['delete'])){
        $afift_alert = $_GET['delete'] . ' deleted.';
        }
    else {
        $afift_alert = 'Could not delete ' . $_GET['delete'] . '. Try to delete it via FTP.';
        }
    }
 ?>

<h2>Upload Fonts</h2>

<?php if(isset($afift_alert)){
    echo '<span class="afift_alert">' . $afift_alert . '</span>';
    } ?>

<p>Uploaded fonts <em>must</em> be .ttf fonts. If you have a font that you would like to upload that is not .ttf, you can <a href="http://www.freefontconverter.com/">try converting it to .ttf online</a>.</p>

<form action="options-general.php?page=auto-featured-image-from-title-pro-trial.php&tab=fonts" method="post" enctype="multipart/form-data">
<input type="file" accept=".ttf" id="upload_font" name="upload_font" /><br /><br />
<input type="submit" value="Upload Font" />
</form>

<hr />

<h2>Delete Fonts</h2>
<p>Deleting fonts is permanent, so use the following resource carefully.</p>

<p>
<?php
$file_display = array('ttf');

$dir_contents = scandir( $afift_fonts_path );
foreach ( $dir_contents as $file ) {
    $file_type = strtolower( end(( explode('.', $file ) )) );
    if ( ($file !== '.') && ($file !== '..') && (in_array( $file_type, $file_display)) ) {
        if((get_option('auto_image_fontface'))==$file){
            echo '<strong>Active font: ' . $file . ' (can\'t delete the active font)</strong><br />';
            }
        else{
            echo '<a href="options-general.php?page=auto-featured-image-from-title-pro-trial.php&amp;tab=fonts&amp;delete=' . $file . '">Delete ' . $file . '</a><br />';
            }
        }
    }
?>
</p>

<?php }elseif($tab=='images'){

if(isset($_FILES['upload_image'])){
    $afift_image = $_FILES['upload_image'];
    $afift_image_subdir = $_POST['subdir'];
    if($afift_image_subdir=='newsubdir'){
        $afift_image_subdir=$_POST['new-cat'];
        mkdir($afift_images_path . '/' . $afift_image_subdir, 0755);
        }
    $afift_image_filepath = $afift_images_path . '/' . $afift_image_subdir . '/' . basename( $_FILES['upload_image']['name'] );

    $allowed = array('gif','png','jpg','jpeg');
    $ext = strtolower(pathinfo($afift_image_filepath, PATHINFO_EXTENSION));
    if(!in_array($ext,$allowed) ) {
        echo '<span class="afift_alert">You may only upload image files (.gif, .png, .jpg, or .jpeg).</span>';
        }
    elseif(file_exists($afift_image_filepath)){
        if($_POST['overwrite']=='yes'){
            unlink($afift_image_filepath);
            unlink($afift_images_path . '/' . $afift_image_subdir . '/resized/' . basename( $_FILES['upload_image']['name'] ) . '.240x120.jpg');
            if(move_uploaded_file( $_FILES['upload_image']['tmp_name'], $afift_image_filepath )){
                $afift_alert="Images Uploaded.";
                }
            else {
                echo '<span class="afift_alert">Could not upload file. Something went wrong. Make sure the images directory is writable and try again. This error could have been triggered if the script was unable to overwrite an existing file. Try to delete the file manually via FTP. If this error persists, contact <a href="http://designsbychris.com">the plugin author</a>.';
                }
            }
        else {
            echo '<span class="afift_alert">Upload failed. An image with that name already exists. If you would still like to upload this image, rename the file and try again, or check the "Overwrite" checkbox.</span>';
            echo 'Overwrite: ' . $_POST['overwrite'];
            }
        }
    elseif(move_uploaded_file( $_FILES['upload_image']['tmp_name'], $afift_image_filepath )){
        $afift_alert="Images Uploaded.";
        }
    else {
        echo '<span class="afift_alert">Could not upload file. Something went wrong. Make sure the images directory is writable and try again. If this error persists, contact <a href="http://designsbychris.com">the plugin author</a>.';
        }
    }
elseif(isset($_GET['delete'])){
    if($_GET['delete']=='folder'){
        if((rmdir($afift_images_path . $_GET['folder'] . '/resized/')) && (rmdir($afift_images_path . $_GET['folder']))){
            $afift_alert = $_GET['folder'] . ' deleted.';
            }
        else {
            $afift_alert = 'Could not delete ' . $_GET['folder'] . '. Make sure the directory is empty and try again.';
            }
        }
    else {
        if((unlink($afift_images_path . $_GET['folder'] . '/resized/' . $_GET['delete'] . '.240x120.jpg')) && (unlink($afift_images_path . $_GET['folder'] . '/' . $_GET['delete']))){
            $afift_alert = $_GET['delete'] . ' deleted.';
            }
        else {
            $afift_alert = 'Could not delete ' . $_GET['delete'] . '. Try to delete it via FTP.';
            }
        }
    }
?>

<h2>Upload Images</h2>

<?php if(isset($afift_alert)){
    echo '<span class="afift_alert">' . $afift_alert . '</span>';
    } ?>

<form action="options-general.php?page=auto-featured-image-from-title-pro-trial.php&tab=images" method="post" enctype="multipart/form-data">

<script type='text/javascript'>
function showDiv(obj) {
    if(obj[obj.selectedIndex].value == 'newsubdir') {
        document.getElementById('new_cat_name').style.opacity = 0;
        document.getElementById('new_cat_name').style.display = 'block';

        (function fade() {
            var val = parseFloat(document.getElementById('new_cat_name').style.opacity);
            if (!((val += .05) > 1)) {
                document.getElementById('new_cat_name').style.opacity = val;
                requestAnimationFrame(fade);
                }
            })();
        }
    else {
        document.getElementById('new_cat_name').style.display = 'none';
        }
    }
</script>

<p>
<label for="subdir">Image Category:</label>
<select name="subdir" id="subdir" onChange="showDiv(this)">

<?php

    $afift_image_subdirs = glob($afift_images_path . '*' , GLOB_ONLYDIR);
    foreach ( $afift_image_subdirs as $afift_image_subdir ) {
        if ( file_exists( $afift_image_subdir ) == false ) {
            echo 'Directory \'', $afift_images_path, '\' not found!';}
        else {
            $afift_image_subdir_folder = str_replace($afift_images_path, '', $afift_image_subdir);
            if($afift_image_subdir_folder != 'fonts'){
                echo '<option value="', $afift_image_subdir_folder ,'">', $afift_image_subdir_folder ,'</option>';
                }
            }
        }
    ?>
<option value="newsubdir">New Category...</option>
</select>
</p>

<div id="new_cat_name" style="display:none;">
<p>
<label for="new-cat">New Category Name:</label>
<input type="text" id="new-cat" name="new-cat" />
</p>
</div>

<p>
<input type="checkbox" id="overwrite" name="overwrite" value="yes" />
<label for="overwrite">Overwrite existing image (if file name already exists)</label>
</p>

<input type="file" accept="image/*" id="upload_image" name="upload_image" /><br /><br />
<input type="submit" value="Upload Image" />
</form>

<hr />

<h2>Delete Images</h2>
<p>Deleting images is permanent, so use the following resource carefully.</p>

<p>
<?php
$file_display = array('jpg', 'jpeg', 'png', 'gif');

$afift_image_subdirs = glob($afift_images_path . '*' , GLOB_ONLYDIR);
foreach ( $afift_image_subdirs as $afift_image_subdir ) {
    if ( file_exists( $afift_image_subdir ) == false ) {
        echo 'Directory \'', $afift_images_path, '\' not found!';}
    else {
        $afift_image_subdir_folder = str_replace($afift_images_path, '', $afift_image_subdir);
        echo '<h3>', $afift_image_subdir_folder ,' Backgrounds</h3>';
        echo '<a href="options-general.php?page=auto-featured-image-from-title-pro-trial.php&amp;tab=images&amp;folder=' . $afift_image_subdir_folder . '&amp;delete=folder">Delete ' . $afift_image_subdir_folder . ' category</a> (it must be empty first!)<br /><br />';
        $dir_contents = scandir( $afift_image_subdir );
        foreach ( $dir_contents as $file ) {
            $file_type = strtolower( end(( explode('.', $file ) )) );
            if ( ($file !== '.') && ($file !== '..') && (in_array( $file_type, $file_display)) ) {
                if(!file_exists($afift_images_path . $afift_image_subdir_folder . '/resized/' . $file . '.240x120.jpg')){
                    $afift_image_filepath = $afift_images_path . $afift_image_subdir_folder . '/' . $file;
                    $ext = strtolower(pathinfo($afift_image_filepath, PATHINFO_EXTENSION));
                    if($ext == 'png'){
                        $new_featured_img = imagecreatefrompng($afift_image_filepath);
                        }
                    elseif($ext == 'gif'){
                        $new_featured_img = imagecreatefromgif($afift_image_filepath);
                        }
                    else{
                        $new_featured_img = imagecreatefromjpeg($afift_image_filepath);
                        }
                    $width = imagesx($new_featured_img);
                    $height = imagesy($new_featured_img);

                    $original_aspect = $width / $height;

                    if ( $original_aspect >= 2 ){
                        // If image is wider than thumbnail (in aspect ratio sense)
                        $new_height = 120;
                        $new_width = $width / ($height / 120);
                        }
                    else {
                        // If the thumbnail is wider than the image
                        $new_width = 240;
                        $new_height = $height / ($width / 240);
                        }

                    $auto_image = imagecreatetruecolor( 240, 120 );

                    // Resize and crop
                    imagecopyresampled(
                        $auto_image,
                        $new_featured_img,
                        // Center the image horizontally
                        0 - ($new_width - 240) / 2,
                        // Center the image vertically
                        0 - ($new_height - 120) / 2,
                        0, 0,
                        $new_width, $new_height,
                        $width, $height);

                    // Save the image
                    $afift_resized_image_path = $afift_images_path . $afift_image_subdir_folder . '/resized/';
                    if(!file_exists($afift_resized_image_path)){
                        mkdir($afift_resized_image_path, 0755);
                        }
                    $newimg = $afift_resized_image_path . basename( $file) . '.240x120.jpg';
                    imagejpeg( $auto_image, $newimg, 75 );
                    }

			  //                if($file!='blank.jpg'){
                    echo '<img src="', plugins_url() . '/auto-featured-image-from-title-uploads/images/' . $afift_image_subdir_folder . '/resized/' . $file , '.240x120.jpg" width="240" height="120" alt="', $file, '"/><br />';
                    if( get_option("auto_image_bg_image" ) == $afift_image_subdir_folder . "/" . $file){
                        echo '<strong>Active background: ' . $file . ' (can\'t delete active background)</strong><br /><br />';
                        }
                    else {
                        echo '<a href="options-general.php?page=auto-featured-image-from-title-pro-trial.php&amp;tab=images&amp;folder=' . $afift_image_subdir_folder . '&amp;delete=' . $file . '">Delete ' . $file . '</a><br /><br />';
                        }
			  //                    }
                }
            }
        }
    }
?>
</p>

<?php }elseif($tab=='bulk'){ ?>

<h2>Bulk Auto Generate Featured Images for All Posts</h2>

<?php if((isset($GLOBALS['done'])) || (isset($_GET['one-batch']))){
    echo '<span class="afift_alert">All featured images have been created.</span>';
    } ?>

	<p>Click the button below if you'd like to generate featured images for all your posts. Featured images will be generated for those which do not have them. Depending on how many posts you have, this might take awhile. Just click it once, and then wait for the page to refresh as many times as it needs to generate images for batches of posts. When all of the images have been created for all of your posts, you will see this page again, with a notice at the top informing you that all the images were created.</p>

<form method="get" action="options-general.php">
<input type="hidden" name="page" value="auto-featured-image-from-title-pro-trial.php" />
<input type="hidden" name="afift_bulk" value="1" />
<input type="hidden" name="tab" value="bulk" />

<p><input type="checkbox" id="afift_bulk_pages" name="afift_bulk_pages" value="yes" <?php if(isset($_GET['afift_bulk_pages'])){ echo 'checked="checked" ';} ?> />
<label for="afift_bulk_pages"><strong>Check to generate images for pages rather than posts</strong></label>
</p>

<p><input type="checkbox" id="override_disable" name="override_disable" value="yes" <?php if(isset($_GET['override_disable'])){ echo 'checked="checked" ';} ?> />
<label for="override_disable"><strong>Check to override "disable for this post" even if set within posts</strong></label>
</p>

<p><input type="checkbox" id="unset_images" name="unset_images" value="yes" <?php if(isset($_GET['unset_images'])){ echo 'checked="checked" ';} ?> />
<label for="unset_images"><strong>Check to first unset ALL featured images</strong>&nbsp;<span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Prior to generating new featured images, this will cause the bulk utility to first unset all existing featured images. It will not delete the previous featured images, but will merely unset them. This setting does not take into account which featured images will be immediately generated by the bulk utility, but will unset ALL featured images for ALL posts before creating new ones based on the plugin's settings. Use with caution.</span></span></label>
</p>
<p><label for="refresh"><strong>Delay in seconds between batches of pages/posts:</strong>&nbsp;<span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>Try increasing this if your server crashes due to the volume of posts you're trying to create images for.</span></span></label>
<input type="text" id="refresh" name="refresh" value="<?php if(isset($_GET['refresh'])){echo $_GET['refresh'];}else{echo '3';} ?>" /></p>

<p><input type="checkbox" id="one-batch" name="one-batch" value="yes" <?php if(isset($_GET['one-batch'])){ echo 'checked="checked" ';} ?> />
<label for="one-batch"><strong>Stop after one batch?</strong>&nbsp;<span class="helptip"><img src="<?php echo plugins_url('images/question_white.png', __FILE__ ); ?>" class="helpicon" alt="help" width="16" height="16" /><span>This is useful if you'd like to do a test run to see how images will be generated before you allow the script to generate images for all of your posts.</span></span></label>
</p>

<input type="submit" class="afift_bulk_button" value="Generate Images" />
</form>

<?php }elseif($tab=='debug'){

$troubleshooting='no'; ?>

<h2>Debug &amp; Troubleshoot</h2>

  <p>This is a highly specialized plugin that depends on many things working together in your PHP and server environment. It works out-of-the-box for most installs, but due to the variety of server setups, sometimes it may fail upon first installing it. This debug tool addresses some of the most common reasons for the plugin to fail.</p>
  <p>Assure that all items below are marked as <strong style="color:#009900;">OK!</strong> If not they are not, attempt to troubleshoot the issue by following the recommended course of action. If the error isn't solved by doing so, refresh this page for additional actions to take, or contact the plugin author with a description of the error and all of the debug information below.</p>
  <hr />
  
  <p>Uploads folder exists: <?php if(file_exists($afift_uploads_path)){echo '<strong style="color:#009900;">OK!</strong>';}else{ ?>
	<strong style="color:#ff0000;">Troubleshoot</strong></p>
    <?php $troubleshooting='yes'; ?>
    <div style="background-color:#eee;padding:10px;"><strong style="font-size:1.1em">Recommended Course of Action:</strong>
	  <ol>
		<li>Download and install the <a href="http://designsbychris.com/auto-featured-image-from-title-uploads.zip">Uploads Helper Plugin</a></li>
		<li>Refresh this page</li>
      </ol>
    </div>
	<?php } ?>

  <p>Background Images folder exists: <?php if(file_exists($afift_images_path)){echo '<strong style="color:#009900;">OK!</strong>';}elseif($troubleshooting!='yes'){ ?>
	<strong style="color:#ff0000;">Troubleshoot</strong></p>
    <?php if($troubleshooting!='yes'){$troubleshooting='yes'; ?>
    <div style="background-color:#eee;padding:10px;"><strong style="font-size:1.1em">Recommended Course of Action:</strong>
	  <ol>
		<li>Find and delete this folder via ftp: /wp-content/plugins/auto-featured-image-from-title-uploads/</li>
		<li>Download and install the <a href="http://designsbychris.com/auto-featured-image-from-title-uploads.zip">Uploads Helper Plugin</a></li>
		<li>Refresh this page</li>
      </ol>
    </div>
    <?php }} ?>

  <p>Fonts folder exists: <?php if(file_exists($afift_fonts_path)){echo '<strong style="color:#009900;">OK!</strong>';}else{ ?>
	<strong style="color:#ff0000;">Troubleshoot</strong></p>
    <?php if($troubleshooting!='yes'){$troubleshooting='yes'; ?>
    <div style="background-color:#eee;padding:10px;"><strong style="font-size:1.1em">Recommended Course of Action:</strong>
	  <ol>
		<li>Attempt to create this folder manually via ftp, giving it write permissions: /wp-content/plugins/auto-featured-image-from-title-uploads/fonts/</li>
		<li>Click the Manage Fonts tab above</li>
		<li>Upload a font (you can download the fonts under "Font Licenses" on the right or below for free)</li>
		<li>Refresh this page</li>
      </ol>
    </div>
    <?php }} ?>

  <p>Images folder contains at least one subfolder:
	<?php $result = false;
    if($dh = opendir($afift_images_path)) {
      while (!$result && ($file = readdir($dh))) {
        $result = $file !== "." && $file !== ".." && is_dir($afift_images_path.'/'.$file);
        }
      closedir($dh);
      }
    if($result!=false){echo '<strong style="color:#009900;">OK!</strong>';}else{ ?>
	<strong style="color:#ff0000;">Troubleshoot</strong></p>
    <?php if($troubleshooting!='yes'){$troubleshooting='yes'; ?>
    <div style="background-color:#eee;padding:10px;"><strong style="font-size:1.1em">Recommended Course of Action:</strong>
	  <ol>
		<li>Attempt to create a folder manually via ftp, giving it write permissions: /wp-content/plugins/auto-featured-image-from-title-uploads/images/Miscellaneous/</li>
		<li>Click the Manage Images tab above</li>
		<li>Upload an image (you can download some great images to use for free <a href="https://unsplash.com/">here</a>)</li>
		<li>Refresh this page</li>
      </ol>
    </div>
    <?php }} ?>
	
  <p>Fonts folder contains fonts: <?php if(glob($afift_fonts_path.'{*.ttf}', GLOB_BRACE)){echo '<strong style="color:#009900;">OK!</strong>';}else { ?>
	<strong style="color:#ff0000;">Troubleshoot</strong></p>
    <?php if($troubleshooting!='yes'){$troubleshooting='yes'; ?>
    <div style="background-color:#eee;padding:10px;"><strong style="font-size:1.1em">Recommended Course of Action:</strong>
	  <ol>
		<li>Click the Manage Fonts tab above</li>
		<li>Upload a font (you can download the fonts under "Font Licenses" on the right or below for free)</li>
		<li>Refresh this page</li>
      </ol>
    </div>
    <?php }}

  if((strpos(get_option('auto_image_bg_image'),'random_') === false) && (get_option('auto_image_bg_image')!='')){ ?>
  <p>The active background image exists: <?php if(file_exists($afift_images_path . get_option('auto_image_bg_image'))){echo '<strong style="color:#009900;">OK!</strong>';}else{ ?>
	<strong style="color:#ff0000;">Troubleshoot</strong></p>
    <?php if($troubleshooting!='yes'){$troubleshooting='yes'; ?>
    <div style="background-color:#eee;padding:10px;"><strong style="font-size:1.1em">Recommended Course of Action:</strong>
	  <ol>
		<li>Click the Settings tab above</li>
		<li>Click the Background Settings link</li>
		<li>Assure that a background image is selected</li>
		<li>Click "Save Changes" to save the settings</li>
		<li>Refresh this page</li>
      </ol>
    </div>
    <?php }} 
  }
  
  if(get_option('auto_image_fontface')!='random'){ ?>
  <p>The active font exists: <?php if(file_exists($afift_fonts_path . get_option('auto_image_fontface'))){echo '<strong style="color:#009900;">OK!</strong>';}else{ ?>
	<strong style="color:#ff0000;">Troubleshoot</strong></p>
    <?php if($troubleshooting!='yes'){$troubleshooting='yes'; ?>
    <div style="background-color:#eee;padding:10px;"><strong style="font-size:1.1em">Recommended Course of Action:</strong>
	  <ol>
		<li>Click the Settings tab above</li>
		<li>Click the Text Settings link</li>
		<li>Assure that a font is selected</li>
		<li>Click "Save Changes" to save the settings</li>
		<li>Refresh this page</li>
      </ol>
    </div>
    <?php }}
  }
  
} ?>

</div>

<div id="afift_info">

    <strong>Font Licenses:</strong><br />
    <small>
	  <a href="http://www.fontspace.com/sudtipos/aguafina-script">Aguafina Script</a> | <a href="http://www.fontspace.com/sudtipos/aguafina-script">license</a><br />
	  <a href="https://www.fontsquirrel.com/fonts/ChunkFive">ChunkFive</a> | <a href="http://www.fontsquirrel.com/license/ChunkFive">license</a><br />
	  <a href="https://www.theleagueofmoveabletype.com/linden-hill">Linden Hill</a> | <a href="https://www.theleagueofmoveabletype.com/linden-hill">license</a><br />
	  <a href="https://www.fontsquirrel.com/fonts/raleway">Raleway</a> | <a href="https://www.fontsquirrel.com/license/raleway">license</a><br />
  	</small><br />

    <strong>Image Licenses:</strong><br />
    <small>
	  <a href="https://pixabay.com/ro/c%C4%83r%C4%83mizi-perete-pietre-structura-459299/">Bricks</a> | 
	  <a href="https://freestocktextures.com/texture/grunge-wall-plaster,571.html">Grunge</a><br />
	</small><br />
    <strong><a href="https://unsplash.com/?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Unsplash</a> Photos:</strong><br />
    <small>
	  <a href="https://unsplash.com/photos/8fMwyZPxqtg">Daisy</a> &lt;<a href="https://unsplash.com/@iidemii?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Demi Kwant</a>&gt;<br />
	  <a href="https://unsplash.com/photos/yZ_2RjtKXUU">Rose</a> &lt;<a href="https://unsplash.com/@ozarkdrones?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Ozark Drones</a>&gt;<br />
	  <a href="https://unsplash.com/photos/Ez5V2THOpDo">Clouds</a> &lt;<a href="https://unsplash.com/@rachadan?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Lim changwon</a>&gt;<br />
	  <a href="https://unsplash.com/photos/ss0vA9RUCV4">Grass Hill</a> &lt;<a href="https://unsplash.com/@miranido?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Ido Miran</a>&gt;<br />
	  <a href="https://unsplash.com/photos/FEGsRHANjRg">Sunset</a> &lt;<a href="https://unsplash.com/@mrsamwheeler?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Sam Wheeler</a>&gt;<br />
	  <a href="https://unsplash.com/photos/VLdaxYyXJvw">Bokeh</a> &lt;<a href="https://unsplash.com/@sebastianmuller?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Sebastian Muller</a>&gt;<br />
	  <a href="https://unsplash.com/photos/082dCXNKfxU">Book</a> &lt;<a href="https://unsplash.com/@bamb?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Alex Read</a>&gt;<br />
	  <a href="https://unsplash.com/photos/h0Vxgz5tyXA">Wood</a> &lt;<a href="https://unsplash.com/@keithmisner?utm_source=Auto_Featured_Image_from_Title&utm_medium=referral&utm_campaign=api-credit">Keith Misner</a>&gt;
	</small>
 
</div>

<?php }

// register_uninstall_hook('uninstall.php', 'afift_pro_uninstall');

// Display a notice that can be dismissed

?>