<?php
/* Plugin Name: WP Reactor
 * Plugin URI: logicsbuffer.com
 * Description: Place this Shortcode in header.php "[show_design_tool]" to make it work.
 * Author: WpRight
 * Author URI: https://www.fiverr.com/wpright
 * Version: 1.0
 */
add_action( 'init', 'design_tool' );

function design_tool() {
	add_shortcode( 'show_design_tool', 'design_tool_form' );
	add_action( 'wp_enqueue_scripts', 'fabric_scripts' );
	include 'wp-facebook-login/facebook-login.php';
	//include 'slider/slider.php';
}
function fabric_scripts() {
	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );
global $wp_query;
	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'postID' => $wp_query->post->ID,
		));
		wp_enqueue_script( 'fabric_js', plugins_url() . '/reactor/js/fabric.js' );
		//wp_enqueue_script( 'design_tool_js', plugins_url() . '/reactor/js/design_tool.js' );
		wp_enqueue_script( 'html2canvas_js', plugins_url() . '/reactor/js/html2canvas.js' );
		wp_enqueue_script( 'slider_js', plugins_url() . '/reactor/js/slider.js' );

		wp_enqueue_style( 'design_tool_css', plugins_url() . '/reactor/css/design_tool.css' );
}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
function post_love_add_love() {
	global $current_user;
	get_currentuserinfo();
	$upload_dir = wp_upload_dir();
	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
	$img_64 = $_REQUEST['canvas_img'];
	$emoji_reaction = $_REQUEST['emoji_reaction'];
	$design_name = $_REQUEST['canvas_img1'];
	$react_post_id = $_REQUEST['c_post_id'];
	$file_type='.jpeg';
	
	$path = $user_dirname.'/'.$design_name.$file_type;
	print_r($path);
	print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	

	//Insert Post 
	global $post;
	if($path){
		$user_name = $current_user->user_login;
		$uniq_id = uniqid();

		// Create post object				
			$my_post = array(
			  'post_title'    => "Reaction $uniq_id",
			  'post_content'  => "test",
			  'post_status'   => 'publish'
			);
		
		// Insert the post into the database
		$parent_post_id = wp_insert_post( $my_post );
	}
	$user_id = get_post_field( 'post_author', $parent_post_id );
	//$fb_url = get_avatar_url( $user_id, 72 );
	$fb_img = get_usermeta($user_id, 'facebook_avatar_thumb');
	$size = 72;
	$fb_id = get_user_meta( $user_id, '_fb_user_id', true );
	if ( $fb_id = get_user_meta( $user_id, '_fb_user_id', true ) ) {
		$fb_url = 'https://graph.facebook.com/' . $fb_id . '/picture?width=' . $size . '&height=' . $size;
	}
	$reaction_id = "reaction_".$react_post_id;
	$category_terms = array( $reaction_id );
	wp_set_object_terms( $parent_post_id, $category_terms , 'category' );
	//wp_set_post_terms( $parent_post_id, array('metais'), $category_texonomy);
	 update_post_meta( $parent_post_id, 'reaction', $emoji_reaction );
	 update_post_meta( $parent_post_id, 'fb_url', $fb_url );

	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $path ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $path ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $path, $parent_post_id );
	set_post_thumbnail( $parent_post_id, $attach_id );	
}

function login_redirect( $redirect_to, $request, $user ){
    return home_url('my-account/');
}
add_filter( 'login_redirect', 'login_redirect', 10, 3 );


function design_tool_form() {
ob_start();
echo show_slider();
?>
<!--<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>-->
		<a href="#reaction_window_parent" id=""/><div id="rt_react"><button id="click_react" class="button">Click To React</button></div></a>
	<!-- Trigger/Open The Modal -->

	<div id="previewImage" style="display:none;">
	</div>
	<div id="preloader" style="display:none;">
	<img scr="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif">
	</div>


<!-- The Modal -->

	
	<!-- CSS Model -->
	<!-- <a href="#reaction_window_parent">Open Modal</a> -->

	<div id="reaction_window_parent" class="modalDialog" style="display:none">
		<?php echo do_shortcode('[fbl_login_button redirect="" hide_if_logged="" size="large" type="continue_with" show_face="true"]');?>
		<div id="reaction_window">
		<span class="btn-close-react">×</span>
		
		<div id="looader" class="" style="position: absolute; z-index: -1;width: 100%;top: 35%;">
		<img src="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif" style="
		    position: absolute;width: 10%;left: 45%; top: 50%; right: 45%;">
		</div>
		<canvas id="canvas"></canvas>
		<div id="emoji_div" style="display:none">
		<?php echo emoji(); ?>		
		</div>
		</div>
	</div>

<style>

span#list_emojis {
    background: none !important;
    width: 345px;
    border: none !important;
    text-align: center;
    position: relative;
    top: 0em;
    max-width: 400px;
}
.canvas-container {
    display: inline-block;
}
div#reaction_window {
    background: #000000bd !important;
    display: inline-block;
    width: 100% !important;
    height: 100%;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
	margin: auto;
    text-align: center;
    margin: 0 auto;
}
div#emoji_div {
    display: block;
    position: relative !important;
    z-index: 9999 !important;
    top: 0em;
    width: 100%;
    text-align: center;
}
.um-message-emoji {
    position: relative;
    right: 0;
    top: 0;
    text-align: center;
    display: inline-block;
}
/* Emoji Styling */
.um-message-emo:hover { opacity: 1; }
.um-message-emolist span:hover {opacity: 1}

.um-message-emolist img.emoji {
	height: 1.5em !important;
	max-height: 1.5em !important;
	width: 1.5em !important;
}
.um-message-emo img {
    display: none !important;
    width: 80px;
    height: 80px;
    padding: 0.2em;
}
img.rec-emoji {
    display: inline !important;
    border: none !important;
    box-shadow: none !important;
    height: auto;
    width: 3em !important;
    margin: 0 .07em !important;
    vertical-align: -0.1em !important;
    background: none !important;
    padding: 0 !important;
}
.um-message-emo {
	border-radius: 3px;
	display: block !important;
	outline: none !important;
	padding: 7px;
	opacity: 0.75;
	transition: none;
	border: none !important;
}
.um-message-emolist {
    border: 1px solid rgba(59, 161, 218, 0.25);
}

.um-message-emolist {
	position: absolute;
	bottom: 30px;
	right: 0;
	background: #fff;
	border-radius: 3px;
	z-index: 8;
	padding: 5px 0 5px 10px;
	box-sizing: border-box;
	@display: none;
	width: 266px;
}
.um-message-emolist span {
	float: left;
	margin: 5px 5px 0 0;
	cursor: pointer;
	opacity: 0.9;
}

.bx-wrapper {
    height: 310px !important;
}
div#psc_thumnail_slider5a5dd4df7a72f {
    max-height: 400px !important;
}
#psc_divSliderMain5a5dd574ae24f .bx-wrapper .bx-viewport {
    height: 300px !important;
}
div#poststuff {
    height: 400px;
}
/* Css Model */
.modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    @background: rgba(0, 0, 0, 0.8);
    z-index: 99999;
    opacity:0;
	border: 3px solid black;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}
.modalDialog > div {
    width: 1006px;
    height: auto;
    position: relative;
    margin: 0 auto;
    padding: 0px 0px 0px 0px;
    border-radius: 10px;
    
   @ background: #fff;
   @ background: -moz-linear-gradient(#fff, #999);
   @ background: -webkit-linear-gradient(#fff, #999);
   @ background: -o-linear-gradient(#fff, #999);
}
.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: -12px;
    text-align: center;
    top: -10px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    -moz-box-shadow: 1px 1px 3px #000;
    -webkit-box-shadow: 1px 1px 3px #000;
    box-shadow: 1px 1px 3px #000;
}
.close:hover {
    background: #00d9ff;
}
#rt_react {
    display: block;
    position: fixed;
    bottom: 50px;
    width: 145px;
    right: 20px;
    z-index: 99;
    outline: none;
    cursor: pointer;
}
@#jssor_1{
	 display:none;
}
.wcp-carousel-wrapper {
   display:none;
}
.modalDialog > div {
    width: 90% !important;
}
button.insert-emo_5 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/clap.gif);
    background-size: cover;
    width: 80px;
    height: 80px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_0 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/love-png-n.gif);
    background-size: cover;
    width: 80px;
    height: 80px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_3 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/sad.gif);
    background-size: cover;
    width: 80px;
    height: 80px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_1 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/haha.gif);
    background-size: cover;
    width: 80px;
    height: 80px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_4 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/angry.gif);
    background-size: cover;
    width: 80px;
    height: 80px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_2 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/wow.gif);
    background-size: cover;
    width: 80px;
    height: 80px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}

.fbl-button {
    display: block;
    position: absolute;
    top: 50%;
}
img#post_thumbnails_main {
    height: 100%;
    text-align: center;
    display: inline-block;
}
img#post_thumbnails_main_m {
	display: block;
    height: 100% !important;
    text-align: center !important;
    width: auto;
    margin: 0 auto !important;
}
</style>
<script>
var uniqid = '<?php echo uniqid(); ?>';
var canvas1 = new fabric.Canvas('canvas', { selection: false });

//Test Code

var rect, isDown, origX, origY;

canvas1.on('mouse:down', function(o){
    isDown = true;
    var pointer = canvas1.getPointer(o.e);
    origX = pointer.x;
    origY = pointer.y;
    var pointer = canvas1.getPointer(o.e);
    rect = new fabric.Rect({
        left: origX,
        top: origY,
        originX: 'left',
        originY: 'top',
        width: pointer.x-origX,
        height: pointer.y-origY,
        angle: 0,
        fill: 'transparent',
		stroke: 'rgba(8, 158, 0, 1)',
        strokeWidth: 5,
        transparentCorners: false
    });
    canvas1.add(rect);
});

canvas1.on('mouse:move', function(o){
    if (!isDown) return;
    var pointer = canvas1.getPointer(o.e);

    if(origX>pointer.x){
        rect.set({ left: Math.abs(pointer.x) });
    }
    if(origY>pointer.y){
        rect.set({ top: Math.abs(pointer.y) });
    }

    rect.set({ width: Math.abs(origX - pointer.x) });
    rect.set({ height: Math.abs(origY - pointer.y) });


    canvas1.renderAll();
});

canvas1.on('mouse:up', function(o){
  isDown = false;
});


//Test Code


var can_width = window.innerWidth;
var can_height = window.innerHeight;
console.log(can_width);
console.log(can_height);
ratio_width = can_width / can_height;

if(can_width >= 761 && can_width <= 1024){		
	ratio_width = can_width / can_height;
	adjusted_height0 = window.innerHeight;
	adjusted_width0 = window.innerWidth;	
	adjusted_height = adjusted_height0 - 100;
	adjusted_width = adjusted_width0 - 10;
}

if(can_width <= 450){
	ratio_width = can_width / can_height;
	var adjusted_height1 = window.innerHeight;
	var adjusted_width1 = window.innerWidth;

	adjusted_height = adjusted_height1 - 100;
	adjusted_width = adjusted_width1 - 10;
}

else{
	ratio_width = can_width / can_height;
	var adjusted_height3 = window.innerHeight;
	var adjusted_width3 = window.innerWidth;
	adjusted_height = adjusted_height3 - 200;
	adjusted_width = adjusted_width3 - 20;
}
var center_left = adjusted_width/2;
var center_bottom = adjusted_height-20;
var comnt_text; // global variable
canvas1.setDimensions({width:adjusted_width, height:adjusted_height});
canvas1.freeDrawingBrush.color = '#FF0000';
canvas1.freeDrawingBrush.width = parseInt(5, 10) || 1;
var getCanvas; // global variable

jQuery(document).ready(function(){	  
var canvasData = localStorage.getItem("react_check");
var react_count = jQuery('#react_count').html();
jQuery('#reaction_window').prepend('<p id="react_count_window">'+react_count+' Reactions</p>');
console.log(canvasData);
if(canvasData == 1){
	jQuery("#slider_background").show();
	jQuery("#jssor_m").show();
	localStorage.setItem("react_check",0);
}
if(canvasData == 0){
	jQuery("#slider_background").hide();
	jQuery("#jssor_m").hide();
}
//jQuery("#jssor_1").hide();
	jQuery("#rt_react").on('click', function () {
		
		if (jQuery('body').hasClass('logged-in')) {
		    //execute your jquery code.
			html2canvas(document.body, { type: 'view' }).then(function(canvas) {
			jQuery('canvas').attr('id', 'newcanvas');
			getCanvas = canvas;
			var imgageData = getCanvas.toDataURL("image/png");
				fabric.Image.fromURL(imgageData, function(img) {
				canvas1.backgroundImage = img;
				canvas1.renderAll();
				});
			});
			jQuery("#reaction_window_parent").show();
			jQuery("#emoji_div").show();
			
		}else{
			console.log("Not Loggedin");
			jQuery("#reaction_window_parent").show();
			jQuery("#reaction_window").hide();
			alert("Please Login First to React.");
		}
		
		
		//jQuery("#reaction_window_parent").show();
		//jQuery("#previewImage").html(canvas1);
	});
jQuery('.insert-emo').click(function() {
	/* text_objext = canvas1.getObjects()[0].text;
	//console.log(text_objext);
	if(text_objext == '  Wanna say something?  '){
		textBox.remove();
	} */
	
	jQuery("#reaction_window_parent").hide();
	jQuery('.modal-wrapper').toggleClass('open');
	jQuery('.page-wrapper').toggleClass('blur-it');	
	
	var emoji_url = jQuery(this).attr('data');
	console.log(emoji_url);
	var canvasData = canvas1.toDataURL("image/png");
	 localStorage.setItem("canvas_image", canvasData);
	var design_name = 'design'+uniqid;
	var canvasData = localStorage.getItem("canvas_image");
	var postData = "canvasData="+canvasData;
	var debugConsole= document.getElementById("debugConsole");
	localStorage.setItem("react_check",1);
	jQuery.ajax({
		url : postlove.ajax_url,
		type : 'post',
		data : {
			action : 'post_love_add_love',
			canvas_img : canvasData,
			emoji_reaction : emoji_url,
			c_post_id : postlove.postID,
			canvas_img1 : design_name
		},
		success : function( response ) {
			//localStorage.setItem("react_check",1);
		}
	});	
	return false;
	
});
jQuery('#see_yes').click(function() {
	jQuery("#reaction_window_parent").hide();
	jQuery(".modal-wrapper").hide();
	setTimeout(function(){window.location.reload();}, 3000);
});

jQuery('#see_no').click(function() {
	jQuery("#preloader").hide();
	jQuery(".modal-wrapper").hide();
	jQuery("#reaction_window_parent").hide();
	jQuery("#slider_background").hide();
	localStorage.setItem("react_check",0);
	location.reload();
});
jQuery('.btn-close').click(function() {
	jQuery("#slider_background").hide();
	jQuery("#jssor_m").hide();	
});
jQuery('.btn-close-react').click(function() {
	jQuery("#reaction_window_parent").hide();
	location.reload();
});

/*     jQuery("#list_emojis").hide();    
    jQuery("#main_emoji_icon").hover(function(){
        jQuery("#list_emojis").show(1800);
        }, function(){
        jQuery("#list_emojis").hide();
    }); */
});
</script>
<?php
$result = ob_get_clean();
return $result;
}

function show_slider() {
?>
	<!-- custom 3d Slider.
    <script src="http://reactor.logicsbuffer.com/wp-content/plugins/reactor/3d-slider/js/slider.js"></script> -->
	<style>  
button#click_react {
    border-radius: 50%;
    height: 127px;
    width: 144px;
    background: #96588a;
    color: white;
    font-size: 16px;
    padding: 0px;
}	
		/* jssor slider loading skin tail-spin css */
        .jssorl-006-tail-spin img {
            animation-name: jssorl-006-tail-spin;
            animation-duration: 1.2s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-006-tail-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }

        .jssorb052 .i {position:absolute;cursor:pointer;}
        .jssorb052 .i .b {fill:#000;fill-opacity:0.3;}
        .jssorb052 .i:hover .b {fill-opacity:.7;}
        .jssorb052 .iav .b {fill-opacity: 1;}
        .jssorb052 .i.idn {opacity:.3;}

        .jssora053 {display:block;position:absolute;cursor:pointer;}
        .jssora053 .a {fill:none;stroke:#000;stroke-width:640;stroke-miterlimit:10;}
        .jssora053:hover {opacity:.8;}
        .jssora053.jssora053dn {opacity:.5;}
        .jssora053.jssora053ds {opacity:.3;pointer-events:none;}

img#reaction {
    width: 54px !important;
    height: auto !important;
    left: 47% !important;
    position: absolute;
    display: block;
    top: 0em;
}
#slider_background{
	display:none;
}
#jssor_m{
	display:none !important;
}	
div#slider_background {
    background: #000000bd !important;
    width: 100% !important;
    height: 100% !important;
    position: fixed;
    text-align: center;
    z-index: 9999;
    @display: block;
    top: 0%;
    left: 0;
    right: 0;
    bottom: 0;
}

div#jssor_1 {
    position: relative !important;
    top: 15% !important;
    left: 0% !important;
    background: white !important;
    text-align: center;
    margin: 0 auto;
    display: block !important;
} 
.jssora051 .a {
    fill: none;
    stroke: #000;
    stroke-width: 360;
    stroke-miterlimit: 10;
	
}
@media (min-width: 425px) and (max-width: 767px) {

#jssor_m img#post_thumbnails_main_m {
	padding-right: 5%;
    width: 320px !important;
    margin: 0 !important;
	padding: 0 !important;
}
}
@media (min-width: 320px) and (max-width: 425px) {

#jssor_m img#post_thumbnails_main_m {
	padding-right: 5%;
    width: 320px !important;
    margin: 0 !important;
	padding: 0 !important;
}

p.user_fb_name {
    left: 34.8% !important;  
}

img#reaction {
    left: 41% !important;
    width: 51px !important;
    top: 0em;
}	
.modal-wrapper .modal {
    width: 350px;
    height: 206px;
    display: block;
    margin: 0 auto;
    position: relative;
    top: 50%;
    left: 0%;
    background: #fff;
    transition: all 0.5s ease-in-out;
    padding-top: 0px !important;
    text-align: center;
}
.confirmation {
    margin-top: 2em;
}
span#list_emojis {
    max-width: 100% !important;
    width: 100% !important;
    display: block !important;
    height: 100% !important;
}
button.insert-emo {
	padding: 1px !important;
    margin: 0px 4px !important;
    float: left !important;
    width: 41px !important;
    height: 42px !important;
}
.good-job h3 {
    font-size: 15px;
}
.confirmation a.btn {
    width: 120px;
    padding: 11px 0;
}
#jssor_m {
	display:block !important;
	30 position: fixed !important;
    top: 15% !important;
    left: 0% !important;
    background: white !important;
    width: 100% !important;
} 
#jssor_1 {
	display:none;
} 
div#jssor_1 {
    position: fixed !important;
    top: 15% !important;
    left: 5% !important;
    background: white !important;
    width: 90% !important;
}  
#rt_react {
    display: block !important;
    position: fixed !important;
    bottom: 20px !important;
    width: 58px !important;
    right: 20px !important;
    z-index: 99 !important;
    outline: none !important;
    cursor: pointer !important;
}
#click_react {
    border-radius: 50% !important;
    height: 55px !important;
    width: 58px !important;
    font-size: 9px !important;
    border-radius: 50%;
    height: 127px;
    width: 144px;
    background-color: #96588a;
    border-color: #96588a;
    color: #ffffff;
}

button#click_react {
    border-radius: 50%;
    height: 127px;
    width: 144px;
    background-color: #96588a;
    border-color: #96588a;
    color: #ffffff	
}
 
.btn-close-react {
	 right: 50%;
}
}
.btn-close {
    font-size: 23px !important;
    position: absolute;
    right: 10px;
    left: 50%;
    top: 5% !important;
    color: white !important;
    z-index: 99999;
    border: 2px solid gray;
    border-radius: 50%;
    height: 16px;
    display: inline-table !important;
    background: #00000063;
    padding: 2px 0px !important;
    width: 40px;
    box-sizing: content-box;
    cursor: pointer;
}

.btn-close-react {
     font-size: 25px !important;
    position: absolute;
    right: 10%;
    top: 9% !important;
    color: white;
    z-index: 99999;
    border: 2px solid gray;
    border-radius: 50%;
    height: 19px;
    display: inline-table;
    background: #00000063;
    padding: 0px 0px !important;
    width: 42px;
    box-sizing: content-box;
    cursor: pointer;
}
.canvas-container {
    margin-top: 6%;
}
img#fb_icon_url {
  width: 50px !important;
    height: auto !important;
    left: 50% !important;
    position: absolute;
    border-radius: 50%;
}
.storefront-handheld-footer-bar {
    display: none;
}
p.user_fb_name {
    background: none;
    position: inherit;
    top: 59px;
    left: 45.8%;
    text-align: center;
    color: #5098C0;
    font-size: 18px;
    padding: 0 10px;
    font-weight: 600;
    width: 120px;
    border-radius: 2px;
	background:#fff;
}
p#react_count_window {
    top: 30px;
    position: absolute;
    font-size: 20px;
    color: #fff;
    border-bottom: none;
    padding: 8px 0px;
    text-align: left;
    z-index: 999;
    left: 30px;
    display: block;
}
	</style>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

         /*    var jssor_1_SlideoTransitions = [
              [{b:0,d:1000,y:185},{b:1000,d:500,o:-1},{b:1500,d:500,o:1},{b:2000,d:1500,r:360},{b:3500,d:1000,rX:30},{b:4500,d:500,rX:-30},{b:5000,d:1000,rY:30},{b:6000,d:500,rY:-30},{b:6500,d:500,sX:1},{b:7000,d:500,sX:-1},{b:7500,d:500,sY:1},{b:8000,d:500,sY:-1},{b:8500,d:500,kX:30},{b:9000,d:500,kX:-30},{b:9500,d:500,kY:30},{b:10000,d:500,kY:-30},{b:10500,d:500,c:{x:125.00,t:-125.00}},{b:11000,d:500,c:{x:-125.00,t:125.00}}],
              [{b:0,d:600,x:535,e:{x:27}}],
              [{b:-1,d:1,o:-1},{b:0,d:600,o:1,e:{o:5}}],
              [{b:-1,d:1,c:{x:250.0,t:-250.0}},{b:0,d:800,c:{x:-250.0,t:250.0},e:{c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1},{b:0,d:600,x:-570,o:1,e:{x:6}}],
              [{b:-1,d:1,o:-1,r:-180},{b:0,d:800,o:1,r:180,e:{r:7}}],
              [{b:0,d:1000,y:80,e:{y:24}},{b:1000,d:1100,x:570,y:170,o:-1,r:30,sX:9,sY:9,e:{x:2,y:6,r:1,sX:5,sY:5}}],
              [{b:2000,d:600,rY:30}],
              [{b:0,d:500,x:-105},{b:500,d:500,x:230},{b:1000,d:500,y:-120},{b:1500,d:500,x:-70,y:120},{b:2600,d:500,y:-80},{b:3100,d:900,y:160,e:{y:24}}],
              [{b:0,d:1000,o:-0.4,rX:2,rY:1},{b:1000,d:1000,rY:1},{b:2000,d:1000,rX:-1},{b:3000,d:1000,rY:-1},{b:4000,d:1000,o:0.4,rX:-1,rY:-1}]
            ]; */

            var jssor_1_options = {
              $AutoPlay: 0,
              $Idle: 2000,
              $Cols: 1,
			  $HWA: false,
              $Align: 0,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            //make sure to clear margin of the slider container element
            jssor_1_slider.$Elmt.style.margin = "";

            /*#region responsive code begin*/

            /*
                parameters to scale jssor slider to fill parent container

                MAX_WIDTH
                    prevent slider from scaling too wide
                MAX_HEIGHT
                    prevent slider from scaling too high, default value is original height
                MAX_BLEEDING
                    prevent slider from bleeding outside too much, default value is 1
                    0: contain mode, allow up to 0% to bleed outside, the slider will be all inside parent container
                    1: cover mode, allow up to 100% to bleed outside, the slider will cover full area of parent container
                    0.1: flex mode, allow up to 10% to bleed outside, this is better way to make full window slider, especially for mobile devices
            */

            var MAX_WIDTH = 1440;
            var MAX_HEIGHT = 600;
            var MAX_BLEEDING = 0.128;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {
                    var originalWidth = jssor_1_slider.$OriginalWidth();
                    var originalHeight = jssor_1_slider.$OriginalHeight();

                    var containerHeight = containerElement.clientHeight || originalHeight;

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
                    var expectedHeight = Math.min(MAX_HEIGHT || containerHeight, containerHeight);

                    //constrain bullets, arrows inside slider area, it's optional, remove it if not necessary
                    if (MAX_BLEEDING >= 0 && MAX_BLEEDING < 1) {
                        var widthRatio = expectedWidth / originalWidth;
                        var heightRatio = expectedHeight / originalHeight;
                        var maxScaleRatio = Math.max(widthRatio, heightRatio);
                        var minScaleRatio = Math.min(widthRatio, heightRatio);

                        maxScaleRatio = Math.min(maxScaleRatio / minScaleRatio, 1 / (1 - MAX_BLEEDING)) * minScaleRatio;
                        expectedWidth = Math.min(expectedWidth, originalWidth * maxScaleRatio);
                        expectedHeight = Math.min(expectedHeight, originalHeight * maxScaleRatio);
                    }

                    //scale the slider to expected size
                    jssor_1_slider.$ScaleSize(expectedWidth, expectedHeight, MAX_BLEEDING);

                    //position slider at center in vertical orientation
                    jssor_1_slider.$Elmt.style.top = ((containerHeight - expectedHeight) / 2) + "px";

                    //position slider at center in horizontal orientation
                    jssor_1_slider.$Elmt.style.left = ((containerWidth - expectedWidth) / 2) + "px";
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
	<?php
$current_id = get_the_ID();
$reaction_id ='reaction_'.$current_id; 
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1,'category_name' => $reaction_id)); 
$count = $wpb_all_query->post_count;
?><p style="display:none;" id="react_count"><? echo $count; ?></p>
<?php 
if ( $wpb_all_query->have_posts() ) : ?>
<ul>
</ul>
    <?php wp_reset_postdata(); ?>
 
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
    <div id="slider_background" >
		<div class="btn-close">×</div>
		<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1440px;height:600px;overflow:hidden;visibility:hidden;">
		<!-- Loading Screen -->
        <div data-u="loading" class="jssorl-006-tail-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/tail-spin.svg" />
        </div>
		<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1440px;height:600px;overflow:hidden;">
		<!-- the loop -->
		<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
		
	/* 	$user_id = get_the_author_meta('ID');
		//$fb_url = get_avatar_url( $user_id, 72 );
		$fb_img = get_usermeta($user_id, 'facebook_avatar_thumb');
		$size = 72;
		$fb_id = get_user_meta( $user_id, '_fb_user_id', true );
		if ( $fb_id = get_user_meta( $user_id, '_fb_user_id', true ) ) {
			$fb_url = 'https://graph.facebook.com/' . $fb_id . '/picture?width=' . $size . '&height=' . $size;
		} */
		$thumb_id = get_post_thumbnail_id();
		$user_nicename = get_the_author_meta('display_name');
		$fb_url = get_post_meta( get_the_ID(), 'fb_url', true );
		$reaction = get_post_meta( get_the_ID(), 'reaction', true );
		$thumb_url_arr = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		$thumb_url = $thumb_url_arr[0];
		?>
		<!-- the loop -->
	   <div data-p="255.00">
	   <img id="fb_icon_url" alt="Facebook Profile photo" src="<?php echo $fb_url; ?>"  class="avatar avatar-72 photo" height="72" width="72">
		   	<!--<img id="fb_icon_url" data-u="image" src="" />  -->
			<img id="reaction" data-u="image" src="<?php echo $reaction; ?>" />
			<img id="post_thumbnails_main" data-u="image" src="<?php echo $thumb_url; ?>" />
			<p class="user_fb_name"><?php echo $user_nicename; ?></p>
		</div>
		
		<?php endwhile;?>
		<!-- end of the loop -->
        </div>
		
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
	
	
	<!-- Slider Mobile-->
	
	<div id="jssor_m" style="display:none;position:relative;margin:0 auto;top:0px;left:0px;width:450px;height:500px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:450px;height:500px;overflow:hidden;">
            <!-- the loop -->
			<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
			
			$thumb_id1 = get_post_thumbnail_id();
			$reaction = get_post_meta( get_the_ID(), 'reaction', true );
			$fb_url = get_post_meta( get_the_ID(), 'fb_url', true );
			$user_nicename = get_the_author_meta('display_name');
			$thumb_url_arr1 = wp_get_attachment_image_src($thumb_id1,'thumbnail-size', true);
			$thumb_url = $thumb_url_arr1[0];
			?>
			<!-- the loop -->
			
			<div data-p="118.75">               
			   <img id="fb_icon_url" alt="Facebook Profile photo" src="<?php echo $fb_url; ?>"  class="avatar avatar-72 photo" height="72" width="72">
				<img id="reaction" data-u="image" src="<?php echo $reaction; ?>" />
				<img id="post_thumbnails_main_m" data-u="image" src="<?php echo $thumb_url; ?>" />
				<p class="user_fb_name"><?php echo $user_nicename; ?></p>
            </div>
            <?php endwhile; ?>
        </div>
        
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora052" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora052" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    </div>
    <!-- #endregion Jssor Slider End -->
	
	<!-- Modal html -->

	<!-- Modal -->
	<div class="modal-wrapper">
	  <div class="modal">
		<div class="head">
		  <!-- <a class="btn-closem trigger" href="#">
			<i class="fa fa-times" aria-hidden="true"></i>
		  </a> -->
		</div>
		<div class="content">
			<div class="good-job">
			  <h3>Do You Want to See Others Reactions</h3>
			</div>
			<div class="confirmation">
				<a id="see_yes" class="btn trigger" href="#">Yes</a>
				<a id="see_no" class="btn trigger" href="#">No</a>
			</div>
		</div>
	  </div>
	</div>
	<style>	
	@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);


	.blur-it {
	  filter: blur(4px);
	}

	a.btn {
	width: 200px;
    padding: 18px 0;
    font-family: 'Montserrat', Arial, Helvetica, sans-serif;
    display: inline-block;
    font-weight: 700;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    color: #fff;
    border-radius: 0;
    background: #e2525c;
	}

	.modal-wrapper {
	  width: 100%;
	  height: 100%;
	  position: fixed;
	  top: 0; 
	  left: 0;
	  background: rgba(41, 171, 164, 0.8);
	  visibility: hidden;
	  opacity: 0;
	  transition: all 0.25s ease-in-out;
	  z-index: 9999 !important;
	}

	.modal-wrapper.open {
	  opacity: 1;
	  visibility: visible;
	}

	.modal-wrapper .modal {
	  width: 600px;
	  height: 270px;
	  display: block;
	  margin: 50% 0 0 -300px;
	  position: relative;
	  top: 50%; 
	  left: 50%;
	  background: #fff;
	  opacity: 0;
	  transition: all 0.5s ease-in-out;
	  padding-top: 0px !important;
	}

	.modal-wrapper.open .modal {
	  margin-top: -200px;
	  opacity: 1;
	}

	.head { 
		width: 100%;
		height: 32px;
		padding: 0px 12px;
		overflow: hidden;
		background: #e2525c;
		height: 45px;
	}

	@.btn-closem {
	  font-size: 28px;
	  display: block;
	  float: right;
	  color: #fff;
	}

	.modal-wrapper .content {	  
	   padding: 20px 10px;
	}

	.good-job {
	  text-align: center;
	  font-family: 'Montserrat', Arial,       Helvetica, sans-serif;
	  color: #e2525c;
	}
	.good-job .fa-thumbs-o-up {
	  font-size: 60px;
	}
	.good-job h1 {
	  font-size: 45px;
	}
	.confirmation {
		display: block;
		width: 100%;
		text-align: center;
		margin-top: 3em;
	}
	</style>	
	<!-- Modal html -->	
	
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb0645 {position:absolute;}
        .jssorb0645 .i {position:absolute;cursor:pointer;}
        .jssorb0645 .i .b {fill:#000;fill-opacity:.5;stroke:#fff;stroke-width:400;stroke-miterlimit:10;stroke-opacity:0.5;}
        .jssorb0645 .i:hover .b {fill-opacity:.8;}
        .jssorb0645 .iav .b {fill:#ffe200;fill-opacity:1;stroke:#ffaa00;stroke-opacity:.7;stroke-width:2000;}
        .jssorb0645 .iav:hover .b {fill-opacity:.6;}
        .jssorb0645 .i.idn {opacity:.3;}

        .jssora052 {display:block;position:absolute;cursor:pointer;}
        .jssora052 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora052:hover {opacity:.8;}
        .jssora052.jssora051dn {opacity:.5;}
        .jssora052.jssora051ds {opacity:.3;pointer-events:none;}
    </style>	
    <script type="text/javascript">
        jssor_m_slider_init = function() {

            var jssor_m_options = {
              $AutoPlay: 0,
              $Idle: 2000,
              $SlideEasing: $Jease$.$InOutSine,
              $DragOrientation: 3,
              $Cols: 1,
              $Align: 0,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_m_slider = new $JssorSlider$("jssor_m", jssor_m_options);

            /*#region responsive code begin*/
 
            var MAX_HEIGHT = 500;

            function ScaleSlider() {
                var containerElement = jssor_m_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {
                    var originalHeight = jssor_m_slider.$OriginalHeight();

                    var containerHeight = containerElement.clientHeight || originalHeight;

                    var expectedHeight = Math.min(MAX_HEIGHT || containerHeight, containerHeight);

                    jssor_m_slider.$ScaleHeight(expectedHeight);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>	
	<script type="text/javascript">jssor_m_slider_init();</script>
	<!-- Slider Mobile-->
	
<?php
}
function emoji() {
	$emojis_url = plugins_url( 'images', __FILE__ );
	$emoji = array(
		'love'=>$emojis_url.'/love-png-n.gif',
		'haha'=>$emojis_url.'/haha.gif',
		'wow'=>$emojis_url.'/wow.gif',
		'sad'=>$emojis_url.'/sad.gif',
		'angry'=>$emojis_url.'/angry.gif');
		?>
		<div class="um-message-emoji">
			<a id="main_emoji_icon" class="um-message-emo"> 
			<!--<img style="display:none;" src="http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/1f610.png" alt="" title="" />-->			
			<span id="list_emojis" style="@display:none;" class="um-message-emolist">
			<?php $n=0; ?>
			<?php foreach( $emoji as $emoji_code => $emoji_url ) { ?>
				
				<button class="insert-emo insert-emo_<?php echo $n;?>" data="<?php echo $emoji_url; ?>" title="<?php echo $emoji_code; ?>">
				</button>
			<?php
			$n++;	
			} ?>
			</span>
			</a>
		</div>

		<?php
	}