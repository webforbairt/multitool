<?php
/* Plugin Name: WP Reactor
 * Plugin URI: reactor.logicsbuffer.com/design-tool/
 * Description: You can react on any selected part of the page.
 * Author: WpRight
 * Author URI: https://www.fiverr.com/wpright
 * Version: 1.0
 */
add_action( 'init', 'design_tool' );

function design_tool() {
	add_shortcode( 'show_design_tool', 'design_tool_form' );
	add_action( 'wp_enqueue_scripts', 'fabric_scripts' );
	include 'wp-facebook-login/facebook-login.php';
	//include 'slider/slider.php';
}
function fabric_scripts() {
	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
		));
		wp_enqueue_script( 'fabric_js', plugins_url() . '/reactor/js/fabric.js' );
		//wp_enqueue_script( 'design_tool_js', plugins_url() . '/reactor/js/design_tool.js' );
		wp_enqueue_script( 'html2canvas_js', plugins_url() . '/reactor/js/html2canvas.js' );
		wp_enqueue_script( 'slider_js', plugins_url() . '/reactor/js/slider.js' );

		wp_enqueue_style( 'design_tool_css', plugins_url() . '/reactor/css/design_tool.css' );
}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
function post_love_add_love() {
	global $current_user;
	get_currentuserinfo();
	 
	$upload_dir = wp_upload_dir();
	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
	$img_64 = $_REQUEST['canvas_img'];
	$emoji_reaction = $_REQUEST['emoji_reaction'];
	$design_name = $_REQUEST['canvas_img1'];
	$file_type='.jpeg';
	
	$path = $user_dirname.'/'.$design_name.$file_type;
	print_r($path);
	print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	

	//Insert Post 
	global $post;
	if($path){
		$user_name = $current_user->user_login;
		$uniq_id = uniqid();

		// Create post object				
			$my_post = array(
			  'post_title'    => "Reaction $uniq_id",
			  'post_content'  => "test",
			  'post_status'   => 'publish'
			);
		 
		// Insert the post into the database
		$parent_post_id = wp_insert_post( $my_post );		
	}
	 update_post_meta( $parent_post_id, 'reaction', $emoji_reaction );

	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $path ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $path ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $path, $parent_post_id );
	set_post_thumbnail( $parent_post_id, $attach_id );	
}

function design_tool_form() {
ob_start();
echo show_slider(); 
?>
<!--<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>-->
		<a href="#reaction_window_parent" id=""/><div id="rt_react"><button id="click_react" class="button">Click To React</button></div></a>
	<!-- Trigger/Open The Modal -->

	<div id="previewImage" style="display:none;">
	</div>
	<div id="preloader" style="display:none;">
	<img scr="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif">
	</div>


<!-- The Modal -->

	
	<!-- CSS Model -->
	<!-- <a href="#reaction_window_parent">Open Modal</a> -->

	<div id="reaction_window_parent" class="modalDialog" style="display:none">
		<?php echo do_shortcode('[fbl_login_button redirect="" hide_if_logged="" size="large" type="continue_with" show_face="true"]');?>
		<div id="reaction_window">
		<span class="btn-close-react">×</span>
		
		<div id="looader" class="" style="position: absolute; z-index: -1;width: 100%;top: 35%;">
		<img src="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif" style="
		    position: absolute;width: 10%;left: 45%; top: 50%; right: 45%;">
		</div>
		<canvas id="canvas"></canvas>
		<div id="emoji_div" style="display:none">
		<?php echo emoji(); ?>		
		</div>
		</div>
	</div>

<style>
button#click_react {
    border-radius: 50%;
    height: 127px;
    width: 144px;
}
span#list_emojis {
    background: none !important;
    width: 345px !important;
    border: none !important;
    text-align: center;
    position: relative;
    top: 0em;
    max-width: 400px !important;
}
.canvas-container {
    display: inline-block;
}
div#reaction_window {
    background: #000000bd !important;
    display: inline-block;
    width: 100% !important;
    height: 100%;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
	margin: auto;
    text-align: center;
    margin: 0 auto;
}
div#emoji_div {
    display: block;
    position: relative !important;
    z-index: 9999 !important;
    top: 0em;
    width: 100%;
    text-align: center;
}
.um-message-emoji {
    position: relative;
    right: 0;
    top: 0;
    text-align: center;
    display: inline-block;
}
/* Emoji Styling */
.um-message-emo:hover { opacity: 1; }
.um-message-emolist span:hover {opacity: 1}

.um-message-emolist img.emoji {
	height: 1.5em !important;
	max-height: 1.5em !important;
	width: 1.5em !important;
}
.um-message-emo img {
    display: none !important;
    width: 80px;
    height: 80px;
    padding: 0.2em;
}
img.rec-emoji {
    display: inline !important;
    border: none !important;
    box-shadow: none !important;
    height: auto;
    width: 3em !important;
    margin: 0 .07em !important;
    vertical-align: -0.1em !important;
    background: none !important;
    padding: 0 !important;
}
.um-message-emo {
	border-radius: 3px;
	display: block !important;
	outline: none !important;
	padding: 7px;
	opacity: 0.75;
	transition: none;
	border: none !important;
}
.um-message-emolist {
    border: 1px solid rgba(59, 161, 218, 0.25);
}

.um-message-emolist {
	position: absolute;
	bottom: 30px;
	right: 0;
	background: #fff;
	border-radius: 3px;
	z-index: 8;
	padding: 5px 0 5px 10px;
	box-sizing: border-box;
	@display: none;
	width: 266px;
}
.um-message-emolist span {
	float: left;
	margin: 5px 5px 0 0;
	cursor: pointer;
	opacity: 0.9;
}

.bx-wrapper {
    height: 310px !important;
}
div#psc_thumnail_slider5a5dd4df7a72f {
    max-height: 400px !important;
}
#psc_divSliderMain5a5dd574ae24f .bx-wrapper .bx-viewport {
    height: 300px !important;
}
div#poststuff {
    height: 400px;
}
/* Css Model */
.modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    @background: rgba(0, 0, 0, 0.8);
    z-index: 99999;
    opacity:0;
	border: 3px solid black;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}
.modalDialog > div {
    width: 1006px;
    height: auto;
    position: relative;
    margin: 0 auto;
    padding: 0px 0px 0px 0px;
    border-radius: 10px;
    
   @ background: #fff;
   @ background: -moz-linear-gradient(#fff, #999);
   @ background: -webkit-linear-gradient(#fff, #999);
   @ background: -o-linear-gradient(#fff, #999);
}
.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: -12px;
    text-align: center;
    top: -10px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    -moz-box-shadow: 1px 1px 3px #000;
    -webkit-box-shadow: 1px 1px 3px #000;
    box-shadow: 1px 1px 3px #000;
}
.close:hover {
    background: #00d9ff;
}
#rt_react {
    display: block;
    position: fixed;
    bottom: 50px;
    width: 145px;
    right: 20px;
    z-index: 99;
    outline: none;
    cursor: pointer;
}
@#jssor_1{
	 display:none;
}
.wcp-carousel-wrapper {
   display:none;
}
.modalDialog > div {
    width: 90% !important;
}
button.insert-emo_5 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/clap.png);
    background-size: cover;
    width: 48px;
    height: 48px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_0 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/love-png-n.png);
    background-size: cover;
    width: 48px;
    height: 48px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_3 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/sad.png);
    background-size: cover;
    width: 48px;
    height: 48px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_1 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/haha.png);
    background-size: cover;
    width: 48px;
    height: 48px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_4 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/angry.png);
    background-size: cover;
    width: 48px;
    height: 48px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}
button.insert-emo_2 {
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/wow.png);
    background-size: cover;
    width: 48px;
    height: 48px;
    border-radius: 180px;
    background-origin: border-box;
    background-position: center;
    background-position-x: center;
    background-position-y: center;
	background-color: transparent !important;
	border: none !important;
}

@media (min-width: 320px) and (max-width: 767px) {
  
#rt_react {
    display: block;
    position: fixed;
    bottom: 20px;
    right: 30px;
    z-index: 99;
    width: 80px;
    height: 80px;
    border: none;
    outline: none;
    background-color: #FFF;
    color: white;
    cursor: pointer;
    padding: 30px 10px;
    border-radius: 80px;
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/clap.png);
    background-size: cover;
} 
}
.fbl-button {
    display: block;
    position: absolute;
    top: 50%;
}
</style>
<script>
var uniqid = '<?php echo uniqid(); ?>';
var canvas1 = new fabric.Canvas('canvas', { isDrawingMode: true });
var can_width = window.innerWidth;
var can_height = window.innerHeight;
console.log(can_width);
console.log(can_height);
ratio_width = can_width / can_height;
adjusted_height = 600;
adjusted_width = ratio_width * 600;
if(can_width <= 450){
ratio_width = can_width / can_height;
adjusted_height = 350;
adjusted_width = ratio_width * 350;
}
console.log(adjusted_width);
console.log(adjusted_height);
canvas1.setDimensions({width:adjusted_width, height:adjusted_height});
canvas1.freeDrawingBrush.color = '#0000FF';
canvas1.freeDrawingBrush.width = parseInt(5, 10) || 1;

var getCanvas; // global variable

jQuery(document).ready(function(){
var canvasData = localStorage.getItem("react_check");
if(canvasData == 1){
	jQuery("#slider_background").show();
	localStorage.setItem("react_check",0);
}
if(canvasData == 0){
	jQuery("#slider_background").hide();
}
//jQuery("#jssor_1").hide();
	jQuery("#rt_react").on('click', function () {
		
		if (jQuery('body').hasClass('logged-in')) {
		    //execute your jquery code.
		    console.log("Loggedin");	
			html2canvas(document.body, { type: 'view' }).then(function(canvas) {
			jQuery('canvas').attr('id', 'newcanvas');
			getCanvas = canvas;
			var imgageData = getCanvas.toDataURL("image/png");
				fabric.Image.fromURL(imgageData, function(img) {
				canvas1.backgroundImage = img;
				canvas1.renderAll();
				});
			});
			jQuery("#reaction_window_parent").show();
			jQuery("#emoji_div").show();
			
		}else{
			console.log("Not Loggedin");
			jQuery("#reaction_window_parent").show();
			jQuery("#reaction_window").hide();
			alert("Please Login First to React.");
		}
		
		
		//jQuery("#reaction_window_parent").show();
		//jQuery("#previewImage").html(canvas1);
	});
jQuery('.insert-emo').click(function() {
	jQuery("#preloader").show();
	jQuery("#reaction_window_parent").hide();
	var emoji_url = jQuery(this).attr('data');
	console.log(emoji_url);
	var canvasData = canvas1.toDataURL("image/png");
	 localStorage.setItem("canvas_image", canvasData);
	var design_name = 'design'+uniqid;
	var canvasData = localStorage.getItem("canvas_image");

	var postData = "canvasData="+canvasData;
	var debugConsole= document.getElementById("debugConsole");  
	jQuery.ajax({
		url : postlove.ajax_url,
		type : 'post',
		data : {
			action : 'post_love_add_love',
			canvas_img : canvasData,
			emoji_reaction : emoji_url,
			canvas_img1 : design_name
		},
		success : function( response ) {
			localStorage.setItem("react_check",1);
			  location.reload();
			//jQuery("#jssor_1").show();
		}
	});	
	return false; 
});
jQuery('.btn-close').click(function() {
	jQuery("#slider_background").hide();
});
jQuery('.btn-close-react').click(function() {
	jQuery("#reaction_window_parent").hide();
});

/*     jQuery("#list_emojis").hide();    
    jQuery("#main_emoji_icon").hover(function(){
        jQuery("#list_emojis").show(1800);
        }, function(){
        jQuery("#list_emojis").hide();
    }); */
	
});
</script>
<?php
$result = ob_get_clean();
return $result;
}

function show_slider() {
?>
	<!-- custom 3d Slider.
    <script src="http://reactor.logicsbuffer.com/wp-content/plugins/reactor/3d-slider/js/slider.js"></script> -->
	<style>
	   /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb051 .i {position:absolute;cursor:pointer;}
        .jssorb051 .i .b {fill:#fff;fill-opacity:0.5;}
        .jssorb051 .i:hover .b {fill-opacity:.7;}
        .jssorb051 .iav .b {fill-opacity: 1;}
        .jssorb051 .i.idn {opacity:.3;}

        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
img#reaction {
	width: 50px !important;
    height: auto !important;
    left: 44% !important;
    position: absolute;
    display: block;
}
#slider_background{
	display:none;
}	
div#slider_background {
    background: #000000bd !important;
    width: 100% !important;
    height: 100% !important;
    position: fixed;
    text-align: center;
    z-index: 9999;
    @display: block;
    top: 0%;
    left: 0;
    right: 0;
    bottom: 0;
}
div#jssor_1 {
    position: absolute !important;
    top: 20% !important;
    left: 20% !important;
}
@media (min-width: 320px) and (max-width: 767px) {
  
#rt_react {
    display: block;
    position: fixed;
    bottom: 20px;
    right: 30px;
    z-index: 99;
    width: 80px;
    height: 80px;
    border: none;
    outline: none;
    background-color: #FFF;
    color: white;
    cursor: pointer;
    padding: 30px 10px;
    border-radius: 80px;
    background-image: url(http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/clap.png);
    background-size: cover;
} 
.btn-close-react {
	 right: 50%;
}
}
.btn-close {
    font-size: 25px !important;
    position: absolute;
    right: 10px;
    top: 7px !important;
    color: white;
    z-index: 99999;
    border: 2px solid gray;
    border-radius: 50%;
    height: 21px;
    display: inline-table;
    background: #00000063;
    padding: 0px 0px !important;
    width: 36px;
    box-sizing: content-box;
	cursor: pointer;
}
.btn-close-react {
     font-size: 25px !important;
    position: absolute;
    right: 10%;
    top: 9% !important;
    color: white;
    z-index: 99999;
    border: 2px solid gray;
    border-radius: 50%;
    height: 19px;
    display: inline-table;
    background: #00000063;
    padding: 0px 0px !important;
    width: 42px;
    box-sizing: content-box;
    cursor: pointer;
}
.canvas-container {
    margin-top: 6%;
}
img#fb_icon_url {
  width: 50px !important;
    height: auto !important;
    left: 50% !important;
    position: absolute;
    border-radius: 50%;
}
	</style>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 0,
              $SlideWidth: 720,
              $Cols: 2,
              $Align: 130,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
	<?php 
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
<?php 
if ( $wpb_all_query->have_posts() ) : ?>
 
<ul>
 

 
</ul>
 
    <?php wp_reset_postdata(); ?>
 
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; 
	global $current_user;
get_currentuserinfo();
$user_id = $current_user->ID;

?>
    <div id="slider_background" >
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
		<span class="btn-close">×</span>
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <!-- <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="https://www.jssor.com/theme/svg/loading/static-svg/spin.svg" /> -->
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
		    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
$user_id = get_the_author_meta('ID');
$fb_url = get_avatar_url( $user_id, 72 );
$fb_img = get_usermeta($user_id, 'facebook_avatar_thumb');
$size = 72;
$fb_id = get_user_meta( $user_id, '_fb_user_id', true );
if ( $fb_id = get_user_meta( $user_id, '_fb_user_id', true ) ) {
	$fb_url = 'https://graph.facebook.com/' . $fb_id . '/picture?width=' . $size . '&height=' . $size;
}
$thumb_id = get_post_thumbnail_id();
$reaction = get_post_meta( get_the_ID(), 'reaction', true );
$thumb_url_arr = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
$thumb_url = $thumb_url_arr[0];
	?>
		   <div data-p="137.50">
			   <img id="fb_icon_url" data-u="image" src="<?php echo $fb_url; ?>" />
                <img id="reaction" data-u="image" src="<?php echo $reaction; ?>" />
                <img data-u="image" src="<?php echo $thumb_url; ?>" />
            </div>
		
    <?php endwhile; ?>
    <!-- end of the loop -->
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:35px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:35px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
<?php
}
function emoji() {
	$emojis_url = plugins_url( 'images', __FILE__ );
	$emoji = array(
		'love'=>$emojis_url.'/love-png-n.png',
		'haha'=>$emojis_url.'/haha.png',
		'wow'=>$emojis_url.'/wow.png',
		'sad'=>$emojis_url.'/sad.png',
		'angry'=>$emojis_url.'/angry.png',
		'clapping'=>$emojis_url.'/clap.png');
		?>
		<div class="um-message-emoji">
			<a id="main_emoji_icon" class="um-message-emo">
			<!--<img style="display:none;" src="http://dev.logicsbuffer.com/wp-content/plugins/reactor/images/1f610.png" alt="" title="" />-->			
			<span id="list_emojis" style="@display:none;" class="um-message-emolist">
			<?php $n=0; ?>
			<?php foreach( $emoji as $emoji_code => $emoji_url ) { ?>
				
				<button class="insert-emo insert-emo_<?php echo $n;?>" data="<?php echo $emoji_url; ?>" title="<?php echo $emoji_code; ?>">
				</button>
			<?php
			$n++;	
			} ?>
			</span></a>
		</div>

		<?php
	}