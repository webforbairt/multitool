<?php
/* Plugin Name: WP Reactor
 * Plugin URI: reactor.logicsbuffer.com/design-tool/
 * Description: You can react on any selected part of the page.
 * Author: WpRight
 * Author URI: https://www.fiverr.com/wpright
 * Version: 1.0
 */
add_action( 'init', 'design_tool' );

function design_tool() {
	add_shortcode( 'show_design_tool', 'design_tool_form' );
	add_action( 'wp_enqueue_scripts', 'fabric_scripts' );
	include 'wp-facebook-login/facebook-login.php';
	include 'slider/slider.php';
}
function fabric_scripts() {
	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
		));
		wp_enqueue_script( 'fabric_js', plugins_url() . '/reactor/js/fabric.js' );
		wp_enqueue_script( 'design_tool_js', plugins_url() . '/reactor/js/design_tool.js' );

		wp_enqueue_style( 'design_tool_css', plugins_url() . '/reactor/css/design_tool.css' );
}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
function post_love_add_love() {
	global $current_user;
	get_currentuserinfo();
	 
	$upload_dir = wp_upload_dir();
	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
	$img_64 = $_REQUEST['canvas_img'];
	$emoji_reaction = $_REQUEST['emoji_reaction'];
	$design_name = $_REQUEST['canvas_img1'];
	$file_type='.jpeg';
	
	$path = $user_dirname.'/'.$design_name.$file_type;
	print_r($path);
	print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	

	//Insert Post 
	global $post;
	if($path){
		$user_name = $current_user->user_login;
		$uniq_id = uniqid();

		// Create post object				
			$my_post = array(
			  'post_title'    => "Reaction $uniq_id",
			  'post_content'  => "test",
			  'post_status'   => 'publish'
			);
		 
		// Insert the post into the database
		$parent_post_id = wp_insert_post( $my_post );		
	}
	 update_post_meta( $parent_post_id, 'reaction', $emoji_reaction );

	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $path ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $path ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $path, $parent_post_id );
	set_post_thumbnail( $parent_post_id, $attach_id );	
}

function design_tool_form() {
ob_start();
//echo do_shortcode('[wcp-carousel id="374"]');
echo do_shortcode('[psc_print_post_slider_carousel]'); ?>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
	<a href="#reaction_window_parent" id=""/><div id="rt_react"></div></a>
	<!-- Trigger/Open The Modal -->

	<div id="previewImage" style="display:none;">
	</div>
<div id="preloader" style="display:none;">
<img scr="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif">
	</div>


<!-- The Modal -->

	
	<!-- CSS Model -->
	<!-- <a href="#reaction_window_parent">Open Modal</a> -->

	<div id="reaction_window_parent" class="modalDialog" style="display:none">
		<div id="reaction_window">
		<?php echo do_shortcode('[fbl_login_button redirect="" hide_if_logged="" size="large" type="continue_with" show_face="true"]');?>
		<canvas id="canvas"></canvas>
		<div id="emoji_div" style="display:none">
		<?php echo emoji(); ?>
		<button onclick="saveViaAJAX();">Save Via AJAX</button>
		</div>
		</div>
		
	</div>

<style>
span#list_emojis {
    background: none !important;
    width: 650px;
    border: none !important;
    text-align: center;
    position: absolute;
    right: 50%;
    left: -131%;
    top: -5em;
}
.canvas-container {
    display: inline-block;
}
div#reaction_window {
    background: #000000bd !important;
    display: inline-block;
    width: 100% !important;
    height: 100%;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
	margin: auto;
    text-align: center;
    margin: 0 auto;
}
div#emoji_div {
    display: block;
    position: relative !important;
    z-index: 9999 !important;
    top: 0em;
    width: 100%;
    text-align: center;
}
.um-message-emoji {
    position: relative;
    right: 0;
    top: 0;
    text-align: center;
    display: inline-block;
}
/* Emoji Styling */
.um-message-emo:hover { opacity: 1; }
.um-message-emolist span:hover {opacity: 1}

.um-message-emolist img.emoji {
	height: 1.5em !important;
	max-height: 1.5em !important;
	width: 1.5em !important;
}
.um-message-emo img {
    display: block !important;
    width: 80px;
    height: 80px;
    padding: 0.2em;
}
img.rec-emoji {
    display: inline !important;
    border: none !important;
    box-shadow: none !important;
    height: auto;
    width: 3em !important;
    margin: 0 .07em !important;
    vertical-align: -0.1em !important;
    background: none !important;
    padding: 0 !important;
}
.um-message-emo {
	border-radius: 3px;
	display: block !important;
	outline: none !important;
	padding: 7px;
	opacity: 0.75;
	transition: none;
	border: none !important;
}
.um-message-emolist {
    border: 1px solid rgba(59, 161, 218, 0.25);
}

.um-message-emolist {
	position: absolute;
	bottom: 30px;
	right: 0;
	background: #fff;
	border-radius: 3px;
	z-index: 8;
	padding: 5px 0 5px 10px;
	box-sizing: border-box;
	display: none;
	width: 266px;
}
.um-message-emolist span {
	float: left;
	margin: 5px 5px 0 0;
	cursor: pointer;
	opacity: 0.9;
}

.bx-wrapper {
    height: 310px !important;
}
div#psc_thumnail_slider5a5dd4df7a72f {
    max-height: 400px !important;
}
#psc_divSliderMain5a5dd574ae24f .bx-wrapper .bx-viewport {
    height: 300px !important;
}
div#poststuff {
    height: 400px;
}
/* Css Model */
.modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    @background: rgba(0, 0, 0, 0.8);
    z-index: 99999;
    opacity:0;
	border: 3px solid black;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}
.modalDialog > div {
    width: 1006px;
    height: auto;
    position: relative;
    margin: 0 auto;
    padding: 0px 0px 0px 0px;
    border-radius: 10px;
    
   @ background: #fff;
   @ background: -moz-linear-gradient(#fff, #999);
   @ background: -webkit-linear-gradient(#fff, #999);
   @ background: -o-linear-gradient(#fff, #999);
}
.close {
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: -12px;
    text-align: center;
    top: -10px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    -moz-box-shadow: 1px 1px 3px #000;
    -webkit-box-shadow: 1px 1px 3px #000;
    box-shadow: 1px 1px 3px #000;
}
.close:hover {
    background: #00d9ff;
}
#rt_react {
    display: block;
    position: fixed;
    bottom: 20px;
    right: 30px;
    z-index: 99;
	width: 150px;
    height: 150px;
    border: none;
    outline: none;
    background-color: #FFF;
    color: white;
    cursor: pointer;
    padding: 55px 19px;
    border-radius: 80px;
    background-image: url(http://reactor.logicsbuffer.com/wp-content/plugins/reactor/images/clapping-hands.png);
    background-size: cover;
}
#poststuff{
	 display:none;
}
.wcp-carousel-wrapper {
   display:none;
}
.modalDialog > div {
    width: 90% !important;
}
</style>
<script>
var uniqid = '<?php echo uniqid(); ?>';
var canvas1 = new fabric.Canvas('canvas', { isDrawingMode: true });
var can_width = window.innerWidth;
var can_height = window.innerHeight;
ratio_width = can_width / can_height;
adjusted_height = 600;
adjusted_width = ratio_width * 600;
console.log(adjusted_width);
console.log(adjusted_height);
canvas1.setDimensions({width:adjusted_width, height:adjusted_height});
canvas1.freeDrawingBrush.color = '#0000FF';
canvas1.freeDrawingBrush.width = parseInt(5, 10) || 1;

var getCanvas; // global variable

jQuery(document).ready(function(){
//jQuery("#poststuff").hide();
	jQuery("#rt_react").on('click', function () {
		html2canvas(document.body, { type: 'view' }).then(function(canvas) {
		jQuery('canvas').attr('id', 'newcanvas');
		getCanvas = canvas;
		var imgageData = getCanvas.toDataURL("image/png");
			fabric.Image.fromURL(imgageData, function(img) {
			canvas1.backgroundImage = img;
			canvas1.renderAll();
			});
		});
		jQuery("#reaction_window_parent").show();
		jQuery("#emoji_div").show();
		//jQuery("#reaction_window_parent").show();
		//jQuery("#previewImage").html(canvas1);
	});
   jQuery("#list_emojis").hide();
    
    jQuery("#main_emoji_icon").hover(function(){
        jQuery("#list_emojis").show(1800);
        }, function(){
        jQuery("#list_emojis").hide();
    });
jQuery('.insert-emo').click(function() {
	jQuery("#preloader").show();
	//jQuery("#reaction_window_parent").hide();
	var emoji_url = jQuery(this).attr('data');
	fabric.Image.fromURL(emoji_url, function (img) {
	  var oImg = img.set({left: 500, top: 20, angle: 00}).scale(0.9);
	  canvas1.add(oImg).renderAll();
	  canvas1.renderAll();
	 var canvasData = canvas1.toDataURL({format: 'png', quality: 1});
		fabric.Image.fromURL(canvasData, function(img) {
		canvas1.backgroundImage = img;
		canvas1.renderAll();
		var canvasData = canvas1.toDataURL("image/png");
	  localStorage.setItem("canvas_image", canvasData);
		});
	});
	
	//var canvasData = canvas1.toDataURL("image/png");
	
	var design_name = 'design'+uniqid;
	var canvasData = localStorage.getItem("canvas_image");

	var postData = "canvasData="+canvasData;
	var debugConsole= document.getElementById("debugConsole");  
	jQuery.ajax({
		url : postlove.ajax_url,
		type : 'post',
		data : {
			action : 'post_love_add_love',
			canvas_img : canvasData,
			emoji_reaction : emoji_url,
			canvas_img1 : design_name
		},
		success : function( response ) {
		}
	});	
	return false; 
});
	
});
</script>
<?php
$result = ob_get_clean();
return $result;
}
function emoji() {
	$emojis_url = plugins_url( 'images', __FILE__ );
	$emoji = array(
		':applause'=>$emojis_url.'/clap.jpg',
		':heart_eyes:'=>$emojis_url.'/love.jpg',
		":'("=>$emojis_url.'/sad.jpg',
		':joy:'=>$emojis_url.'/haha.jpg',
		':rage:'=>$emojis_url.'/rage.jpg',
		':rage11:'=>$emojis_url.'/1f47f.png',
		':hushed:'=>$emojis_url.'/wow.jpg');
		?>
		<div class="um-message-emoji">
			<a id="main_emoji_icon" class="um-message-emo"><img src="http://reactor.logicsbuffer.com/wp-content/plugins/reactor/images/1f610.png" alt="" title="" />			
			<span id="list_emojis" style="display:none;" class="um-message-emolist"></span></a>
			<?php foreach( $emoji as $emoji_code => $emoji_url ) { ?>
				<button class="insert-emo" data="<?php echo $emoji_url; ?>" title="<?php echo $emoji_code; ?>">
				</button>
			<?php

			} ?>
		</div>

		<?php
	}