<?php
/* Plugin Name: WP Reactor
 * Plugin URI: reactor.logicsbuffer.com/design-tool/
 * Description: You can react on any selected part of the page.
 * Author: WpRight
 * Author URI: https://www.fiverr.com/wpright
 * Version: 1.0
 */
add_action( 'init', 'design_tool' );

function design_tool() {
	add_shortcode( 'show_upload_design', 'design_upload_design' );
	add_shortcode( 'show_design_tool', 'design_tool_form' );
	add_action( 'wp_enqueue_scripts', 'fabric_scripts' );
	include 'wp-facebook-login/facebook-login.php';
	include 'slider/slider.php';
}
function fabric_scripts() {
	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
		));
		wp_enqueue_script( 'fabric_js', plugins_url() . '/reactor/js/fabric.js' );
		wp_enqueue_script( 'design_tool_js', plugins_url() . '/reactor/js/design_tool.js' );
		wp_enqueue_script( 'slider_js', plugins_url() . '/reactor/js/slider.js' );

		wp_enqueue_style( 'design_tool_css', plugins_url() . '/reactor/css/design_tool.css' );
}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
function design_wallpaper() {
        $upload_directory = wp_upload_dir();
		print_r($upload_directory);
		
        $error = NULL;
		
       //if ($_FILES['file_upload']['tmp_name']) {
       if(isset($_POST['submit_design'])){
		   $design_name = $_POST['design_name'];
		   print_r($design_name);
		   $file_type = '.png';
                include('include/class.simple-image.php');
                $image = new SimpleImage();
                $image->load($upload_directory['basedir'] . '/admin/'.$design_name.$file_type );
                $image->resize(722, 508);
                $image->save($upload_directory['basedir'] . '/admin/murg.png');
                $url = get_permalink(get_option('wdyw_custom_wallpaper'));
				print_r($url);
                // Returns a string if the URL has parameters or NULL if not
                if ($query) {
                    $url .= '&filename=' . urlencode($upload_directory['baseurl'] . '/admin/murg.png');
                } else {
                    $url .= '?filename=' . urlencode($upload_directory['baseurl'] . '/admin/murg.png');
                }
                ?>
                <script>
                    window.location = '<?php echo $url; ?>';
                </script> 
                <?php
        }
        include('include/upload.php');
}
function post_love_add_love() {
	global $current_user;
	get_currentuserinfo();
	 
	$upload_dir = wp_upload_dir();
	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
	$img_64 = $_REQUEST['canvas_img'];
	$emoji_reaction = $_REQUEST['emoji_reaction'];
	$design_name = $_REQUEST['canvas_img1'];
	$file_type='.jpeg';
	
	$path = $user_dirname.'/'.$design_name.$file_type;
	print_r($path);
	print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	

	//Insert Post 
	global $post;
	print_r($path);
	//if($path){
		$user_name = $current_user->user_login;
		// Create post object
		$my_post = array(
		  'post_title'    => "Reaction $user_name",
		  'post_content'  => "test",
		  'post_status'   => 'publish'
		);
		 
		// Insert the post into the database
		$parent_post_id = wp_insert_post( $my_post );		
	//}
	 update_post_meta( $parent_post_id, 'reaction', $emoji_reaction );

	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $path ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $path ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $path, $parent_post_id );

	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $path );
	wp_update_attachment_metadata( $attach_id, $attach_data );

	set_post_thumbnail( $parent_post_id, $attach_id );

	
	 
	
}
function design_upload_design(){
	if ( ! function_exists( 'wp_handle_upload' ) ){
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			global $post;
			if(isset($_POST['submit_img'])){
				$uploadedfile = $_FILES['canvas_img'];
				
				$upload_overrides = array( 'test_form' => false );
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				$post_id = 450;
				$attachment = array(
				'guid' => $movefile['url'] . '/' . basename( $movefile['file'] ), 
				'post_mime_type' => $movefile['type'],
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $movefile['file'] ) ),
				'post_content' => '',
				'post_status' => 'publish',
				'key' => '_visibility'
				);
				
				wp_insert_attachment( $attachment, $movefile['file'], $post_id );
				
			}
?>

<form role="form" action="" method="post" id="addmenu" enctype="multipart/form-data" method="post">  First name:<br>
 
  <br>
 file:<br>
  <input type="file" name="canvas_img">
  <br><br>
  <input type="submit" name="submit_img" value="Submit">
</form>
<br />
<?php
}

function design_tool_form() {
	
//ob_start();
?>
	<?php echo do_shortcode('[psc_print_post_slider_carousel]'); ?>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
	<input id="rt_react" type="button" value="Click to React"/>
	<!-- Trigger/Open The Modal -->

	<div id="previewImage" style="display:none;">
	</div>
<div id="preloader" style="display:none;">
<img scr="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif">
	</div>


<!-- The Modal -->
	<div id="reaction_window" style="display:none;">
	 <span class="close">&times;</span>
	<?php echo do_shortcode('[fbl_login_button redirect="" hide_if_logged="" size="large" type="continue_with" show_face="true"]');?>
	<canvas id="canvas" width="1000" height="500"></canvas>
<<<<<<< HEAD
	<input type="text" value="" id="design_name">
	<input id="btn-Preview-Image" type="button" value="Save Your Reaction"/>
	<?php
	echo emoji();
	?>
=======
<?php
echo emoji();
?>
>>>>>>> 1ce1604eddcf5e1df1cb3548f7a2e76150976d41
	</div>


<?php
?>
<script>
var canvas1 = new fabric.Canvas('canvas', { isDrawingMode: true });
canvas1.freeDrawingBrush.color = '#0000FF';
canvas1.freeDrawingBrush.width = parseInt(5, 10) || 1;
jQuery(document).ready(function(){
jQuery("#rt_react").on('click', function () {
	//jQuery("#previewImage").html('');
	html2canvas(document.body, { type: 'view' }).then(function(canvas) {
	jQuery('canvas').attr('id', 'newcanvas');
	getCanvas = canvas;
	var imgageData = getCanvas.toDataURL("image/png");
		fabric.Image.fromURL(imgageData, function(img) {
		canvas1.backgroundImage = img;
		canvas1.renderAll();
		});
	});
	jQuery("#reaction_window").show();
	//jQuery("#previewImage").html(canvas1);
});
 

var getCanvas; // global variable
var element =jQuery("#html-content-holder"); // global variable
 
jQuery(".um-message-insert-emo").on('click', function () {
	jQuery("#preloader").show();
	jQuery("#reaction_window").hide();
	var emoji_url = jQuery(this).attr('data');
	/* fabric.Image.fromURL(emoji_url, function (img) {
		  var oImg = img.set({left: 0, top: 0, angle: 00}).scale(0.9);
		  canvas1.add(oImg).renderAll();
		  var a = canvas1.setActiveObject(oImg);
		  var dataURL = canvas1.toDataURL({format: 'png', quality: 0.8});
		}); */
	var canvasData = canvas1.toDataURL("image/png");
	 localStorage.setItem("canvas_image", canvasData);
	var num = 1;
	var design_name = 'design'+num;
	var canvasData = localStorage.getItem("canvas_image");

	var postData = "canvasData="+canvasData;
	var debugConsole= document.getElementById("debugConsole");  
	jQuery.ajax({
		url : postlove.ajax_url,
		type : 'post',
		data : {
			action : 'post_love_add_love',
			canvas_img : canvasData,
			emoji_reaction : emoji_url,
			canvas_img1 : design_name
		},
		success : function( response ) {
			jQuery("#preloader").hide();
			jQuery("#poststuff").show();
		}
	});

	return false; 
	
});

});

jQuery("#post_slider").hide();

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
//var btn = document.getElementById("rt_react");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
/* btn.onclick = function() {
    modal.style.display = "block";
} */

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
<?php
}
function emoji() {
	$emojis_url = plugins_url( 'images', __FILE__ );
	$emoji = array(
		':applause'=>$emojis_url.'/clapping-hands.png',
		':heart_eyes:'=>$emojis_url.'/1f60d.png',
		":'("=>$emojis_url.'/1f622.png',
		':joy:'=>$emojis_url.'/1f602.png',
		':rage:'=>$emojis_url.'/1f621.png',
		':hushed:'=>$emojis_url.'/1f62f.png');
		?>
		<div class="um-message-emoji">
			<a href="#" class="um-message-emo"><img src="<?php echo um_messaging_url . 'assets/img/emoji_init.png'; ?>" alt="" title="" /></a>
			<span class="um-message-emolist">

		<?php foreach( $emoji as $emoji_code => $emoji_url ) { ?>

			<span data="<?php echo $emoji_url; ?>" title="<?php echo $emoji_code; ?>" class="um-message-insert-emo">
				<img src="<?php echo $emoji_url; ?>" alt="<?php echo $emoji_code; ?>" title="<?php echo $emoji_code; ?>" class="rec-emoji">
			</span>
		<?php
		} ?>

		</span>
		</div>

		<?php
	}