<?php
/* Plugin Name: WP Screenshot
 * Plugin URI: http://www.larsbachmann.dk/screenshots-af-websites-i-wordpress.html
 * Description: Just insert a simple shortcode to show a screenshot of any website.
 * Author: Lars Bachmann
 * Author URI: http://www.larsbachmann.dk/
 * Stable tag: 1.5
 * Version: 1.5
 */
 
function myScreenshot($atts, $content = null) {
	 global $post;
	$crunchifyURL = get_permalink( $post->ID );
	 extract(shortcode_atts(array(  
        "width" => 'width'  
    ), $atts));  
if (!preg_match('#^http(s)?://#', $crunchifyURL)) {
		    $crunchifyURL = 'http://' . $crunchifyURL;
		}
 
		$urlParts = parse_url($crunchifyURL);
 
		// Get Domain. i.e. crunchify.com
		$domain = preg_replace('/^www\./', '', $urlParts['host']);
 
		// Get Path. i.e. /path
		$path = preg_replace('/^www\./', '', $urlParts['path']);
 
		// Merge to generate complete URL
		$crunchifyURL = $domain. $path;
		echo $crunchifyURL;
return '<img src="https://s.wordpress.com/mshots/v1/http%3A%2F%2F'.$crunchifyURL.'?w=' . esc_attr($width) . '" />';
}
add_shortcode("screenshot", "myScreenshot");

add_action( 'init', 'design_tool' );

function design_tool() {
	add_shortcode( 'show_upload_design', 'design_upload_design' );
	add_shortcode( 'show_design_tool', 'design_tool_form' );
	add_action( 'wp_enqueue_scripts', 'fabric_scripts' ); 

}
function fabric_scripts() {
	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
		));
		wp_enqueue_script( 'fabric_js', plugins_url() . '/wp-screenshot/js/fabric.js' );
		wp_enqueue_style( 'design_tool', plugins_url() . '/wp-screenshot/css/design_tool.css' );
}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
function design_wallpaper() {
        $upload_directory = wp_upload_dir();
		print_r($upload_directory);
		
        $error = NULL;
		
       //if ($_FILES['file_upload']['tmp_name']) {
       if(isset($_POST['submit_design'])){
		   $design_name = $_POST['design_name'];
		   print_r($design_name);
		   $file_type = '.png';
                /* $rand = rand();
                $datetime = strtotime(date('Y-m-d H:i:s'));
                $filename = $rand . $datetime . ".jpg";
                $filename_resized = $rand . $datetime . "-resized.jpg";
                if (!move_uploaded_file($_FILES['file_upload']['tmp_name'], $upload_directory['basedir'] . '/custom-wallpapers/' . $filename)) {
                    $error = "Error in uploading file";
                } */

                include('include/class.simple-image.php');
                $image = new SimpleImage();
                $image->load($upload_directory['basedir'] . '/admin/'.$design_name.$file_type );
                $image->resize(722, 508);
                $image->save($upload_directory['basedir'] . '/admin/murg.png');
                $url = get_permalink(get_option('wdyw_custom_wallpaper'));
				print_r($url);
                // Returns a string if the URL has parameters or NULL if not
                if ($query) {
                    $url .= '&filename=' . urlencode($upload_directory['baseurl'] . '/admin/murg.png');
                } else {
                    $url .= '?filename=' . urlencode($upload_directory['baseurl'] . '/admin/murg.png');
                }
                ?>
                <script>
                    window.location = '<?php echo $url; ?>';
                </script> 
                <?php
        }
        include('include/upload.php');
}
function post_love_add_love() {
	global $current_user;
	get_currentuserinfo();
	 
	$upload_dir = wp_upload_dir();
	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
	$img_64 = $_REQUEST['canvas_img'];
	$design_name = $_REQUEST['canvas_img1'];
	$file_type='.png';
	
	$path = $user_dirname.'/'.$design_name.$file_type;
	print_r($path);
	print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	
}
function design_upload_design(){
	if ( ! function_exists( 'wp_handle_upload' ) ){
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			global $post;
			if(isset($_POST['submit_img'])){
				$uploadedfile = $_FILES['canvas_img'];
				
				$upload_overrides = array( 'test_form' => false );
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				$post_id = 450;
				$attachment = array(
				'guid' => $movefile['url'] . '/' . basename( $movefile['file'] ), 
				'post_mime_type' => $movefile['type'],
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $movefile['file'] ) ),
				'post_content' => '',
				'post_status' => 'publish',
				'key' => '_visibility'
				);
				
				wp_insert_attachment( $attachment, $movefile['file'], $post_id );
				
			}
?>

<form role="form" action="" method="post" id="addmenu" enctype="multipart/form-data" method="post">  First name:<br>
 
  <br>
 file:<br>
  <input type="file" name="canvas_img">
  <br><br>
  <input type="submit" name="submit_img" value="Submit">
</form>
<br />
<?php
}

function design_tool_form() {
if(isset($_POST['submit_size'])){
			$canvas_width = $_POST['width'];
			$canvas_height = $_POST['height'];
		}
ob_start();
	 global $post;
	$crunchifyURL = get_permalink( $post->ID );
	$width = 1000;
if (!preg_match('#^http(s)?://#', $crunchifyURL)) {
		    $crunchifyURL = 'http://' . $crunchifyURL;
		}
 
		$urlParts = parse_url($crunchifyURL);
 
		// Get Domain. i.e. crunchify.com
		$domain = preg_replace('/^www\./', '', $urlParts['host']);
 
		// Get Path. i.e. /path
		$path = preg_replace('/^www\./', '', $urlParts['path']);
 
		// Merge to generate complete URL
		$crunchifyURL = $domain. $path;
		echo $crunchifyURL;
 //echo '<img id="scream" src="https://s.wordpress.com/mshots/v1/http%3A%2F%2F'.$crunchifyURL.'?w=' . esc_attr($width) . '" />';
//echo '<img id="scream" src="https://s.wordpress.com/mshots/v1/http%3A%2F%2Freactor.logicsbuffer.com/design-tool/?w=500" />';
?>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<div id="html-content-holder" style="background-color: #F0F0F1; color: #00cc65; width: 100%;
        padding-left: 25px; padding-top: 10px;">
        <strong>Codepedia.info</strong><hr/>
        <h3 style="color: #3e4b51;">
            Html to canvas, and canvas to proper image
        </h3>
        <p style="color: #3e4b51;">
            <b>Codepedia.info</b> is a programming blog. Tutorials focused on Programming ASP.Net,
            C#, jQuery, AngularJs, Gridview, MVC, Ajax, Javascript, XML, MS SQL-Server, NodeJs,
            Web Design, Software</p>
        <p style="color: #3e4b51;">
            <b>html2canvas</b> script allows you to take "screenshots" of webpages or parts
            of it, directly on the users browser. The screenshot is based on the DOM and as
            such may not be 100% accurate to the real representation.
        </p>
    </div>

	<input id="rt_react" type="button" value="Click to React"/>
	<!-- Trigger/Open The Modal -->

	<div id="previewImage" style="display:none;">
	</div>



<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
	<?php echo do_shortcode('[fbl_login_button redirect="" hide_if_logged="" size="large" type="continue_with" show_face="true"]');?>	
	<div id="reaction_window" style="display:none;">
	<canvas id="canvas" width="1000" height="800"></canvas>
	<input type="text" value="" id="design_name">
	<input id="btn-Preview-Image" type="button" value="Save Your Reaction"/>
	</div>
  </div>

</div>

<script>
var canvas1 = new fabric.Canvas('canvas', { isDrawingMode: true });
jQuery(document).ready(function(){
jQuery("#rt_react").on('click', function () {
 jQuery("#reaction_window").show();
 });
var getCanvas; // global variable
var element =jQuery("#main"); // global variable
html2canvas(element,{
 onrendered: function (canvas) {
		jQuery("#previewImage").append(canvas);
		jQuery('canvas').attr('id', 'newcanvas');
		getCanvas = canvas;
		console.log(getCanvas);
		var imgageData = getCanvas.toDataURL("image/png");
		fabric.Image.fromURL(imgageData, function(img) {
		canvas1.backgroundImage = img;
		canvas1.backgroundImage.width = 1000;
		canvas1.backgroundImage.height = 800;
		canvas1.renderAll();
		});
	 }
 });
jQuery("#btn-Preview-Image").on('click', function () {
	 var canvasData = canvas1.toDataURL("image/png");
	 localStorage.setItem("canvas_image", canvasData);

	 console.log(canvasData);
	 //var testCanvas = document.getElementById("canvas");  
	var design_name = document.getElementById("design_name").value;
	var canvasData = localStorage.getItem("canvas_image");

	var postData = "canvasData="+canvasData;
	var debugConsole= document.getElementById("debugConsole");  
	jQuery.ajax({
		url : postlove.ajax_url,
		type : 'post',
		data : {
			action : 'post_love_add_love',
			canvas_img : canvasData,
			canvas_img1 : design_name
		},
		success : function( response ) {
			//var debugConsole1= document.getElementById("result");  
			//debugConsole1.value = response;
		}
	});

	return false; 
});
function select_mode(){
	 canvas.isDrawingMode = false;
}

function draw_mode(){
	 canvas.isDrawingMode = true;
	 
}

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("rt_react");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
});

function saveViaAJAX(){
	
}
</script>
<style>
/* The Modal (background) */
#rt_react{
	display: block;
    position: fixed;
    bottom: 20px;
    right: 30px;
    z-index: 99;
    border: none;
    outline: none;
    background-color: ##7d3f71;
    color: white;
    cursor: pointer;
    padding: 51px 22px;
    border-radius: 80px;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php
$result = ob_get_clean();
return $result;
}
?>