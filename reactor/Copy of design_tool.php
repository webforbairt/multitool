<?php
/*

Plugin Name: Design Tool

*/
add_action( 'init', 'design_tool' );

function design_tool() {
	add_shortcode( 'show_upload_design', 'design_upload_design' );
	add_shortcode( 'show_design_tool', 'design_tool_form' );
	add_action( 'wp_enqueue_scripts', 'fabric_scripts' ); 

}
function fabric_scripts() {
	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
		));
		wp_enqueue_script( 'fabric_js', plugins_url() . '/design-tool/js/fabric.min.js' );
		wp_enqueue_style( 'design_tool', plugins_url() . '/design-tool/css/design_tool.css' );
}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
function design_wallpaper() {
        $upload_directory = wp_upload_dir();
		print_r($upload_directory);
		
        $error = NULL;
		
       //if ($_FILES['file_upload']['tmp_name']) {
       if(isset($_POST['submit_design'])){
		   $design_name = $_POST['design_name'];
		   print_r($design_name);
		   $file_type = '.png';
                /* $rand = rand();
                $datetime = strtotime(date('Y-m-d H:i:s'));
                $filename = $rand . $datetime . ".jpg";
                $filename_resized = $rand . $datetime . "-resized.jpg";
                if (!move_uploaded_file($_FILES['file_upload']['tmp_name'], $upload_directory['basedir'] . '/custom-wallpapers/' . $filename)) {
                    $error = "Error in uploading file";
                } */

                include('include/class.simple-image.php');
                $image = new SimpleImage();
                $image->load($upload_directory['basedir'] . '/admin/'.$design_name.$file_type );
                $image->resize(722, 508);
                $image->save($upload_directory['basedir'] . '/admin/murg.png');
                $url = get_permalink(get_option('wdyw_custom_wallpaper'));
				print_r($url);
                // Returns a string if the URL has parameters or NULL if not
                if ($query) {
                    $url .= '&filename=' . urlencode($upload_directory['baseurl'] . '/admin/murg.png');
                } else {
                    $url .= '?filename=' . urlencode($upload_directory['baseurl'] . '/admin/murg.png');
                }
                ?>
                <script>
                    window.location = '<?php echo $url; ?>';
                </script> 
                <?php
        }
        include('include/upload.php');
}
function post_love_add_love() {
	global $current_user;
	get_currentuserinfo();
	 
	$upload_dir = wp_upload_dir();
	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
	$img_64 = $_REQUEST['canvas_img'];
	$design_name = $_REQUEST['canvas_img1'];
	$file_type='.png';
	
	$path = $user_dirname.'/'.$design_name.$file_type;
	print_r($path);
	print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	
}
function design_upload_design(){
	if ( ! function_exists( 'wp_handle_upload' ) ){
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			global $post;
			if(isset($_POST['submit_img'])){
				$uploadedfile = $_FILES['canvas_img'];
				
				$upload_overrides = array( 'test_form' => false );
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				$post_id = 450;
				$attachment = array(
				'guid' => $movefile['url'] . '/' . basename( $movefile['file'] ), 
				'post_mime_type' => $movefile['type'],
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $movefile['file'] ) ),
				'post_content' => '',
				'post_status' => 'publish',
				'key' => '_visibility'
				);
				
				$attach_id = wp_insert_attachment( $attachment, $movefile['file'], $post_id );
				print_r($attach_id);
				
			}
?>

<form role="form" action="" method="post" id="addmenu" enctype="multipart/form-data" method="post">  First name:<br>
 
  <br>
 file:<br>
  <input type="file" name="canvas_img">
  <br><br>
  <input type="submit" name="submit_img" value="Submit">
</form>
<br />
<?php
}

function design_tool_form() {
if(isset($_POST['submit_size'])){
			$canvas_width = $_POST['width'];
			$canvas_height = $_POST['height'];
		}
ob_start();
?>
<canvas id="canvas" width="<?php echo $canvas_width; ?>" height="<?php echo $canvas_height; ?>">asdasd</canvas>
<div id="lable_height" class="canvas_lable_height"><p>&nbsp;Ft</p>
</div>  
<div id="lable_width" class="canvas_lable_width"><p>&nbsp;Ft</p></div>
<input type="file" id="file"><br />
<input type="file" id="bg_img"><br />
<button id="copy">Copy</button>
<button id="paste">Paste</button>
<button onclick="select_mode()" id="select">Selection mode</button>
<button onclick="draw_mode()" id="draw">Drawing mode</button>
<button onclick="deleteObjects()" id="delete">Delete</button>
<input type="button" value="undo" onclick="undo()">
<input type="button" value="redo" onclick="redo()">
<input type="button" value="clear" onclick="clearcan()">
<script>
var canvas = new fabric.Canvas('canvas', { isDrawingMode: false });

function select_mode(){
	 canvas.isDrawingMode = false;
}
function draw_mode(){
	 canvas.isDrawingMode = true;
	 
}
var state = [];
var mods = 0;
canvas.on(
    'object:modified', function () {
    updateModifications(true);
},
    'object:added', function () {
    updateModifications(true);
});

function updateModifications(savehistory) {
    if (savehistory === true) {
        myjson = JSON.stringify(canvas);
        state.push(myjson);
    }
}

undo = function undo() {
    if (mods < state.length) {
        canvas.clear().renderAll();
        canvas.loadFromJSON(state[state.length - 1 - mods - 1]);
        canvas.renderAll();
        //console.log("geladen " + (state.length-1-mods-1));
        //console.log("state " + state.length);
        mods += 1;
        //console.log("mods " + mods);
    canvas.renderAll();
	}
	
}

redo = function redo() {
    if (mods > 0) {
        canvas.clear().renderAll();
        canvas.loadFromJSON(state[state.length - 1 - mods + 1]);
        canvas.renderAll();
        //console.log("geladen " + (state.length-1-mods+1));
        mods -= 1;
        //console.log("state " + state.length);
        //console.log("mods " + mods);
    canvas.renderAll();
	}
	
}

clearcan = function clearcan() {
    canvas.clear().renderAll();
    newleft = 0;
}
document.getElementById('file').addEventListener("change", function (e) {
  var file = e.target.files[0];
  var reader = new FileReader();
  reader.onload = function (f) {
    var data = f.target.result;                    
    fabric.Image.fromURL(data, function (img) {
      var oImg = img.set({left: 0, top: 0, angle: 00}).scale(0.9);
      canvas.add(oImg).renderAll();
      var a = canvas.setActiveObject(oImg);
      var dataURL = canvas.toDataURL({format: 'png', quality: 0.8});
    });
  };
  reader.readAsDataURL(file);
  canvas.renderAll();
});
document.getElementById('bg_img').addEventListener("change", function (e) {
  var file = e.target.files[0];
  var reader = new FileReader();
  reader.onload = function (f) {
    var data = f.target.result;
    fabric.Image.fromURL(data, function(img) {
    canvas.backgroundImage = img;
	canvas.backgroundImage.width = 800;
    canvas.backgroundImage.height = 800;
	canvas.renderAll();
	});
  };
  reader.readAsDataURL(file);
  canvas.renderAll();
});
function deleteObjects(){
	var activeObject = canvas.getActiveObject(),
    activeGroup = canvas.getActiveGroup();
    if (activeObject) {
        if (confirm('Are you sure Delete this object?')) {
            canvas.remove(activeObject);
        }
    }
    else if (activeGroup) {
        if (confirm('Are you sure Delete this object?')) {
            var objectsInGroup = activeGroup.getObjects();
            canvas.discardActiveGroup();
            objectsInGroup.forEach(function(object) {
            canvas.remove(object);
            });
        }
    }
	canvas.renderAll();
}

var clipboard = null;

var copy =  document.getElementById("copy");
var paste =  document.getElementById("paste");

copy.onclick = Copy;
paste.onclick = Paste;

function Copy() {
    // Single Object
    if(canvas.getActiveObject()) {
        // Does this object require an async clone?
        if(!fabric.util.getKlass(canvas.getActiveObject().type).async) {
            clipboard = canvas.getActiveObject().clone();
        } else {
            canvas.getActiveObject().clone(function(clone) {
                clipboard= clone;
            });
        }
    }

    // Group of Objects (all groups require async clone)
    if(canvas.getActiveGroup()) {
        canvas.getActiveGroup().clone(function(clone) {
            clipboard = clone;
        });
    }
}


function Paste() {
    // Do we have an object in our clipboard?
    if(clipboard) {
        // Lets see if we need to clone async 
        if(!fabric.util.getKlass(clipboard.type).async) {
            var obj = clipboard.clone();
            obj.setTop(obj.top += 10);
            obj.setLeft(obj.left += 10);            
            canvas.add(obj);
            // We do not need to clone async, all groups require async clone
            canvas.setActiveObject(obj);
            clipboard = obj;
        }  else {
            clipboard.clone(function(clone) {
                clone.setTop(clone.top += 10);
                clone.setLeft(clone.left += 10);
                clone.forEachObject(function(obj){
										canvas.add(obj);
								});
                
   							canvas.deactivateAll();

                // We need to clone async, but this doesnt mean its a group
                if(clipboard.isType("group")) {
                    canvas.setActiveGroup(clone);
                } else {
                    canvas.setActiveObject(clone);
                }
                clipboard = clone;
            });
        }
    }
    canvas.renderAll();
}
function set_height(){		
	var can_height= document.getElementById('can_height').value;
  	canvas.setHeight(can_height);
	canvas.renderAll();
}
function set_width(){		
	var can_width= document.getElementById('can_width').value;
	canvas.setWidth(can_width);
	canvas.renderAll();
} 
function Addtext() { 
canvas.add(new fabric.IText('Tap and Type', { 
      left: 0,
      top: 0,
      fontFamily: 'arial black',
      fill: '#333',
	    fontSize: 50
}));
}

function set_color(){
	var text_color= document.getElementById('text-color').value;
	canvas.getActiveObject().setFill(text_color);
	canvas.renderAll();
}
function set_font_family(){
	var text_font= document.getElementById('font-family').value;
	canvas.getActiveObject().setFontFamily(text_font);
	canvas.renderAll();
}
function set_text_lines_bg_color(){
	var text_lines_bg= document.getElementById('text-lines-bg-color').value;
	canvas.getActiveObject().setTextBackgroundColor(text_lines_bg);
	canvas.renderAll();
}
function set_text_bg_color(){
	var text_bg= document.getElementById('text-bg-color').value;
	canvas.getActiveObject().setBackgroundColor(text_bg);
	canvas.renderAll();
}
function set_text_align(){
	var text_align= document.getElementById('text-align').value;
	canvas.getActiveObject().setTextAlign(text_align);
	canvas.renderAll();
}
function set_stroke_color(){
	var stroke_color= document.getElementById('text-stroke-color').value;
	canvas.getActiveObject().setStroke(stroke_color);
	canvas.renderAll();
}
function set_stroke_width(){
	var stroke_width= document.getElementById('text-stroke-width').value;
	canvas.getActiveObject().setStrokeWidth(stroke_width);
	canvas.renderAll();
}
function set_font_size(){
	var font_size= document.getElementById('text-font-size').value;
	canvas.getActiveObject().setFontSize(font_size);
	canvas.renderAll();
}
function set_line_height(){
	var line_height= document.getElementById('text-line-height').value;
	canvas.getActiveObject().setLineHeight(line_height);
	canvas.renderAll();
}
function text_bold(){
 var bold = document.getElementById("text_bold").checked;
	  if(bold == true){
		canvas.getActiveObject().set("fontWeight", "bold");
		canvas.renderAll();
	  }else{
		 canvas.getActiveObject().set("fontWeight", "");
		 canvas.renderAll();
	  }
}

function text_italic(){
 var italic = document.getElementById("text_italic").checked;
	  if(italic == true){
		canvas.getActiveObject().set("fontStyle", "italic");
		canvas.renderAll();
	  }else{
		 canvas.getActiveObject().set("textDecoration", "");
		 canvas.renderAll();
	  }
}
function text_underline(){
 var underline = document.getElementById("text_underline").checked;
	  if(underline == true){
		canvas.getActiveObject().set("textDecoration", "underline");
		canvas.renderAll();
	  }else{
		 canvas.getActiveObject().set("textDecoration", "");
		 canvas.renderAll();
	  }
}

function text_linethrough(){
 var linethrough = document.getElementById("text_linethrough").checked;
	  if(linethrough == true){
		 canvas.getActiveObject().set("textDecoration", "line-through");
		canvas.renderAll();
	  }else{
		  canvas.getActiveObject().set("textDecoration", "");
		 canvas.renderAll();
	  }
}

function text_overline(){
 var overline = document.getElementById("text_overline").checked;
	  if(overline == true){
		canvas.getActiveObject().set("textDecoration", "overline");
		canvas.renderAll();
	  }else{
		 canvas.getActiveObject().set("textDecoration", "");
		 canvas.renderAll();
	  }
}
function set_unit(){
		x = document.getElementById('can_height').value;
		y = document.getElementById('can_width').value;
		var current_unit = document.getElementById('unit').value;
		jQuery('#lable_height').html("<p>"+y + "&nbsp;" + current_unit+"</p>");
		jQuery('#lable_width').html("<p>"+x + "&nbsp;" + current_unit+"</p>");
		console.log(current_unit);
		switch (current_unit) {
			case 'foot':
				unit_name = "Foot";
				foot_width = document.getElementById('can_width').value;
				foot_height = document.getElementById('can_height').value;
				break;
			case 'cm':
				unit_name = "CM";
				foot_width = document.getElementById('can_width').value / 30.48;
				foot_height = document.getElementById('can_height').value / 30.48;
				break;
			case 'm':
				unit_name = "Meters";
				foot_width = document.getElementById('can_width').value / 0.3048000;
				foot_height = document.getElementById('can_height').value / 0.3048000;
				break;
			case 'mm':
				unit_name = "MM";
				foot_width = document.getElementById('can_width').value / 304.8000;
				foot_height = document.getElementById('can_height').value / 304.8000;
				break;
			case 'inches':
				unit_name = "Inches";
				foot_width = document.getElementById('can_width').value / 12;
				foot_height = document.getElementById('can_height').value / 12;
				break;
		}
		console.log(foot_width);
		console.log(foot_height);
		var px_width = foot_width * 1151.9999999832;
		var px_height = foot_height * 1151.9999999832;
		canvas.setWidth(px_width);
		canvas.setHeight(px_height);
		total_sq_feet = foot_width * foot_height;
		total_sq_feet = total_sq_feet.toFixed(2);
		
}

function set_gradiant_bg_color(){
bg1 = document.getElementById('bg-gr-color').value
bg2 = document.getElementById('bg-gr-color2').value
var bgrect = new fabric.Rect({
  left: 0,
  top: 0,
  width: canvas.getWidth(),
  height: canvas.getHeight(),
  selectable:false
});


bgrect.setGradient('fill', {
  x1: 0,
  y1: 0,
  x2: 0,
  y2: bgrect.height,
  colorStops: {
    0: bg1,
    1: bg2
  }
});

canvas.add(bgrect)
canvas.renderAll();
}

function set_bg_color(){
background = document.getElementById('bg-color').value
canvas.setBackgroundColor(background, canvas.renderAll.bind(canvas));
canvas.renderAll();
}
</script>
<div class="font_all_content text_box" id="fontoption">
 <button class="add_text" onclick="Addtext()">Add Texto</button>
        <div class="font_option_otr">
            <h2>Add Text</h2>

            <div class="enter_txt">
			
			<div class="wall_size col-sm-4">
				<h3>Enter your wall size</h3>
            <div class="wall_unit">
			
                <div class="row">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Units: </label>
                    </div>
                    <div class="small-9 columns">
                        <select id="unit" onchange="set_unit()">                            
                            <option selected="selected" value="foot">Foot</option>
                            <option value="cm">CM</option>
                            <option value="m">Meter</option>
                            <option value="inches">Inches</option>
                            <option value="mm">MM</option>
                        </select>
                    </div>
                </div>

			<p>height</p>
                <input onKeyup="set_unit()" type="text" id="can_height" value="">
			<p>width</p>
                <input onKeyup="set_unit()" type="text" id="can_width"  value="">
            </div>
                <div class="row">
                    <div class="small-3 columns">
                        <label for="design_yout_wall_height" class="middle">Height: </label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="design_yout_wall_height" value="<?php echo get_option('wdyw_default_height'); ?>">
                    </div>
                </div>
            </div>
        </div>
            <div id="text-wrapper" style="margin-top: 10px" ng-show="getText()">

    <div id="text-controls">
	  <input onchange="set_color()" type="color" value="" id="text-color" size="10">
      <label for="font-family" style="display:inline-block">Font family:</label>
      <select onchange="set_font_family()" id="font-family">
        <option value="arial">Arial</option>
        <option value="helvetica" selected>Helvetica</option>
        <option value="myriad pro">Myriad Pro</option>
        <option value="delicious">Delicious</option>
        <option value="verdana">Verdana</option>
        <option value="georgia">Georgia</option>
        <option value="courier">Courier</option>
        <option value="comic sans ms">Comic Sans MS</option>
        <option value="impact">Impact</option>
        <option value="monaco">Monaco</option>
        <option value="optima">Optima</option>
        <option value="hoefler text">Hoefler Text</option>
        <option value="plaster">Plaster</option>
        <option value="engagement">Engagement</option>
      </select>
      <br>
      <label for="text-align" style="display:inline-block">Text align:</label>
      <select onchange="set_text_align()" id="text-align">
        <option value="left">Left</option>
        <option value="center">Center</option>
        <option value="right">Right</option>
        <option value="justify">Justify</option>
      </select>
      <div>
        <label for="text-bg-color">Background color:</label>
        <input onchange="set_text_bg_color()" type="color" value="" id="text-bg-color" size="10">
      </div>
      <div>
        <label for="text-lines-bg-color">Background text color:</label>
        <input onchange="set_text_lines_bg_color()" type="color" value="" id="text-lines-bg-color" size="10">
      </div>
      <div>
        <label for="text-stroke-color">Stroke color:</label>
        <input onchange="set_stroke_color()" type="color" value="" id="text-stroke-color">
      </div>
      <div>
	  

        <label for="text-stroke-width">Stroke width:</label>
        <input onchange="set_stroke_width()" type="range" value="1" min="1" max="10" id="text-stroke-width">
      </div>
      <div>
        <label for="text-font-size">Font size:</label>
        <input onchange="set_font_size()" type="range" value="" min="1" max="120" step="1" id="text-font-size">
      </div>
      <div>
        <label for="text-line-height">Line height:</label>
        <input onchange="set_line_height()" type="range" value="" min="0" max="10" step="0.1" id="text-line-height">
      </div>
    </div>
    <div id="text-controls-additional">
		Bold
		<input onchange="text_bold()" id="text_bold" type="checkbox" >
		Italic
		<input onchange="text_italic()" id="text_italic" type="checkbox" >
		Underline
		<input onchange="text_underline()" id="text_underline" type="checkbox" >
		Linethrough
		<input onchange="text_linethrough()" id="text_linethrough" type="checkbox" >
		Overline
		<input onchange="text_overline()" id="text_overline" type="checkbox" >
    </div>
	 <label for="background">BACKGROUND:</label>
	  <div>
        <label for="text-lines-bg-color">Background color:</label>
        <input onchange="set_bg_color()" type="color" value="" id="bg-color" size="10">
      </div>
	  <div>
        <label for="text-lines-bg-color">Gradiant Background color:</label>
        <input onchange="set_gradiant_bg_color()" type="color" value="" id="bg-gr-color" size="10">
        <input onchange="set_gradiant_bg_color()" type="color" value="" id="bg-gr-color2" size="10">
      </div>
</div>
</div>
</div>
The image below consists of randomly generated circles drawn on HTML 5 canvas object. 
<br>You can then press the Save button to save the image.  
<p>
 
     </p><div>  
      </div>	  
      <div>
	<textarea id="debugConsole" rows="10" cols="60">Data</textarea>
	 Design name
	<textarea id="result" rows="10" cols="60"></textarea>
       <p><button onclick="saveViaAJAX();">Save Via AJAX</button>   </p><p>
	</p><div id="debugFilenameConsole">Wait for a while after clicking the button and the filename of the image will be shown to you.  </div>
      </div> 
	<script type="text/javascript">

function saveViaAJAX()
{
	var testCanvas = document.getElementById("canvas");  
	var design_name = document.getElementById("design_name").value;
	var canvasData = testCanvas.toDataURL("image/png");
	var postData = "canvasData="+canvasData;
	var debugConsole= document.getElementById("debugConsole");  
	jQuery.ajax({
		url : postlove.ajax_url,
		type : 'post',
		data : {
			action : 'post_love_add_love',
			canvas_img : canvasData,
			canvas_img1 : design_name
		},
		success : function( response ) {
			var debugConsole1= document.getElementById("result");  
			debugConsole1.value = response;
		}
	});

	return false; 
}
</script>
<?php
design_wallpaper();
$result = ob_get_clean();
return $result;
}