<?php
global $wpdb;
require_once ("paypal/paypalplatform.php");
$current_user = wp_get_current_user();
if(isset($_SESSION['paypal']) && !empty($_SESSION['paypal']))
{
    $paypal_resp = CallPaymentDetails($_SESSION['paypal']['payKey']);
    $itemnumber = $_SESSION['paypal']['item_number'];
    $params = $_SESSION['paypal']['params'];
    $tot_amount = $_SESSION['paypal']['tot_amount'];
    $calendar_post_id = $_SESSION['paypal']['calendar_post_id'];
    
    
    $myrows = $wpdb->get_results( "SELECT * FROM ".DEX_BCCF_TABLE_NAME." WHERE id=".$itemnumber );
    $mycalendarrows = $wpdb->get_results( 'SELECT * FROM '.DEX_BCCF_CONFIG_TABLE_NAME .' WHERE `'.TDE_BCCFCONFIG_ID.'`='.$myrows[0]->calendar);
    if (!defined('CP_BCCF_CALENDAR_ID'))
        define ('CP_BCCF_CALENDAR_ID',$myrows[0]->calendar);
    
    $SYSTEM_EMAIL = dex_bccf_get_option('notification_from_email', DEX_BCCF_DEFAULT_PAYPAL_EMAIL);
    $SYSTEM_RCPT_EMAIL = dex_bccf_get_option('notification_destination_email', DEX_BCCF_DEFAULT_PAYPAL_EMAIL);
    
    $email_subject1 = dex_bccf_get_option('email_subject_confirmation_to_user', DEX_BCCF_DEFAULT_SUBJECT_CONFIRMATION_EMAIL);
    $email_content1 = dex_bccf_get_option('email_confirmation_to_user', DEX_BCCF_DEFAULT_CONFIRMATION_EMAIL);
    $email_subject2 = dex_bccf_get_option('email_subject_notification_to_admin', DEX_BCCF_DEFAULT_SUBJECT_NOTIFICATION_EMAIL);
    $email_content2 = dex_bccf_get_option('email_notification_to_admin', DEX_BCCF_DEFAULT_NOTIFICATION_EMAIL);
    
    $option_calendar_enabled = dex_bccf_get_option('calendar_enabled', DEX_BCCF_DEFAULT_CALENDAR_ENABLED);
    if ($option_calendar_enabled != 'false')
    {
        $information = "Item: ".$mycalendarrows[0]->uname."\n\n".
                       "Date From-To: ".$myrows[0]->booked_time_s." - ".$myrows[0]->booked_time_e."\n\n".
                       $myrows[0]->question;
    }
    else
    {
        $information = "Item: ".$mycalendarrows[0]->uname."\n\n".                      
                       $myrows[0]->question;    
    }                   
    
    $email_content1 = str_replace("%INFORMATION%", $information, $email_content1);   
    $email_content2 = str_replace("%INFORMATION%", $information, $email_content2);
    $attachments = array();
    $firstparam = "";
    $firstattachment = "";
    foreach ($params as $item => $value)        
     {
        if ($value != '' && $firstparam == '') $firstparam = $value;
        $email_content1 = str_replace('<%'.$item.'%>',(is_array($value)?(implode(", ",$value)):($value)),$email_content1);    
        $email_content2 = str_replace('<%'.$item.'%>',(is_array($value)?(implode(", ",$value)):($value)),$email_content2);    
        if (strpos($item,"_link"))
        {            
            $attachments[] = $value;
        }    
        if (strpos($item,"_url"))
        {
            $firstattachment = $value;
        }
     }
    
    $booked_title = isset($params['fieldname2'])?$params['fieldname2']:'' ;
    
    // SEND EMAIL TO USER
    $to = dex_bccf_get_option('cu_user_email_field', DEX_BCCF_DEFAULT_cu_user_email_field);
    $_POST[$to] = $myrows[0]->notifyto;
    if (trim($_POST[$to]) != '')
        wp_mail(trim($_POST[$to]), $email_subject1, $email_content1,
                 "From: \"$SYSTEM_EMAIL\" <".$SYSTEM_EMAIL.">\r\n".
                 "Content-Type: text/plain; charset=utf-8\n".
                 "X-Mailer: PHP/" . phpversion());
    
    if ($payer_email && $payer_email != $_POST[$to])
        wp_mail($payer_email , $email_subject1, $email_content1,
                 "From: \"$SYSTEM_EMAIL\" <".$SYSTEM_EMAIL.">\r\n".
                 "Content-Type: text/plain; charset=utf-8\n".
                 "X-Mailer: PHP/" . phpversion());
    
    
    // SEND EMAIL TO ADMIN
    wp_mail($SYSTEM_RCPT_EMAIL, $email_subject2, $email_content2,
             "From: \"$SYSTEM_EMAIL\" <".$SYSTEM_EMAIL.">\r\n".
             "Content-Type: text/plain; charset=utf-8\n".
             "X-Mailer: PHP/" . phpversion(), $attachments);
    
    $current_user = wp_get_current_user();
    
    wp_mail($current_user->user_email, "New reservation request", $information,
         "Content-Type: text/plain; charset=utf-8\n".
             "X-Mailer: PHP/" . phpversion());
    
    $post = array (
                    'post_author' => $current_user->ID,
                    'post_title'    => $firstparam,
                    'post_content' => '<a href="'.$firstattachment.'"><img src="'.$firstattachment.'" /></a>',                    
                    'post_status' => 'future',
                    'post_date' =>  date("Y-m-d H:i:s", strtotime($myrows[0]->booked_time_unformatted_s)),
                    'post_date_gmt' =>  date("Y-m-d H:i:s", strtotime($myrows[0]->booked_time_unformatted_s)),                  
                    'post_type' => 'post'
                   );
    $id = wp_insert_post( $post );
    
    wp_set_post_terms( $id, $params["category"], 'category' );
    
    $post = array (
                    'post_author' => $current_user->ID,
                    'post_title'    => 'Featured image for post #'.$id,                    
                    'post_parent' => $id,
                    'post_status' => 'inherit',
                    'post_type' => 'attachment',
                    'post_mime_type' => 'image/jpg'
                   );    
    $id2 = wp_insert_post( $post );
    
    $image = wp_get_image_editor( $firstattachment );
    if ( ! is_wp_error( $image ) ) {
        //$image->rotate( 90 );
        $uploads = wp_upload_dir();
        $image->resize( 90, 90, true );
        $extension = pathinfo($firstattachment,PATHINFO_EXTENSION);
        $filename = pathinfo($firstattachment,PATHINFO_FILENAME);
        $image->save( $uploads['path'].'/'.$filename.'_thumb.'.$extension );
    }
     
    for ($i=1;$i<100;$i++)
        $firstattachment = str_replace ('sites/'.$i.'/','',$firstattachment);    
    update_post_meta($id2, "_wp_attached_file", substr($firstattachment,strpos($firstattachment,'/uploads/')+strlen('/uploads/')));
    update_post_meta($id, "_thumbnail_id", $id2);
    update_post_meta($id, "bccf_expiry_date", date("Y-m-d H:i:s", strtotime($myrows[0]->booked_time_unformatted_e)));
    update_post_meta($id, "bccf_calendar_post_id", $calendar_post_id);
    update_post_meta($id, "royalslider_custom_url", $params['fieldname3']);
    $rows_affected = $wpdb->insert( TDE_BCCFCALENDAR_DATA_TABLE, array( 'reservation_calendar_id' => $myrows[0]->calendar,
                                                                        'datatime_s' => date("Y-m-d H:i:s", strtotime($myrows[0]->booked_time_unformatted_s)),
                                                                        'datatime_e' => date("Y-m-d H:i:s", strtotime($myrows[0]->booked_time_unformatted_e)),
                                                                        'title' => ($_POST[$to]?$_POST[$to]:"Booked"),
                                                                        'description' => str_replace("\n","<br />", $information),
                                                                        'user_id'=>$current_user->ID,
                                                                        'post_id'=>$id,
                                                                        'item_number' =>$itemnumber,
                                                                        'booked_title' => $booked_title
                                                                         ) );
    
    $paypal_insert = $wpdb->insert( DEX_BCCF_PAYPAL, array( 'itemId' => $itemnumber,
                                                            'calender_data_id' => $wpdb->insert_id,
                                                            'post_id' => $id,
                                                            'sender_email' => $paypal_resp['senderEmail'],
                                                            'trackingId' => $paypal_resp['trackingId'],
                                                            'payKey' => $paypal_resp['payKey'],
                                                            'status' => $paypal_resp['status'],
                                                            'transactionId_1' => $paypal_resp['paymentInfoList.paymentInfo(0).transactionId'],
                                                            'transactionStatus_1' => $paypal_resp['paymentInfoList.paymentInfo(0).transactionStatus'],
                                                            'receiverAmount_1' => $paypal_resp['paymentInfoList.paymentInfo(0).receiver.amount'],                                                                     
                                                            'receiverEmail_1' => $paypal_resp['paymentInfoList.paymentInfo(0).receiver.email'],
                                                            'senderTransactionId_1' => $paypal_resp['paymentInfoList.paymentInfo(0).senderTransactionId'],
                                                            'transactionId_2' => $paypal_resp['paymentInfoList.paymentInfo(1).transactionId'],
                                                            'transactionStatus_2' => $paypal_resp['paymentInfoList.paymentInfo(1).transactionStatus'],
                                                            'receiverAmount_2' => $paypal_resp['paymentInfoList.paymentInfo(1).receiver.amount'],     
                                                            'receiverEmail_2' => $paypal_resp['paymentInfoList.paymentInfo(1).receiver.email'],
                                                            'senderTransactionId_2' => $paypal_resp['paymentInfoList.paymentInfo(1).senderTransactionId'],
                                                            'tot_amount' => $tot_amount,
                                                            'transaction_date' => current_time('mysql')
                                                        )
                                   );
    unset($_SESSION['paypal']);
}
//echo '<pre>';print_r($paypal_resp);
//echo '<pre>';print_r($_SESSION);
//.die;
//wp_redirect( home_url().'/upload-confirmation/' );
?>
