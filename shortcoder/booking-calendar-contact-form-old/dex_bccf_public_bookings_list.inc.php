<?php

global $wpdb;

$message = "";

$current_user = wp_get_current_user();

if(isset($_POST['hid_id']) && $_POST['action'] == 'del'){
    $row = $wpdb->get_row("SELECT user_id ,post_id, item_number, datatime_s, datatime_e FROM ". DEX_BCCF_CALENDARS_TABLE_NAME ." WHERE id= ". $_POST['hid_id']);
    if ((time() < strtotime($row->datatime_e))){
        if ((time() - strtotime($row->datatime_s)) < 0 ){
            $response = refund_payment($row->post_id, $row->item_number, $row->datatime_s, $row->datatime_e, 'total');
        }else{
            $response = refund_payment($row->post_id, $row->item_number, $row->datatime_s,$row->datatime_e, 'partial');
        }
    }else{
        $response['insert_id'] = 1;
    }
    
    //$wpdb->query("DELETE FROM ". DEX_BCCF_TABLE_NAME ." WHERE id= ". $row->item_number);
    //$wpdb->query("DELETE FROM ". DEX_BCCF_CALENDARS_TABLE_NAME ." WHERE id= ". $_POST['hid_id']);
    //$wpdb->query("DELETE FROM ". $wpdb->prefix ."posts WHERE id= ". $row->post_id);
    //$wpdb->query("DELETE FROM ". $wpdb->prefix ."posts WHERE post_parent= ". $row->post_id);
    
    if($response['insert_id'] > 0){
        $wpdb->query("UPDATE ". $wpdb->prefix ."posts SET post_status = 'draft' WHERE ID = ". $row->post_id);
        $wpdb->query("UPDATE ". DEX_BCCF_CALENDARS_TABLE_NAME ." SET booking_status = 'f' WHERE id= ". $_POST['hid_id']);
        add_post_meta($row->post_id, 'delete_reason', $_POST['delete_reason']);
        
        if($_POST['booking_msg'] != '')
        {
            add_post_meta($row->post_id, 'delete_msg', $_POST['booking_msg']);
            $row = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."users WHERE id= ". $_POST['user_id']);
            
            $headers[] = 'Content-type: text/html; charset=utf-8';
            $headers[] = 'From: Admin <'.get_option( 'admin_email' ).'>';
            
            //wp_mail($row->user_email, 'Notification', $_POST['booking_msg'],"Content-Type: text/plain; charset=utf-8\n"."X-Mailer: PHP/" . phpversion());
            
            wp_mail($row->user_email, 'Notification', $_POST['booking_msg'], $headers);
        }
        echo "This booking has been deleted and your message has been sent";
    }else{
        echo $response['status'];
    }
}
function refund_payment($post_id, $item_id, $datetime_s, $datetime_e, $refund_type){
    global $wpdb;
    require_once ("paypal/paypalplatform.php");
    $row = $wpdb->get_row("SELECT * FROM ". DEX_BCCF_PAYPAL ." WHERE itemId = ". $item_id);
    if(isset($row) && !empty($row)){
        if($refund_type == 'total'){
            $resp = CallRefund($row->payKey, $row->transactionId_2, $row->trackingId, array($row->receiverEmail_2),array($row->receiverAmount_2));
        }else{
            $current_date = date('Y-m-d');
            $calender_id = $wpdb->get_var("SELECT reservation_calendar_id FROM ". DEX_BCCF_CALENDARS_TABLE_NAME ." WHERE id = ". $row->calender_data_id);
            $per_day_amount = $wpdb->get_var("SELECT request_cost FROM ". DEX_BCCF_CONFIG_TABLE_NAME ." WHERE id = ". $calender_id);
            $booking_days = cp_bccf_get_dateDifference($current_date, $datetime_e);
            $refunded_amount[] = ($per_day_amount * $booking_days ) / 1.25;
            $resp = CallRefund($row->payKey, $row->transactionId_2, $row->trackingId, array($row->receiverEmail_2),$refunded_amount);
        }
        $ack = strtoupper($resp["responseEnvelope.ack"]);
        if($ack=="SUCCESS"){
            $refund_insert = $wpdb->insert( DEX_BCCF_PAYPAL_REFUND, array( 'paypal_trans_id' => $row->id,
                                                        'correlationId' => $resp['responseEnvelope.correlationId'],
                                                        'build' => $resp['responseEnvelope.build'],
                                                        'status' => $resp['refundInfoList.refundInfo(0).refundStatus'],
                                                        'received_amount' => $resp['refundInfoList.refundInfo(0).receiver.amount'],
                                                        'received_email' => $resp['refundInfoList.refundInfo(0).receiver.email'],
                                                        'transactionId' => $resp['refundInfoList.refundInfo(0).encryptedRefundTransactionId'],
                                                        'refunded_netamount' => $resp['refundInfoList.refundInfo(0).refundNetAmount'],
                                                        'refund_date' => current_time('mysql')
                                                    ));
            $response['status'] = $resp['refundInfoList.refundInfo(0).refundStatus'];
            $response['insert_id'] = $wpdb->insert_id;
        }else{
            $response['insert_id'] = 0;
            if(isset($resp['refundInfoList.refundInfo(0).refundStatus'])){
                $response['status'] = $resp['refundInfoList.refundInfo(0).refundStatus'];
            }else{
                $response['status'] = $resp['error(0).message'];
            }
        }
        return $response;
    }else{
        $response['insert_id'] = 1;
        return $response;
    }
}
if(isset($_POST['send']) && $_POST['action'] == 'msg'){

    if($_POST['booking_msg'] != '')
    {
        $row = $wpdb->get_row("SELECT user_id ,post_id, item_number FROM ". DEX_BCCF_CALENDARS_TABLE_NAME ." WHERE id= ". $_POST['hid_id']);
        
        add_post_meta($row->post_id, 'delete_msg', $_POST['booking_msg']);
        $row = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."users WHERE id= ". $_POST['user_id']);
        
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = 'From: Admin <'.get_option( 'admin_email' ).'>';
        
        wp_mail($row->user_email, 'Notification', $_POST['booking_msg'],$headers);
        echo "Your message has been sent";
    }
    else
    {
        echo "Please enter some text";
    }
    
}


if (0 != $current_user->ID ) {

$current_page = intval($_GET["p"]);
if (!$current_page) $current_page = 1;
$records_per_page = 50;                                                                                  

$cond = " WHERE conwer=".$current_user->ID." AND booking_status='t'  ";
if ($_GET["search"] != '') $cond .= " AND (".DEX_BCCF_CALENDARS_TABLE_NAME." .title like '%".$wpdb->escape($_GET["search"])."%' OR ".DEX_BCCF_CALENDARS_TABLE_NAME.".description LIKE '%".$wpdb->escape($_GET["search"])."%')";
if ($_GET["dfrom"] != '') $cond .= " AND (datatime_s >= '".$wpdb->escape($_GET["dfrom"])."')";
if ($_GET["dto"] != '') $cond .= " AND (datatime_s <= '".$wpdb->escape($_GET["dto"])." 23:59:59')";


//$current_user->user_login 

//$events = $wpdb->get_results( "SELECT * FROM ".DEX_BCCF_CALENDARS_TABLE_NAME." INNER JOIN ".DEX_BCCF_CONFIG_TABLE_NAME." ON ".DEX_BCCF_CALENDARS_TABLE_NAME.".reservation_calendar_id=".DEX_BCCF_CONFIG_TABLE_NAME.".id ".$cond." ORDER BY datatime_s DESC" );
    //commented by SJ
    //$events = $wpdb->get_results( "SELECT * , ".DEX_BCCF_CALENDARS_TABLE_NAME.".id AS uid FROM ".DEX_BCCF_CALENDARS_TABLE_NAME." INNER JOIN ".DEX_BCCF_CONFIG_TABLE_NAME." ON ".DEX_BCCF_CALENDARS_TABLE_NAME.".reservation_calendar_id=".DEX_BCCF_CONFIG_TABLE_NAME.".id ".$cond." ORDER BY datatime_s DESC" );
    $events = $wpdb->get_results( "SELECT * , ".DEX_BCCF_CALENDARS_TABLE_NAME.".id AS uid FROM ".DEX_BCCF_CALENDARS_TABLE_NAME." INNER JOIN ".DEX_BCCF_CONFIG_TABLE_NAME." ON ".DEX_BCCF_CALENDARS_TABLE_NAME.".reservation_calendar_id=".DEX_BCCF_CONFIG_TABLE_NAME.".id INNER JOIN ".$wpdb->prefix."posts as p on p.ID = ".DEX_BCCF_CALENDARS_TABLE_NAME.".post_id ".$cond." ORDER BY datatime_s DESC" );
$total_pages = ceil(count($events) / $records_per_page);

$option_calendar_enabled = true;
 
?>

<!-- <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet" />
<form action="" method="get">
 <input type="hidden" name="page_id" value="156" />
 <nobr>Search for: <input type="text" size="10" name="search" value="<?php echo esc_attr($_GET["search"]); ?>" /> &nbsp; &nbsp; &nbsp; </nobr> 
 <nobr>From: <input type="text" size="10" id="dfrom" name="dfrom" value="<?php echo esc_attr($_GET["dfrom"]); ?>" /> &nbsp; &nbsp; &nbsp; </nobr>
 <nobr>To: <input type="text"  size="10" id="dto" name="dto" value="<?php echo esc_attr($_GET["dto"]); ?>" /> &nbsp; &nbsp; &nbsp; </nobr>
 <span class="submit"><input type="submit" name="ds" value="Filter" /></span>
</form>

<br /> -->
                             
<?php


echo paginate_links(  array(
    'base'         => '?dfrom='.urlencode($_GET["dfrom"]).'&dto='.urlencode($_GET["dto"]).'&search='.urlencode($_GET["search"]),
    'format'       => '&p=%#%',
    'total'        => $total_pages,
    'current'      => $current_page,
    'show_all'     => False,
    'end_size'     => 1,
    'mid_size'     => 2,
    'prev_next'    => True,
    'prev_text'    => __('&laquo; Previous'),
    'next_text'    => __('Next &raquo;'),
    'type'         => 'plain',
    'add_args'     => False
    ) );

?>

    <script type="text/javascript">

        function cp_deleteMessageItem(id,uid)
        {
            var wrapper = jQuery('<div id="sendmsgwrapper"></div>');
            var closebtnwrapper =  jQuery('<div class="btnwrapper"></div>');
            var close = jQuery('<div id="closefrm"></div>');
            var elemHTML = '<div class="msgtablewrapper">';
            elemHTML += '<form method="post" action="">';
                elemHTML += '<table>';
                	elemHTML += '<tr><label style="font-size: 14px; color: ##CFCFCF;">Choose a reason, send a message to the author and delete the post.</label></tr><br/><br/>';
                    elemHTML += '<tr><input type="radio" name="delete_reason" checked="checked" value="Offensive">Offensive&nbsp</input></tr>';
                    elemHTML += '<tr><input type="radio" name="delete_reason" checked="checked" value="Unsuitable">Unsuitable</input></tr><br/><br/>';
                    elemHTML += '<tr><textarea style="width: 372px; height: 104px;" name="booking_msg"></textarea></tr>';
                    elemHTML += '<tr><input type="submit" class="button2" name="send" value="Send and delete"><input type="hidden" name="user_id" value="'+uid+'"><input type="hidden" name="hid_id" value="'+id+'"><input type="hidden" name="action" value="del"></tr>';
                        
                elemHTML += '</table>';
            elemHTML += '</form></div>';
            var elem= jQuery(elemHTML);



            var overlay = jQuery('<div>');
            overlay.attr('id','dialog-overlay');
            jQuery('body').append(overlay);
            arrPageSizes = _getPageSize();
            overlay.css({
                opacity:			0.5,
                width:				arrPageSizes[0],
                height:				arrPageSizes[1]
            });
            overlay.show();
            closebtnwrapper.append(close);
            wrapper.append(closebtnwrapper);
            wrapper.append(elem);

            wrapper.appendTo(jQuery('body'));

            wrapper.css("position","absolute");
            wrapper.css("top", ( jQuery(window).height() - wrapper.height() ) / 2+ jQuery(window).scrollTop() + "px");
            wrapper.css("left", ( jQuery(window).width() - wrapper.width() ) / 2+ jQuery(window).scrollLeft() + "px");

            close.click(function(){
                overlay.remove();
                wrapper.remove();

            });
        }

        function cp_sendMessageItem(id, uid)
        {
            var wrapper = jQuery('<div id="sendmsgwrapper"></div>');
            var closebtnwrapper =  jQuery('<div class="btnwrapper"></div>');
            var close = jQuery('<div id="closefrm"></div>');
            var elem= jQuery('<div class="msgtablewrapper"><form method="post" action=""><table><tr><label style="font-size: 14px; color: ##CFCFCF;">Send a message to the author.</label></tr><br/><br/><tr><textarea style="width: 372px; height: 104px;" name="booking_msg"></textarea></tr><tr style="text-align: center;"><input type="submit" class="button3" name="send" value="Send"><input type="hidden" name="user_id" value="'+uid+'"><input type="hidden" name="hid_id" value="'+id+'"><input type="hidden" name="action" value="msg"></tr></table></form></div>');


            var overlay = jQuery('<div>');
            overlay.attr('id','dialog-overlay');
            jQuery('body').append(overlay);
            arrPageSizes = _getPageSize();
            overlay.css({
                opacity:			0.5,
                width:				arrPageSizes[0],
                height:				arrPageSizes[1]
            });
            overlay.show();
            closebtnwrapper.append(close);
            wrapper.append(closebtnwrapper);
            wrapper.append(elem);
            wrapper.appendTo(jQuery('body'));
            wrapper.css("position","absolute");
            wrapper.css("top", ( jQuery(window).height() - wrapper.height() ) / 2+ jQuery(window).scrollTop() + "px");
            wrapper.css("left", ( jQuery(window).width() - wrapper.width() ) / 2+ jQuery(window).scrollLeft() + "px");

            close.click(function(){
                overlay.remove();
                wrapper.remove();

            });

        }

        function _getPageSize() {
            var xScroll, yScroll;
            if (window.innerHeight && window.scrollMaxY) {
                xScroll = window.innerWidth + window.scrollMaxX;
                yScroll = window.innerHeight + window.scrollMaxY;
            } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
                xScroll = document.body.scrollWidth;
                yScroll = document.body.scrollHeight;
            } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
                xScroll = document.body.offsetWidth;
                yScroll = document.body.offsetHeight;
            }
            var windowWidth, windowHeight;
            if (self.innerHeight) {	// all except Explorer
                if(document.documentElement.clientWidth){
                    windowWidth = document.documentElement.clientWidth;
                } else {
                    windowWidth = self.innerWidth;
                }
                windowHeight = self.innerHeight;
            } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
                windowWidth = document.documentElement.clientWidth;
                windowHeight = document.documentElement.clientHeight;
            } else if (document.body) { // other Explorers
                windowWidth = document.body.clientWidth;
                windowHeight = document.body.clientHeight;
            }
            // for small pages with total height less then height of the viewport
            if(yScroll < windowHeight){
                pageHeight = windowHeight;
            } else {
                pageHeight = yScroll;
            }
            // for small pages with total width less then width of the viewport
            if(xScroll < windowWidth){
                pageWidth = xScroll;
            } else {
                pageWidth = windowWidth;
            }
            arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
            return arrayPageSize;
        }

    </script>

<div id="dex_printable_contents">
<table class="wp-list-table widefat fixed pages" cellspacing="0">
	<thead>
	<tr>
        <th style="font-weight:bold;">Visual post</th>
        <th style="font-weight:bold;">Booked time</th>
        <th style="font-weight:bold;">Title</th>
        <th style="font-weight:bold;">Amount</th>
        <th style="font-weight:bold;">Author</th>
        <th style="font-weight:bold;">Board</th>
        <th style="font-weight:bold;">Manage</th>
	</tr>
	</thead>
	<tbody id="the-list">
            <?php //echo '<pre>';print_r($events);die; ?>
	 <?php for ($i=($current_page-1)*$records_per_page; $i<$current_page*$records_per_page; $i++) if (isset($events[$i])) {
         $row =$wpdb->get_row("SELECT * FROM wp_posts WHERE id=". $events[$i]->post_id);
         
         
        $calendar_post_id = get_post_meta( $events[$i]->post_id, 'bccf_calendar_post_id', true );
	$calendar_post = get_post( $calendar_post_id, ARRAY_A  ); 
         
         $user_info = get_userdata($row->post_author);
         preg_match( '/src="([^"]*)"/i', $row->post_content, $result ) ;
         $thumbnail=null;
         if($result){
             $extension = pathinfo($result[1],PATHINFO_EXTENSION);
             $filename = pathinfo($result[1],PATHINFO_FILENAME);
             $path = pathinfo($result[1],PATHINFO_DIRNAME);
             $thumbnail = $path.'/'.$filename.'_thumb.'.$extension;
         }
		 
		if (has_post_thumbnail( $events[$i]->post_id ) ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $events[$i]->post_id ) );
			$thumbnail = $image[0];
		}
		 
         $days= cp_bccf_get_dateDifference($events[$i]->datatime_s,$events[$i]->datatime_e);

         $cost= (float) $events[$i]->request_cost* $days;
         $price = ( 0.8 * (float) $cost);
         //$totalamount = $totalamount+$price;
         $price = $events[$i]->currency.' '. $price;
         $row_pay =$wpdb->get_row("SELECT tot_amount FROM wp_bccf_reservation_paypal_transaction WHERE post_id=". $events[$i]->post_id, ARRAY_A);
         
         if(isset($row_pay) && $row_pay['tot_amount'] != ''){
            $showprice = $events[$i]->currency.' '.($row_pay['tot_amount'] / 1.25);
            $price = ($row_pay['tot_amount'] / 1.25);
         }else{
            $showprice = $events[$i]->currency.' 0';
            $price = 0;
         }
         $totalamount = $totalamount+$price;
         ?>
         <tr class='<?php if (!($i%2)) { ?>alternate <?php } ?>author-self status-draft format-default iedit' valign="top">
             <td> <?php if(!is_null($thumbnail)) {?>
                    <a target="_blank" href="<?php echo $thumbnail;?>"><img width="50" height="50" src="<?php echo $thumbnail;?>" ></a>
                 <?php }?>
             </td>
             <td style="vertical-align: middle;">From <?php echo date("d-m-Y", strtotime($events[$i]->datatime_s) );?> <br/> to <?php echo date("d-m-Y", strtotime($events[$i]->datatime_e) );?> </td>
             <td style="vertical-align: middle;"><?php echo $events[$i]->post_title; ?></td>
             <td style="vertical-align: middle;"><?php echo $showprice; ?></td>
             <td style="vertical-align: middle;"><?php echo $user_info->user_login; ?></td>
             <td style="vertical-align: middle;">
                <a class="button4" href="<?php echo get_permalink($calendar_post['ID']); ?>"><?php echo $calendar_post['post_title']; ?></a>
             </td>
             <td style="vertical-align: middle;">
                 <input type="button" class="button2" name="caldelete_<?php echo $events[$i]->id; ?>" value="Delete" onclick="cp_deleteMessageItem(<?php echo $events[$i]->uid; ?>,<?php echo $events[$i]->user_id; ?>);" /><input type="button" class="button3" name="sendmsg_<?php echo $events[$i]->id; ?>" value="Send Message" onclick="cp_sendMessageItem(<?php echo $events[$i]->uid; ?>,<?php echo $events[$i]->user_id; ?>);" />
             </td>
         </tr>
     <?php } ?>
	</tbody>
</table>
    <div style="font-size: 12px;"><?php echo _e('Total amount');?>:</div><div style="font-size: 20px;font-weight:bold;margin-top:10px;color:#777777;"><?php echo $events[0]->currency.' '. $totalamount;?></div>
</div>

<!-- <p class="submit"><input type="button" name="pbutton" value="Print" onclick="do_dexapp_print();" /></p> -->




<script type="text/javascript">
 function do_dexapp_print()
 {
      w=window.open();
      w.document.write("<style>table{border:2px solid black;width:100%;}th{border-bottom:2px solid black;text-align:left}td{padding-left:10px;border-bottom:1px solid black;}</style>"+document.getElementById('dex_printable_contents').innerHTML);
      w.print();
      w.close();    
 }
 
 var $j = jQuery.noConflict();
 $j(function() {
 	$j("#dfrom").datepicker({     	                
                    dateFormat: 'yy-mm-dd'
                 });
 	$j("#dto").datepicker({     	                
                    dateFormat: 'yy-mm-dd'
                 });
 });
 
</script>



<!-- 
<?php } else { ?>
  <br />
  Please <a href="/wp-login.php" class="simplemodal-login">Log in</a> as a board owner to get access to your bookings list.
<?php } ?> -->










