<?php
global $wpdb;
$user_ID = get_current_user_id();
$params = array(
    'post_type'			=> 'ait-dir-item',
    'nopaging'			=>	true,
    'post_status'		=> 'publish',
    'author'                    => $user_ID
);
$itemsQuery = new WP_Query();
$items = $itemsQuery->query($params);
if (0 != $user_ID ) {
?>
<?php if(isset($items) && !empty($items)) : ?>
    <div id="dex_printable_contents">
        <table class="wp-list-table widefat fixed pages" cellspacing="0">
            <thead>
                <tr>
                    <th style="padding-left:7px;font-weight:bold;">Title</th>
                    <th style="padding-left:7px;font-weight:bold;">Owner</th>
                    <th style="padding-left:7px;font-weight:bold;">Image</th>
                    <th style="padding-left:7px;font-weight:bold;">Address</th>
                    <th style="padding-left:7px;font-weight:bold;">Category</th>
                    <th style="padding-left:7px;font-weight:bold;">Location</th>
                </tr>
            </thead>
            <tbody id="the-list">
                <?php foreach ($items as $key => $item) : ?>
                    <?php
                    $user_info = get_userdata($item->post_author);
                    if (has_post_thumbnail( $item->ID ) ) {
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ) );
                        $thumbnail = $image[0];
                    }
                    $term_cat = wp_get_post_terms($item->ID, 'ait-dir-item-category');
                    $term_loc = wp_get_post_terms($item->ID, 'ait-dir-item-location');
                    $meta_values = get_post_meta( $item->ID, '_ait-dir-item' );
                    ?>
                    <tr class='<?php if (!($key%2)) { ?>alternate <?php } ?>author-self status-draft format-default iedit' valign="top">
                        <td style="vertical-align: middle;">
                            <a type="button" href="<?php echo get_permalink($item->ID);?>" class="button4" ><?php echo $item->post_title; ?></a>
                        </td>
                        <td style="vertical-align: middle;"><?php echo $user_info->user_login; ?></td>
                        <td>
                            <?php if(!is_null($thumbnail)) {?>
                                <a target="_self" href="<?php echo get_permalink($item->ID);?>">
                                    <img width="150" height="150" src="<?php echo $thumbnail;?>" >
                                </a>
                            <?php }?>
                        </td>
                        <td style="vertical-align: middle;">
                            <?php if(isset($meta_values[0])) {?>
                                <?php echo $meta_values[0]['address']; ?>
                            <?php }?>
                        </td>
                        <td style="vertical-align: middle;">
                            <?php if(isset($term_cat[0])) {?>
                                <?php echo $term_cat[0]->name; ?>
                            <?php }?>
                        </td>
                        <td style="vertical-align: middle;">
                            <?php if(isset($term_loc[0])) {?>
                                <?php echo $term_loc[0]->name; ?>
                            <?php }?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <div>
<?php else : ?>
Sorry, You have no calendar list yet !!
<?php endif; ?>
<?php }else{ ?>
Please log in to get access to the calendar list.
<?php } ?>