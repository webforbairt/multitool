<?php
function showForm1($atts) {
?>

<div class="container1 ">
<div class="stern-taxi-fare">
<?php
$form_dataset = get_option( 'frontend_dataComp' );` 
foreach ( $form_dataset as $data_car ) {
$car_make[] = $data_car[0]; 
$car_model[] = $data_car[1]; 
$car_year[] = $data_car[2]; 
$car_code[] = $data_car[3]; 
$car_colorname[]= $data_car[4]; 			
$car_r[]= $data_car[5]; 			
$car_g[]= $data_car[6]; 			
$car_b[]= $data_car[7]; 			
$car_tricoat[]= $data_car[8]; 			
} 
$vehicle_make = array_unique($car_make);
$vehicle_model = array_unique($car_model);
$vehicle_year = array_unique($car_year);
$vehicle_code = array_unique($car_code);
$vehicle_colname = array_unique($car_colorname);
$vehicle_r = array_unique($car_r);
$vehicle_g = array_unique($car_g);
$vehicle_b = array_unique($car_b);
$vehicle_tricoat = array_unique($car_tricoat);
?>
	<div id="steps_buttons" class="row" style="position:relative;">
		<div id="btn_step1" class="col col-lg-6">
			<button type="button" class="step_1btn" id="btn_step1">
			<span id="step_count1">1</span><span>Step 1</span></button>
		</div>
		<div id="btn_step2" class="col col-lg-6">
			<button type="button" class="step_1btn" id="btn_step2">
			<span id="step_count1">2</span><span>Step 2</span</button>
		</div>
	</div>

<form  id="ins_mainform" method="post">

<div id="step1">

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Brand</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage yearselect" id="mul_brand" name="vehicleyear">
<option class="options" value="" selected>-- Select Brand --</option>
<?php
foreach ( $vehicle_make as $v_year ) { ?>
<option class="options" value="<?php echo $v_year;?>"><?php echo $v_year;?></option>
<?php } ?>
</select>
</div>
</div>
</div>

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Model Name</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage makeoption" id="mul_modelname" name="vehiclemake">
<option class="options" value="" selected>-- Select Model --</option>
<?php
foreach ( $vehicle_model as $model_show ) { ?>
<option class="options" value="<?php echo $model_show;?>"><?php echo $model_show;?></option>
<?php } ?>
</select>
</div>
</div>
</div>

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Closed Length Ins</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage modeloption" id="closed_lengthins" name="vehiclemodel">
<option class="options" value="" selected>-- Select Length --</option>
<?php
foreach ( $vehicle_year as $length_show ) { ?>
<option class="options" value="<?php echo $length_show;?>"><?php echo $length_show;?></option>
<?php } ?>
</select>
</div>
</div>

</div>

		<table id="show_formdata_comp">
			<thead>
			<tr class="fd-head">
					<td>Brand</td>	
					<td>Model Name</td>	
					<td>Closed Length ins</td>							
					<td>Closed Length cms</td>							
					<td>Weight oz</td>							
					<td>Weight gms</td>							
					<td>Price Range</td>							
					<td>Outside Accessible</td>							
					<td>Pliers</td>							
					<td>Knife</td>							
					<td>Scissors</td>							
					<td>Secondary Tools</td>							
					<td>Functions</td>							
					<td>Main Colour</td>						
			</tr>
			</thead>
			<tbody class="maintablebody">
			</tbody>				
		</table>

	<table id="datatable" class="finalTable" >	
	</table>


</form>

<!-- Show data -->
<div id="show_data" class="row">
<div id="showdata_parent" class="col col-lg-12">
<div type="button" class="step_results" id="show_results"><span>Show Results</span></div>
</div>
</div>


<!-- Next Step Button -->
<div id="btn_next_step1" class="row">
<div id="btn_nextstep1" class="col col-lg-12">
<div type="button" class="step_next1btn" id="btn_nextstep1inner"><span>Next</span></div>
</div>
</div>
</div>  <!-- div step1 close -->


		<div id="step2" style="display:none;">
		<div id="looader" class="" style="display: none;background: rgba(0, 0, 0, 0.8);height: 100%;position: absolute; z-index: 9999;width: 100%;">
		<img src="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif" style="
		    position: absolute;width: 10%;left: 45%; top: 10%; right: 45%;
		">
		</div>

			<textarea id="color_data" style="display:none;"></textarea>
			<div id="btn_next_step1" class="row">
			<div id="btn_nextstep1" class="col col-lg-12">
			<div type="button" class="step_next1btn" id="btn_nextstep1inner2"><span>Next</span></div>
			</div>
			</div>
		</div>

 <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>


		<!-- get data csv -->
		<?php  
		$form_dataComp = get_option( 'frontend_dataComp' );
		$json_form_dataComp = json_encode($form_dataComp); 
		?>

<script>
jQuery(document).ready(function() {	
		var form1 = jQuery( "#ins_mainform" );
			var result = [];
			var data_tt = [];
		var tableControl = document.getElementById('show_formdata_comp');
jQuery('#btn_step1').click(false);

jQuery("#btn_step1").addClass("active_step");
jQuery("#btn_step1").click(false);
// jQuery("#btn_step1").click(function(){
// jQuery("#btn_step2").removeClass("active_step");
// jQuery("#btn_step1").addClass("active_step");
// });		
jQuery("#btn_step2").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").addClass("active_step");
});		
jQuery("#btn_nextstep2inner").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").removeClass("active_step");
});


var basePrice = 3.00;
var width;

//jQuery( "#ins_submodel" ).hide();
jQuery( "#step2" ).hide();
jQuery( "#step3" ).hide();
jQuery( "#step4" ).hide();
jQuery("#calCheckout_url").hide();

jQuery( "#btn_step2" ).click(function() {
jQuery( "#step2" ).show();
});	
jQuery("#show_results").click(function () {
setTable();
});

function setTable(){
	var mul_brand = jQuery('#mul_brand option:selected').val();
	var mul_modelname = jQuery('#mul_modelname option:selected').val();
	var closed_lengthins = jQuery('#closed_lengthins option:selected').val();
 
	// has a value selected array
	var selected_valarrayt = [mul_brand, mul_modelname, closed_lengthins];
	var step_1data = <?php echo $json_form_dataComp; ?>;
	
	//console.log(step_1data);
	var filteredTable;
	var valuetable = [];

	jQuery.each( step_1data, function( i, value ) {
	var value0 = value[0];
	var value1 = value[1];
	var value2 = value[2];	
	var value3 = value[3];	
	var value4 = value[4];	
	var value5 = value[5];	
	var value6 = value[6];	
	
	console.log("selected_valarrayt0..."+selected_valarrayt[0]);
	console.log("selected_valarrayt1..."+selected_valarrayt[1]);
	console.log("selected_valarrayt2..."+selected_valarrayt[2]);
	
	//Getting data For Final Table	
	if(value0 == selected_valarrayt[0]){
		if(value1 == selected_valarrayt[1]){
			if(value2 == selected_valarrayt[2]){
				valuetable.push(value0);
				valuetable.push(value1);				
				valuetable.push(value2);				
				valuetable.push(value3);				
				valuetable.push(value4);								
				valuetable.push(value5);
				valuetable.push(value6);
			}		  
		}		  
	}		  
	});
	//Set to Table
	console.log(valuetable.length);
	valuetable_len = valuetable.length;
	console.log(valuetable_len);
	if(valuetable_len > 0){
	jQuery("#btn_step1").removeClass("active_step");
	jQuery("#btn_step2").addClass("active_step");
	jQuery('.maintablebody').html('');		
	//jQuery("#step1").hide();
	jQuery("#step2").show();
	jQuery.each( valuetable, function( i, valuetableset1 ) {
		arraylength3 = valuetable.length - 1;

		if(i <= arraylength3){
			jQuery('.maintablebody').append('<td class="tabledata" value="'+valuetableset1+'">"'+valuetableset1+'"</td>');	    
		}
	});

 	var td = jQuery("#show_formdata_comp td");
	td.each(function(i){
		if (i % 7 == 0) {
			td.slice(i, i+7).wrapAll('<tr/>');
		}
	}).parent('tr').unwrap();

	jQuery("#show_formdata_comp tr").append("<td id='select_color'><input type='radio' class='tableradio' id='tableradio' name='tableradio' required><label for='tableradio'>tableradio</label></td>");	 
	}else{
		alert('No color found for this match, Please try again !!!');
	}
	
	
}
});
</script>			

}
