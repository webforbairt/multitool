<?php

Class checkoutAfter{
	function __construct($order_id, $isTextFormat=true, $isInAdmin=false){

		if(!isProductTaxiIsInCart($order_id)) {
			return;
		}

			//Get data from PHP Sessions
			session_start();  	
			$ins_price_sel = $_SESSION["ins_price"];
			$ins_compname_sel = $_SESSION["ins_compname"];
			$ins_gcc_spec_sel = $_SESSION["ins_gcc_spec"];
			$ins_vehicle_cat_sel = $_SESSION["ins_vehicle_cat"];
			$ins_repair_cond_sel = $_SESSION["ins_repair_cond"];
			$ins_minimum_val_sel = $_SESSION["ins_minimum_val"];
			$ins_maximum_val_sel = $_SESSION["ins_maximum_val"];
			$ins_rate_sel = $_SESSION["ins_rate"];
			$ins_minimum_sel = $_SESSION["ins_minimum"];
			$ins_dl_less_than_year_sel = $_SESSION["ins_dl_less_than_year"];
			$ins_less_than_year_sel = $_SESSION["ins_less_than_year"];
			$ins_os_coverage_sel = $_SESSION["ins_os_coverage"];
			$ins_pa_driver_sel = $_SESSION["ins_pa_driver"];
			$ins_papp_sel = $_SESSION["ins_papp"];
			$ins_rs_assistance_sel = $_SESSION["ins_rs_assistance"];
			$ins_rent_car_sel = $_SESSION["ins_rent_car"];
			$ins_excess_amount_sel = $_SESSION["ins_excess_amount"];
			
			//User Data
			$ins_reg_date_aj = $_SESSION["ins_reg_date_aj"];
			$ins_polstart_date_aj = $_SESSION["ins_polstart_date_aj"];
			$ins_email_address_aj = $_SESSION["ins_email_address_aj"];
			$ins_firstName_aj = $_SESSION["ins_firstName_aj"];
			$ins_lastname_aj = $_SESSION["ins_lastname_aj"];
			$ins_dob_aj = $_SESSION["ins_dob_aj"];
			$ins_nationality_aj = $_SESSION["ins_nationality_aj"];
			$ins_contact_num_aj = $_SESSION["ins_contact_num_aj"];
			$ins_country_firstdl_aj = $_SESSION["ins_country_firstdl_aj"];
			$ins_long_uaedrivinglicence_aj = $_SESSION["ins_long_uaedrivinglicence_aj"];
			$ins_years_dltotal_aj = $_SESSION["ins_years_dltotal_aj"];
			$ins_imported_aj = $_SESSION["ins_imported_aj"];
			
		
		if($isTextFormat) {

/* 			$distanceHtml = get_post_meta( $order_id , '_distance', true );
			$durationHtml = get_post_meta( $order_id , '_durationHtml', true );
			$estimated_fare = get_post_meta($order_id , '_estimated_fare', true );
			$cartypes = get_post_meta( $order_id , '_cartypes', true );
			$source = get_post_meta( $order_id , '_source', true );
			$destination = get_post_meta( $order_id , '_destination', true );
			$car_seats = get_post_meta( $order_id , '_car_seats', true );
			$dateTimePickUp = date(getFormatDateTime("php"),strtotime(get_post_meta( $order_id , '_local_pickup_time_select', true )));
			$dateTimePickUpRoundTrip = date(getFormatDateTime("php"),strtotime(get_post_meta( $order_id , '_dateTimePickUpRoundTrip', true )));
			$nbToll = get_post_meta( $order_id , '_nbToll', true );
			$numberOfPets = get_post_meta( $order_id , '_NumberOfPets', true );
			//$dateTimePickUpRoundTrip = get_post_meta( $order_id , '_dateTimePickUpRoundTrip', true );
			$stern_taxi_fare_round_trip_text = (get_post_meta( $order_id , '_stern_taxi_fare_round_trip', true )=="true") ? __('Round Trip', 'stern_taxi_fare') : __('One way', 'stern_taxi_fare') ;
			$suitcases = get_post_meta( $order_id , '_suitcases', true );
			$idCalendar = get_post_meta( $order_id , '_idCalendar', true );
			$idCalendarRoundTrip = get_post_meta( $order_id , '_idCalendarRoundTrip', true ); */			

			?>
			<h4><?php _e('Selected Company Detail', 'stern_taxi_fare' ); ?></h4>


			<?php if($ins_compname_sel !="") : ?>
				<p>
					<strong><?php _e('Company Name: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_compname_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_price_sel !="") : ?>
				<p>
					<strong><?php _e('Price: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_price_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_gcc_spec_sel !="") : ?>
				<p>
					<strong><?php _e('Ins Gcc Spec Sel: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_gcc_spec_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_vehicle_cat_sel !="") : ?>
				<p>
					<strong><?php _e('Vehicle Category: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_vehicle_cat_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_repair_cond_sel !="") : ?>
				<p>
					<strong><?php _e('Repair Condition: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_repair_cond_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_dl_less_than_year_sel !="") : ?>
				<p>
					<strong><?php _e('Driving Licence Less then One year: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_dl_less_than_year_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_less_than_year_sel !="") : ?>
				<p>
					<strong><?php _e('Age Less Then 25 Years: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_less_than_year_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_os_coverage_sel !="") : ?>
				<p>
					<strong><?php _e('Outside UAE Coverage: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_os_coverage_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_pa_driver_sel !="") : ?>
				<p>
					<strong><?php _e('Personal Accident (Driver): ', 'stern_taxi_fare'); ?></strong><?php echo $ins_pa_driver_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_papp_sel !="") : ?>
				<p>
					<strong><?php _e('Personal Accident Per Passengers: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_papp_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_rs_assistance_sel !="") : ?>
				<p>
					<strong><?php _e('Road Side Assistance: ', 'stern_taxi_fare'); ?></strong><?php echo $ins_rs_assistance_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_rent_car_sel !="") : ?>
				<p>
					<strong><?php _e('Rant a Car:', 'stern_taxi_fare'); ?></strong><?php echo $ins_rent_car_sel; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_excess_amount_sel !="") : ?>
				<p>
					<strong><?php _e('Excess Amount:', 'stern_taxi_fare'); ?></strong><?php echo $ins_excess_amount_sel; ?>
				</p>
			<?php endif; ?>
			
			<h4><?php _e('User Data', 'stern_taxi_fare' ); ?></h4>
			
			<?php if($ins_reg_date_aj !="") : ?>
				<p>
					<strong><?php _e('Date of Registration:', 'stern_taxi_fare'); ?></strong><?php echo $ins_reg_date_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_polstart_date_aj !="") : ?>
				<p>
					<strong><?php _e('Policy Start Date:', 'stern_taxi_fare'); ?></strong><?php echo $ins_polstart_date_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_email_address_aj !="") : ?>
				<p>
					<strong><?php _e('Email Address:', 'stern_taxi_fare'); ?></strong><?php echo $ins_email_address_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_firstName_aj !="") : ?>
				<p>
					<strong><?php _e('First Name:', 'stern_taxi_fare'); ?></strong><?php echo $ins_firstName_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_lastname_aj !="") : ?>
				<p>
					<strong><?php _e('Last Name:', 'stern_taxi_fare'); ?></strong><?php echo $ins_lastname_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_dob_aj !="") : ?>
				<p>
					<strong><?php _e('Date of Birth:', 'stern_taxi_fare'); ?></strong><?php echo $ins_dob_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_nationality_aj !="") : ?>
				<p>
					<strong><?php _e('Nationality:', 'stern_taxi_fare'); ?></strong><?php echo $ins_nationality_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_contact_num_aj !="") : ?>
				<p>
					<strong><?php _e('Contact Number:', 'stern_taxi_fare'); ?></strong><?php echo $ins_contact_num_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_country_firstdl_aj !="") : ?>
				<p>
					<strong><?php _e('Country of Your First Driving Licence:', 'stern_taxi_fare'); ?></strong><?php echo $ins_country_firstdl_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_long_uaedrivinglicence_aj !="") : ?>
				<p>
					<strong><?php _e('Year Driving in UAE:', 'stern_taxi_fare'); ?></strong><?php echo $ins_long_uaedrivinglicence_aj; ?>
				</p>
			<?php endif; ?>

			<?php if($ins_years_dltotal_aj !="") : ?>
				<p>
					<strong><?php _e('Year Driving Total:', 'stern_taxi_fare'); ?></strong><?php echo $ins_years_dltotal_aj; ?>
				</p>
			<?php endif; ?>
							

			<?php if($ins_imported_aj !="") : ?>
				<p>
					<strong><?php _e('My Car Imported?:', 'stern_taxi_fare'); ?></strong><?php echo $ins_imported_aj; ?>
				</p>
			<?php endif; ?>
							

		<?php

		} else {


/* 			$distanceHtml = get_post_meta( $order_id , '_distanceHtml', true );
			$durationHtml = get_post_meta( $order_id , '_durationHtml', true );
			$estimated_fare = get_post_meta($order_id , '_estimated_fare', true );
			$cartypes = get_post_meta( $order_id , '_cartypes', true );
			$source = get_post_meta( $order_id , '_source', true );
			$destination = get_post_meta( $order_id , '_destination', true );
			$car_seats = get_post_meta( $order_id , '_car_seats', true );
			$dateTimePickUp = date(getFormatDateTime("php"),strtotime(get_post_meta( $order_id , '_local_pickup_time_select', true )));
			$dateTimePickUpRoundTrip = date(getFormatDateTime("php"),strtotime(get_post_meta( $order_id , '_dateTimePickUpRoundTrip', true )));
			$nbToll = get_post_meta( $order_id , '_nbToll', true );
			$numberOfPets = get_post_meta( $order_id , '_NumberOfPets', true );
			$suitcases = get_post_meta( $order_id , '_suitcases', true );
			$stern_taxi_fare_round_trip_text = (get_post_meta( $order_id , '_stern_taxi_fare_round_trip_text', true )=="true") ? __('Round Trip', 'stern_taxi_fare') : __('One way', 'stern_taxi_fare') ;
 */




			?>


			<?php //new qrCode($order_id); ?>
			<h2><?php _e('Selected Company Details', 'stern_taxi_fare' ); ?></h2>

			<div class="row">

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Company Name:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_compname_sel; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Price:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_price_sel; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Support Modified:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_gcc_spec_sel; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Vehicle Category:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_vehicle_cat_sel; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Repair Condition:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_repair_cond_sel; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Driving license less than 1 Year:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_dl_less_than_year_sel; ?>">
				</div>
				
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Age less than 25 year:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_less_than_year_sel; ?>">
				</div>
				
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Out Side UAE Coverage:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_os_coverage_sel; ?>">
				</div>
				
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Personal Accident (Driver):', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_pa_driver_sel; ?>">
				</div>
				
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Personal Accident Per Passengers:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_papp_sel; ?>">
				</div>
				
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Road Side Assistance:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_rs_assistance_sel; ?>">
				</div>
				
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Rant a Car:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_rent_car_sel; ?>">
				</div>
				
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Excess Amount:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_excess_amount_sel; ?>">
				</div>
			</div>
			<br/>
			
			<h2><?php _e('User Details', 'stern_taxi_fare' ); ?></h2>
			<div class="row">

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Registration Date:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_reg_date_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Policy Start Date :', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_polstart_date_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Email Address:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_email_address_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('First name:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_firstName_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Last name:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_lastname_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Date of Birth:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_dob_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Nationality:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_nationality_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Contact number:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_contact_num_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Country of Your First Driving Licence:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_country_firstdl_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Year Driving in UAE:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_long_uaedrivinglicence_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('Year Driving in Total:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_years_dltotal_aj; ?>">
				</div>

				<div class="col-xs-12 col-sm-6 col-lg-4">
					<label><?php _e('My Car Imported:', 'stern_taxi_fare'); ?></label><input type="text" class="input-text" readonly value="<?php echo $ins_imported_aj; ?>">
				</div>
				
			</div>	
			
			
			<?php
		}
	}
}
