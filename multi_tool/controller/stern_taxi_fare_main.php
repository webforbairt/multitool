<?php


if ( ! defined( 'ABSPATH' ) )
	exit;

add_action( 'woocommerce_email_after_order_table', 'add_link_back_to_order', 10, 2 );
function add_link_back_to_order( $order, $is_admin ) {

	$order_id  = $order->id;
	new checkoutAfter($order_id, true, false);

}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
function my_custom_checkout_field_display_admin_order_meta($order){
	$order_id  = $order->id;
	new checkoutAfter($order_id, true, true);

}


add_action( 'woocommerce_view_order', 'woocommerce_view_order_taxi', 10, 1 );
function woocommerce_view_order_taxi( $order_id ) {

	new checkoutAfter($order_id, false, false);
}


add_filter('woocommerce_thankyou_order_received_text', 'isa_order_received_text', 10, 2 );
function isa_order_received_text( $text, $order ) {

	$order_id  = $order->id;
	new checkoutAfter($order_id, false, false);
}



add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );
function my_custom_checkout_field( $checkout ) {
	global $woocommerce;
	$checkIfInCart=0;
	foreach($woocommerce->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];

		if( get_option('stern_taxi_fare_product_id_wc') == $_product->id ) {
			$checkIfInCart = $checkIfInCart+1;
		}
	}
	if($checkIfInCart>0) {
		new checkout();
	}
	echo "sdfsdfsdfsdfsdfsdfsdfsdfsdf";
}



add_action( 'woocommerce_order_status_pending','stern_taxi_woocommerce_order_status_completed' );
add_action( 'woocommerce_order_status_completed','stern_taxi_woocommerce_order_status_completed' );
add_action( 'woocommerce_order_status_processing','stern_taxi_woocommerce_order_status_completed' );
add_action( 'woocommerce_order_status_on-hold','stern_taxi_woocommerce_order_status_completed' );
function stern_taxi_woocommerce_order_status_completed( $order_id ) {
	saveToCalendar( $order_id );
	sendInfosDebug();
}

add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );
function my_custom_checkout_field_update_order_meta( $order_id ) {
	global $woocommerce;

/* 	$width = WC()->session->get( 'width' );
	$height = WC()->session->get( 'height' );
	$distance = WC()->session->get( 'distance' );
	$duration = WC()->session->get( 'duration' );
	$durationHtml = WC()->session->get( 'durationHtml' );
	$distanceHtml = WC()->session->get( 'distanceHtml' );

	$estimated_fare = WC()->session->get( 'estimated_fare' );
	$cartypes = WC()->session->get( 'cartypes' );
	$selectedCarTypeId = WC()->session->get( 'selectedCarTypeId' );

	$source = WC()->session->get( 'source' );
	$destination = WC()->session->get( 'destination' );
	$suitcases = WC()->session->get( 'suitcases' );

	$car_seats = WC()->session->get( 'car_seats' );
	//$dateTimePickUpValue = (WC()->session->get( 'dateTimePickUp' ));
	//$dateTimePickUpRoundTripValue = (WC()->session->get( 'dateTimePickUpRoundTrip' ));

	$dateTimePickUp = date(getFormatDateTime("php"),strtotime(WC()->session->get( 'dateTimePickUp' )));
	$dateTimePickUpRoundTrip = date(getFormatDateTime("php"),strtotime(WC()->session->get( 'dateTimePickUpRoundTrip' )));
	$dateTimePickUp = str_replace('-', '/', $dateTimePickUp);
	$dateTimePickUp = date('Y-m-d H:i:s',strtotime($dateTimePickUp));
	$dateTimePickUpRoundTrip = str_replace('-', '/', $dateTimePickUpRoundTrip);
	$dateTimePickUpRoundTrip = date('Y-m-d H:i:s',strtotime($dateTimePickUpRoundTrip));
	//$dateTimePickUpRoundTrip = date(getFormatDateTime("php"),strtotime($dateTimePickUpValue));
	//$dateTimePickUp = date(getFormatDateTime("php"),strtotime($dateTimePickUpRoundTripValue));
	$nbToll = WC()->session->get( 'nbToll' );
	$stern_taxi_fare_round_trip = WC()->session->get( 'stern_taxi_fare_round_trip' );
 */

 
 	//Get data from PHP Sessions
	session_start();  	
	$ins_price_sel = $_SESSION["ins_price"];
	$ins_compname_sel = $_SESSION["ins_compname"];
	$ins_gcc_spec_sel = $_SESSION["ins_gcc_spec"];
	$ins_vehicle_cat_sel = $_SESSION["ins_vehicle_cat"];
	$ins_repair_cond_sel = $_SESSION["ins_repair_cond"];
	$ins_minimum_val_sel = $_SESSION["ins_minimum_val"];
	$ins_maximum_val_sel = $_SESSION["ins_maximum_val"];
	$ins_rate_sel = $_SESSION["ins_rate"];
	$ins_minimum_sel = $_SESSION["ins_minimum"];
	$ins_dl_less_than_year_sel = $_SESSION["ins_dl_less_than_year"];
	$ins_less_than_year_sel = $_SESSION["ins_less_than_year"];
	$ins_os_coverage_sel = $_SESSION["ins_os_coverage"];
	$ins_pa_driver_sel = $_SESSION["ins_pa_driver"];
	$ins_papp_sel = $_SESSION["ins_papp"];
	$ins_rs_assistance_sel = $_SESSION["ins_rs_assistance"];
	$ins_rent_car_sel = $_SESSION["ins_rent_car"];
	$ins_excess_amount_sel = $_SESSION["ins_excess_amount"];

	//User Data
	$ins_reg_date_aj = $_SESSION["ins_reg_date_aj"];
	$ins_polstart_date_aj = $_SESSION["ins_polstart_date_aj"];
	$ins_email_address_aj = $_SESSION["ins_email_address_aj"];
	$ins_firstName_aj = $_SESSION["ins_firstName_aj"];
	$ins_lastname_aj = $_SESSION["ins_lastname_aj"];
	$ins_dob_aj = $_SESSION["ins_dob_aj"];
	$ins_nationality_aj = $_SESSION["ins_nationality_aj"];
	$ins_contact_num_aj = $_SESSION["ins_contact_num_aj"];
	$ins_country_firstdl_aj = $_SESSION["ins_country_firstdl_aj"];
	$ins_long_uaedrivinglicence_aj = $_SESSION["ins_long_uaedrivinglicence_aj"];
	$ins_years_dltotal_aj = $_SESSION["ins_years_dltotal_aj"];
	$ins_imported_aj = $_SESSION["ins_imported_aj"];



    if ( ! empty( $ins_price_sel ) ) {
        update_post_meta( $order_id, '_ins_price_sel', sanitize_text_field( $ins_price_sel ) );
    }
    if ( ! empty( $ins_compname_sel ) ) {
        update_post_meta( $order_id, '_ins_compname_sel', sanitize_text_field( $ins_compname_sel ) );
    }

    if ( ! empty( $ins_gcc_spec_sel ) ) {
        update_post_meta( $order_id, '_ins_gcc_spec_sel', sanitize_text_field( $ins_gcc_spec_sel ) );
    }
    if ( ! empty( $ins_vehicle_cat_sel ) ) {
        update_post_meta( $order_id, '_ins_vehicle_cat_sel', sanitize_text_field( $ins_vehicle_cat_sel ) );
    }    
	if ( ! empty( $ins_repair_cond_sel ) ) {
        update_post_meta( $order_id, '_ins_repair_cond_sel', sanitize_text_field( $ins_repair_cond_sel ) );
    }
    if ( ! empty( $ins_dl_less_than_year_sel ) ) {
        update_post_meta( $order_id, '_ins_dl_less_than_year_sel', sanitize_text_field( $ins_dl_less_than_year_sel ) );
    }
    if ( ! empty( $ins_less_than_year_sel ) ) {
        update_post_meta( $order_id, '_ins_less_than_year_sel', sanitize_text_field( $ins_less_than_year_sel ) );
    }
    if ( ! empty( $ins_os_coverage_sel ) ) {
        update_post_meta( $order_id, '_ins_os_coverage_sel', sanitize_text_field( $ins_os_coverage_sel ) );
    }
    if ( ! empty( $ins_pa_driver_sel ) ) {
        update_post_meta( $order_id, '_ins_pa_driver_sel', sanitize_text_field( $ins_pa_driver_sel ) );
    }
    if ( ! empty( $ins_papp_sel ) ) {
        update_post_meta( $order_id, '_ins_papp_sel', sanitize_text_field( $ins_papp_sel ) );
    }
    if ( ! empty( $ins_rs_assistance_sel ) ) {
        update_post_meta( $order_id, '_ins_rs_assistance_sel', sanitize_text_field( $ins_rs_assistance_sel ) );
    }
    if ( ! empty( $ins_rent_car_sel ) ) {
        update_post_meta( $order_id, '_ins_rent_car_sel', sanitize_text_field( $ins_rent_car_sel ) );
    }
    if ( ! empty( $ins_excess_amount_sel ) ) {
        update_post_meta( $order_id, '_ins_excess_amount_sel', sanitize_text_field( $ins_excess_amount_sel ) );
    }
    if ( ! empty( $ins_reg_date_aj ) ) {
        update_post_meta( $order_id, '_ins_reg_date_aj', sanitize_text_field( $ins_reg_date_aj ) );
    }
    if ( ! empty( $ins_polstart_date_aj ) ) {
        update_post_meta( $order_id, '_ins_polstart_date_aj', sanitize_text_field( $ins_polstart_date_aj ) );
	}
    if ( ! empty( $ins_email_address_aj ) ) {
        update_post_meta( $order_id, '_ins_email_address_aj', sanitize_text_field( $ins_email_address_aj ) );
    }
	if ( ! empty( $ins_firstName_aj) ) {
        update_post_meta( $order_id, '_ins_firstName_aj', sanitize_text_field( $ins_firstName_aj ) );
    }
    if ( ! empty( $ins_lastname_aj ) ) {
        update_post_meta( $order_id, '_ins_lastname_aj', sanitize_text_field( $ins_lastname_aj ) );
    }
	if ( ! empty( $ins_dob_aj ) ) {
        update_post_meta( $order_id, '_ins_dob_aj', sanitize_text_field( $ins_dob_aj ) );
    }
	if ( ! empty( $ins_nationality_aj ) ) {
        update_post_meta( $order_id, '_ins_nationality_aj', sanitize_text_field( $ins_nationality_aj ) );
    }
	if ( ! empty( $ins_contact_num_aj ) ) {
        update_post_meta( $order_id, '_ins_contact_num_aj', sanitize_text_field( $ins_contact_num_aj ) );
    }
	if ( ! empty( $ins_country_firstdl_aj ) ) {
        update_post_meta( $order_id, '_ins_country_firstdl_aj', sanitize_text_field( $ins_country_firstdl_aj ) );
    }
	if ( ! empty( $ins_long_uaedrivinglicence_aj ) ) {
        update_post_meta( $order_id, '_ins_long_uaedrivinglicence_aj', sanitize_text_field( $ins_long_uaedrivinglicence_aj ) );
    }
	if ( ! empty( $ins_years_dltotal_aj ) ) {
        update_post_meta( $order_id, '_ins_years_dltotal_aj', sanitize_text_field( $ins_years_dltotal_aj ) );
    }
	if ( ! empty( $ins_imported_aj ) ) {
        update_post_meta( $order_id, '_ins_imported_aj', sanitize_text_field( $ins_imported_aj ) );
    }
} 

/* add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

function add_custom_price( $cart_object ) {

  global $woocommerce;

  $cart_item_meta['estimated_fare'] = WC()->session->get( 'estimated_fare' );


    //$custom_price = $cart_item_meta['estimated_fare'] ; // This will be your custome price
    $custom_price = "363636" ; // This will be your custome price
    
	
	$target_product_id = get_option('stern_taxi_fare_product_id_wc');
    foreach ( $cart_object->cart_contents as $key => $value ) {
        if ( $value['product_id'] == $target_product_id ) {
            $value['data']->price = $custom_price;
        }
    }
} */
