<?php
/*
 Plugin Name: Multi tool Search System
 Description: This plugin use to select Search Multi tools of different brands.
 Version: 1.0.0
 Author: WpRight
*/
add_action( 'wp_ajax_nopriv_post_love_set_product_canvas', 'post_love_set_product_canvas' );

add_action( 'wp_ajax_post_love_set_product_canvas', 'post_love_set_product_canvas' );

add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_css');

add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_js');

add_action('admin_enqueue_scripts', 'stern_taxi_fares_script_back_js');

add_action('admin_enqueue_scripts', 'stern_taxi_fares_script_back_css');

add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);

add_action('init', 'stern_taxi_fare_register_shortcodes1');
	include 'controller/stern_taxi_fare_admin.php';
	include 'controller/functions.php';
	include 'templates/form/showResults.php';
	include 'templates/admin/settings.php';
function post_love_set_product_canvas(){
	
}
function stern_taxi_fare_register_shortcodes1() {
    add_shortcode('mt_features', 'stern_taxi_fare1');
}

function stern_taxi_fare1($atts) {
	ob_start();
	?>
<style>
.tabledata {
    width: 50%;
    float: left;
}
.tabledata {
    width: 48%;
    float: left;
    padding: 10px;
    border: 2px solid gray;
    margin: 5px 1%;
}
.tabledata p {
    font-size: 14px;
    margin: 2px 0px;
}
.tool_label {
    font-weight: bolder;
}
div#step2 {
    background: rgba(255, 255, 255, 0);
    padding: 0px 25px;
}
</style>
<div class="container1 ">
<div class="stern-taxi-fare">
<?php
?>
	<div id="steps_buttons" class="row" style="position:relative;">
		<div id="btn_step1" class="col col-lg-6">
			<button type="button" class="step_1btn" id="btn_step1">
			<span style="display:none;" id="step_count1">1</span><span>Search Multi-Tools</span></button>
		</div>
		<div id="btn_step2" class="col col-lg-6">
			<button type="button" class="step_1btn" id="btn_step2">
			<span style="display:none;" id="step_count1">2</span><span>Search Again</span</button>
		</div>
	</div>

<form  id="ins_mainform" method="post">

<div id="step1">
<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Brands</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage yearselect" id="mul_brand" name="vehicleyear" multiple="multiple">	
	<option class="gerber" value="Gerber" > Gerber </option>
	<option class="leatherman" value="Leatherman" > Leatherman </option>
	<option class="victorinox" value="Victorinox" > Victorinox </option>
	<option class="SOG" value="SOG" > SOG </option>
	</select>
</div>
</div>
</div>
<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Price Range</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage makeoption" id="mul_pricerange" name="vehiclemake">
<option class="options" value="0" selected>-- All --</option>
<option class="options" value="25" > less than $25 </option>
<option class="options" value="50" > less than $50 </option>
<option class="options" value="75" > less than $75 </option>
<option class="options" value="100" > less than $100 </option>
<option class="options" value="101" > more than $100 </option>
</select>
</div>
</div>
</div>

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Weight</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage modeloption" id="mul_weight" name="mul_weight">
<option class="options" value="0" selected>-- All --</option>
<option class="options" value="3" > less than 3oz / 85gm </option>
<option class="options" value="6" > less than 6oz / 171gm </option>
<option class="options" value="10" > less than 10oz / 282gm </option>
<option class="options" value="10.1" > more than 10oz / 283gm </option>
</select>
</div>
</div>
</div>

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Closed Length</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage modeloption" id="closed_lengthins" name="closed_lengthins">
<option class="options" value="0" selected>-- All --</option>
<option class="options" value="3" > less than 3in / 7.5cm </option>
<option class="options" value="4" > less than 4in / 10.0cm </option>
<option class="options" value="5" > less than 5in / 12.5cm </option>
<option class="options" value="5.1" > more than 5in / 12.5cm </option>
</select>
</div>
</div>
</div>

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Outside Accessible Tools</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage modeloption" id="mul_accessible_tools" name="mul_accessible_tools">
<option class="options" value="0" selected>-- All --</option>
<option class="options" value="Yes" > Yes </option>
<option class="options" value="No" > No </option>
</select>
</div>
</div>
</div>

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Pliers</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage modeloption" id="mul_pliers" name="mul_pliers">
<option class="options" value="0" selected>-- All --</option>
<option class="options" value="yes" > Yes </option>
<option class="options" value="no" > No </option>
</select>
</div>
</div>
</div>

<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Knife Blade</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage modeloption" id="mul_knifeblades" name="mul_knifeblades">
<option class="options" value="0" selected>-- All --</option>
<option class="options" value="Straight" > Straight </option>
<option class="options" value="Serrated" > Serrated </option>
<option class="options" value="Combo" > Combo </option>
<option class="options" value="No" > None </option>
</select>
</div>
</div>
</div>
<div class="row form_row-space">
<div class="col col-lg-6"><p class="step1_labels">Scissors</p></div>
<div class="col col-lg-5"><div class="form-group">
<select class="form-control calculate percentage modeloption" id="mul_scissors" name="mul_pliers">
<option class="options" value="0" selected>-- All --</option>
<option class="options" value="yes" > Yes </option>
<option class="options" value="no" > No </option>
</select>
</div>
</div>
</div>

</form>

<!-- Show data -->
<div id="show_data" class="row">
<div id="showdata_parent" class="col col-lg-12">
<div type="button" class="step_next1btn step_results" id="show_results"><span>Show Results</span></div>
</div>
</div>


<!-- Next Step Button -->
<div id="btn_next_step1" class="row">
<div id="btn_nextstep1" class="hide col col-lg-12">
<div type="button" class="step_next1btn" id="btn_nextstep1inner"><span>Next</span></div>
</div>
</div>
</div>  <!-- div step1 close -->


		<div id="step2" style="display:none;">
			<div class="data-head">
				<h3>Multi-tools that match your search</h3>					
			</div>

			<div class="maintablebody">
			</div>				
		

		<table id="datatable" class="finalTable" >	
		</table>
		
		<!-- Search Again Button -->
		<div id="search_again" class="row">
		<div id="btn_search_again" class="col col-lg-12">
		<div type="button" class="step_next1btn search_again_btn" id="search_again_btn"><span>Search Again</span></div>
		</div>
		</div>

		
		<div id="looader" class="" style="display: none;background: rgba(0, 0, 0, 0.8);height: 100%;position: absolute; z-index: 9999;width: 100%;">
		<img src="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif" style="
		    position: absolute;width: 10%;left: 45%; top: 10%; right: 45%;
		">
		</div>

			<textarea id="color_data" style="display:none;"></textarea>
			<div id="btn_next_step1" class="row">
			<div id="btn_nextstep1" class="hide col col-lg-12">
			<div type="button" class="step_next1btn" id="btn_nextstep1inner2"><span>Next</span></div>
			</div>
			</div>
		</div>
		
		
		 <!-- Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
			
			  <!-- Modal content-->
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" id="model_close_icon" class="close" data-dismiss="modal">&times;</button>
				  <h4 style="color:red;" class="modal-title">Alert</h4>
				</div>
				<div class="modal-body">
				  <p>Sorry MacGyver, no multi-tools match your search. Try not there search!.</p>
				</div>
				<div class="modal-footer">
				  <button type="button" id="model_close" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			  </div>
			  
			</div>
		  </div>
		 <!-- Model End -->
		
		<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>


		<!-- get data csv -->
		<?php  
		$form_dataComp = get_option( 'frontend_dataComp' );
		$json_form_dataComp = json_encode($form_dataComp); 
		?>

<script>
jQuery.noConflict();
jQuery(document).ready(function() {
		localStorage.setItem("selected_brand","");	
		jQuery('#mul_brand').multiselect({
			// ...
			onChange: function() {
				var selected_brand = this.$select.val();
				console.log(selected_brand);
				localStorage.setItem("selected_brand",selected_brand);
				//setTable();
			// ...
			},
			nonSelectedText:'All',
			allSelectedText: '--- Select All ---',
			includeSelectAllOption: true,
			buttonWidth: '100%',
			maxHeight: 200,
			includeSelectAllOption: true
			});
	

		var form1 = jQuery( "#ins_mainform" );
			var result = [];
			var data_tt = [];
		var tableControl = document.getElementById('show_formdata_comp');
		//jQuery('#btn_step1').click(false);

		jQuery("#btn_step1").addClass("active_step");
		//jQuery("#btn_step1").click(false);
		jQuery("#btn_step1").click(function(){
		jQuery("#btn_step2").removeClass("active_step");
		jQuery("#btn_step1").addClass("active_step");
		jQuery( "#step1" ).show();
		//jQuery( "#step2" ).hide();
		});		
		jQuery("#btn_step2").click(function(){
			jQuery("#btn_step2").removeClass("active_step");
			jQuery("#btn_step1").addClass("active_step");
			jQuery( "#step1" ).show();
			jQuery( "#step2" ).hide();
		});		
		jQuery("#btn_nextstep2inner").click(function(){
			jQuery("#btn_step1").removeClass("active_step");
			jQuery("#btn_step2").removeClass("active_step");
		});


var basePrice = 3.00;
var width;

//jQuery( "#ins_submodel" ).hide();
jQuery( "#step2" ).hide();
jQuery( "#step3" ).hide();
jQuery( "#step4" ).hide();
jQuery("#calCheckout_url").hide();

jQuery( "#model_close" ).click(function() {
	jQuery('.fade.in').css('opacity', '0');
	jQuery('#myModal').css('display', 'none');
});
jQuery( "#model_close_icon" ).click(function() {
	jQuery('.fade.in').css('opacity', '0');
	jQuery('#myModal').css('display', 'none');
});

jQuery( "#btn_step2" ).click(function() {
	jQuery( "#step2" ).show();
});	
jQuery( "#search_again_btn" ).click(function() {
	jQuery( "#step1" ).show();
	jQuery( "#step2" ).hide();
});	
 jQuery("#show_results").click(function () {
setTable();
});

function get_filtered_by_brands(filtered_arr){
	//var step_brands = <?php echo $json_form_dataComp; ?>;
	var value_brands = [];
	jQuery.each( filtered_arr, function( i, value ) {
	var mul_brand = jQuery('#mul_brand option:selected').val();
	var mul_brands_arr = mul_brand.split(',');
	var rt_brand = value[0];
	if(typeof mul_brands_arr[0] == 'undefined'){
		mul_brands_arr[0] = 0;
	}	
	if(typeof mul_brands_arr[1] == 'undefined'){
		mul_brands_arr[1] = 0;
	}	
	if(typeof mul_brands_arr[2] == 'undefined'){
		mul_brands_arr[2] = 0;
	}	
	if(typeof mul_brands_arr[3] == 'undefined'){
		mul_brands_arr[3] = 0;
	}
	if(rt_brand == mul_brands_arr[0] || rt_brand == mul_brands_arr[1] || rt_brand == mul_brands_arr[2] || rt_brand == mul_brands_arr[3]){
		value_brands.push(value);
	}
	});
	return value_brands;
}

function get_filtered_by_range(filtered_arr){
	var mul_range = jQuery('#mul_pricerange option:selected').val();
	var mul_range_parse = parseFloat(mul_range);
	var value_range = [];
	jQuery.each( filtered_arr, function( i, value ) {
		var prange_upper = value[16];
		prange_upper_parse = parseFloat(prange_upper);
		if( mul_range_parse != 101){			
			if(prange_upper_parse <= mul_range_parse){
				value_range.push(value);
			}
		}else if(mul_range_parse == 101){
			if(prange_upper_parse >= mul_range_parse){
				value_range.push(value);
			}			
		}
	});
	return value_range;
}

function get_filtered_by_weight(filtered_arr){
	var mul_weight_unparsed = jQuery('#mul_weight option:selected').val();
	mul_weight = parseFloat(mul_weight_unparsed);
	var value_weight = [];
	jQuery.each( filtered_arr, function( i, value ) {
		var rt_weight_unparsed = value[3];
		var rt_weight = parseFloat(rt_weight_unparsed);
		if( mul_weight != 10.1){			
			if(rt_weight <= mul_weight){
				value_weight.push(value);
			}
		}else if(mul_weight == 10.1){
			if(rt_weight >= mul_weight){
				value_weight.push(value);
			}			
		}
	});
	return value_weight;
}

function get_filtered_by_length(filtered_arr){
	var mul_length_unparsed = jQuery('#closed_lengthins option:selected').val();
	var mul_length = parseFloat(mul_length_unparsed);
	var value_length = [];
	jQuery.each( filtered_arr, function( i, value ) {
		var rt_length_unparsed = value[5];
		var rt_length = parseFloat(rt_length_unparsed);
		if( mul_weight != 5.1){			
			if(rt_length <= mul_length){
				value_length.push(value);
			}
		}else if(rt_length == 5.1){
			if(rt_length >= mul_length){
				value_length.push(value);
			}			
		}
	});
	return value_length;
}

function get_filtered_by_accessible_tools(filtered_arr){
	var mul_atoolsu = jQuery('#mul_accessible_tools option:selected').val();
	var mul_atools = mul_atoolsu.toLowerCase();
	var value_atools = [];
	jQuery.each( filtered_arr, function( i, value ) {
		var rt_atoolsu = value[7];
		var rt_atools = rt_atoolsu.toLowerCase();
			if(rt_atools == mul_atools){
				value_atools.push(value);
			}
		});
	return value_atools;
}

function get_filtered_by_pliers(filtered_arr){
	var mul_pliersu = jQuery('#mul_pliers option:selected').val();
	var mul_pliers = mul_pliersu.toLowerCase();
	var value_pliers = [];
	jQuery.each( filtered_arr, function( i, value ) {
		var rt_pliersu = value[8];
		var rt_pliers = rt_pliersu.toLowerCase();
			if(rt_pliers == mul_pliers){
				value_pliers.push(value);
			}
		});
	return value_pliers;
}

function get_filtered_by_knife(filtered_arr){
	var mul_knifeblades_u = jQuery('#mul_knifeblades option:selected').val();
	var mul_knifeblades = mul_knifeblades_u.toLowerCase();
	var value_knife = [];
	jQuery.each( filtered_arr, function( i, value ) {
	var rt_knifeblades_u = value[9];
	var rt_knifeblades = rt_knifeblades_u.toLowerCase();
	var knife_check = rt_knifeblades.search(mul_knifeblades);
		if(knife_check >= 0){
			value_knife.push(value);
		}
	});
	return value_knife;
}
	
function get_filtered_by_scissors(filtered_arr){
	var mul_scissorsu = jQuery('#mul_scissors option:selected').val();
	var mul_scissors = mul_scissorsu.toLowerCase();
	var value_scissors = [];
	jQuery.each( filtered_arr, function( i, value ) {
		var rt_scissorsu = value[10];
		var rt_scissors = rt_scissorsu.toLowerCase();
			if(rt_scissors == mul_scissors){
				value_scissors.push(value);
			}
		});
	return value_scissors;
}

function setTable(){
	// temp
	//var mul_brand = localStorage.getItem("selected_brand");
	var full_data_arr = <?php echo $json_form_dataComp; ?>;
	var mul_brand = jQuery('#mul_brand option:selected').val();
	var mul_range = jQuery('#mul_pricerange option:selected').val();
	var mul_weight_unparsed = jQuery('#mul_weight option:selected').val();
	var mul_length_unparsed = jQuery('#closed_lengthins option:selected').val();
	var mul_atoolsu = jQuery('#mul_accessible_tools option:selected').val();
	var mul_pliersu = jQuery('#mul_pliers option:selected').val();
	var mul_knifeblades_u = jQuery('#mul_knifeblades option:selected').val();
	var mul_scissorsu = jQuery('#mul_scissors option:selected').val();
	
	var mul_atools = mul_atoolsu.toLowerCase();
	var mul_pliers = mul_pliersu.toLowerCase();
	var mul_knifeblades = mul_knifeblades_u.toLowerCase();
	var mul_scissors = mul_scissorsu.toLowerCase();
	/* var rt_brand = value[0];
	var rt_range = value[2];
	var rt_weight_unparsed = value[3];
	var value4 = value[3];
	var value5 = value[4];
	var rt_length_unparsed = value[5];
	var value7 = value[6];
	var rt_atoolsu = value[7];
	var rt_pliersu = value[8];
	var rt_knifeblades_u = value[9];
	var rt_scissorsu = value[10];
	
	var prange_lower = value[15];
	var prange_upper = value[16]; */
	
/* 	var rt_atools = rt_atoolsu.toLowerCase();
	var rt_pliers = rt_pliersu.toLowerCase();
	var rt_scissors = rt_scissorsu.toLowerCase();
	var rt_knifeblades = rt_knifeblades_u.toLowerCase(); */
	mul_weight = parseFloat(mul_weight_unparsed);
	mul_length = parseFloat(mul_length_unparsed);
	var valuetable = full_data_arr;
	if(mul_brand != 0){
		valuetable = get_filtered_by_brands(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	if(mul_range != 0){
		valuetable = get_filtered_by_range(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	if(mul_weight != 0){
		valuetable = get_filtered_by_weight(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	if(mul_length != 0){
		valuetable = get_filtered_by_length(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	if(mul_atools != 0){
		valuetable = get_filtered_by_accessible_tools(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	if(mul_pliers != 0){
		valuetable = get_filtered_by_pliers(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	if(mul_knifeblades != 0){
		valuetable = get_filtered_by_knife(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	if(mul_scissors != 0){
		valuetable = get_filtered_by_scissors(valuetable);
	}else{
		//valuetable = full_data_arr;
	}
	//Set to Table
	valuetable_len = valuetable.length;
	console.log(valuetable_len)
	if(valuetable_len > 0){
	jQuery("#btn_step1").removeClass("active_step");
	jQuery("#btn_step2").addClass("active_step");
	jQuery('.maintablebody').html('');		
	jQuery("#step1").hide();
	jQuery("#step2").show();
	jQuery.each( valuetable, function( i, valuetableset1 ) {
		arraylength3 = valuetable.length - 1;
		brand = valuetableset1[0];
		model_name = valuetableset1[1];
		price_range = valuetableset1[2];
		weight_ons = valuetableset1[3];
		weight_grm = valuetableset1[4];
		weight = weight_ons+' oz/'+weight_grm+' gm';
		lenght_inch = valuetableset1[5];
		lenght_cm = valuetableset1[6];
		lenght = lenght_inch+' inch/'+lenght_cm+' cm';
		acc_tools = valuetableset1[7];
		pliers = valuetableset1[8];
		knife_blade = valuetableset1[9];
		scissors = valuetableset1[10];
		secondary_tools = valuetableset1[11];
		functions = valuetableset1[12];
		finish =valuetableset1[13];
		silver =valuetableset1[14];
		tool_img =valuetableset1[17];
		var finish;
		finish =valuetableset1[13]
		////console.log(secondary_tools);
		if(i <= arraylength3){
			jQuery('.maintablebody').append('<div class="tabledata" value=""><p><b style="font-size: 17px;">'+brand+' '+model_name+'</b></p><p><span class="tool_label">Price Range: </span>'+price_range+'</p><p><span class="tool_label">Weight: </span>'+weight+'</p><p><span class="tool_label">Closed Length</span> - '+lenght+'</p><p><span class="tool_label">Outside Accessible Tools: </span>'+acc_tools+'</p><p><span class="tool_label">Pliers: </span>'+pliers+'</p><p><span class="tool_label">Knife Blade: </span>'+knife_blade+'</p><p><span class="tool_label">Scissors: </span>'+scissors+'</p><p><span class="tool_label">Secondary Tools: </span>'+secondary_tools+'</p><p><span class="tool_label">Functions: </span>'+functions+'</p><p><span class="tool_label">Finish: </span>'+finish+'</p><p><img style="width:220px;" src="'+tool_img+'"></p></div>');	    
		}
	});

	var td = jQuery("#show_formdata_comp td");
	td.each(function(i){
		if (i % 3 == 0) {
			td.slice(i, i+3).wrapAll('<tr/>');
		}
	}).parent('tr').unwrap();

	jQuery("#show_formdata_comp tr").append("<td id='select_color'><input type='radio' class='tableradio' id='tableradio' name='tableradio' required><label for='tableradio'>tableradio</label></td>");	
	}else{
		//alert('Sorry MacGyver, no multi-tools match your search. Try not there search!');
		jQuery('#myModal').css('display', 'block');		
		jQuery( "#myModal" ).addClass( "in" );
		jQuery('.fade.in').css('opacity', '1');		
	}	
}
});
</script>		
<style>			
.multiselect-container{position:absolute;list-style-type:none;margin:0;padding:0}.multiselect-container .input-group{margin:5px}.multiselect-container>li{padding:0}.multiselect-container>li>a.multiselect-all label{font-weight:700}.multiselect-container>li.multiselect-group label{margin:0;padding:3px 20px 3px 20px;height:100%;font-weight:700}.multiselect-container>li.multiselect-group-clickable label{cursor:pointer}.multiselect-container>li>a{padding:0}.multiselect-container>li>a>label{margin:0;height:100%;cursor:pointer;font-weight:400;padding:3px 20px 3px 40px}.multiselect-container>li>a>label.radio,.multiselect-container>li>a>label.checkbox{margin:0}.multiselect-container>li>a>label>input[type=checkbox]{margin-bottom:5px}.btn-group>.btn-group:nth-child(2)>.multiselect.btn{border-top-left-radius:4px;border-bottom-left-radius:4px}.form-inline .multiselect-container label.checkbox,.form-inline .multiselect-container label.radio{padding:3px 20px 3px 40px}.form-inline .multiselect-container li a label.checkbox input[type=checkbox],.form-inline .multiselect-container li a label.radio input[type=radio]{margin-left:-20px;margin-right:0}
.multiselect-container {
        width: 100% !important;
    }
	button.multiselect.dropdown-toggle.btn.btn-default {
    border: 1px solid #39b54a!important;
    color: #39b54a!important;
	text-align: left;
}
.modal-dialog {
    transform: translate(0%, 50%) !important;
}

</style>
<?php
	return ob_get_clean();
}

function stern_taxi_fares_script_back_css() {
		wp_register_style( 'stern_jquery_ui_css', plugins_url('css/jquery-ui.css', __FILE__ ));
		wp_enqueue_style( 'stern_jquery_ui_css');
		wp_register_style( 'stern_fullCalendar_MIN_css', plugins_url('css/fullcalendar.min.css', __FILE__ ));
		wp_enqueue_style( 'stern_fullCalendar_MIN_css');
}



function stern_taxi_fares_script_front_css() {

		/* CSS */
		wp_register_style('stern_bootstrapValidatorMinCSS', plugins_url('css/bootstrapValidator.min.css',__FILE__));
        wp_enqueue_style('stern_bootstrapValidatorMinCSS');

		wp_register_style('stern_bootstrapbootstrap_selectMIN', plugins_url('css/bootstrap-select.min.css',__FILE__));
        wp_enqueue_style('stern_bootstrapbootstrap_selectMIN');

		wp_register_style('stern_bootstrapbootstrap_select', plugins_url('css/bootstrap-select.css',__FILE__));
        wp_enqueue_style('stern_bootstrapbootstrap_select');

		wp_register_style( 'stern_fullCalendar_MIN_css', plugins_url('css/fullcalendar.min.css', __FILE__ ));
		wp_enqueue_style( 'stern_fullCalendar_MIN_css');



		//if(get_option('stern_taxi_fare_lib_bootstrap_css')!="false"){

			wp_register_style( 'stern-bootstrap', plugins_url('bootstrap/css/bootstrap.css', __FILE__ ));
			wp_enqueue_style( 'stern-bootstrap');
		//}

		wp_register_style( 'stern_jquery_ui_css', plugins_url('css/jquery-ui.css', __FILE__ ));
		wp_enqueue_style( 'stern_jquery_ui_css');

		wp_register_style( 'stern_appendGrid_CSS', plugins_url('css/jquery.appendGrid-1.6.1.css', __FILE__ ));
		wp_enqueue_style( 'stern_appendGrid_CSS');


        wp_register_style('stern_taxi_fare_datetimepicker', plugins_url('css/bootstrap-datetimepicker.css',__FILE__));

        wp_enqueue_style('stern_taxi_fare_datetimepicker');





        wp_register_style('ins_style', plugins_url('css/ins_style.css',__FILE__));

        wp_enqueue_style('ins_style');
		wp_register_style('tabel_style', plugins_url('css/tabel_style.css',__FILE__));

        wp_enqueue_style('tabel_style');

		wp_enqueue_script( 'love', '');	
		wp_localize_script( 'love', 'postlove', array('ajax_url' => admin_url( 'admin-ajax.php' )));

}

function stern_taxi_fares_script_back_js() {
	
	wp_register_script('stern_jquery_ui', plugins_url('js/jquery-ui.js', __FILE__ ),array('jquery'));

	wp_enqueue_script('stern_jquery_ui');
}

function stern_taxi_fares_script_front_js() {


		wp_register_script('stern_taxi_fare_fullcalendar_front_js', plugins_url('js/stern_taxi_fare_fullcalendar_front.js', __FILE__ ),array('jquery'));
		wp_enqueue_script('stern_taxi_fare_fullcalendar_front_js');

		wp_register_script('stern_taxi_fare_multi_select_js', plugins_url('js/bootstrap-multiselect.js', __FILE__ ),array('jquery'));
		wp_enqueue_script('stern_taxi_fare_multi_select_js');

		wp_register_script('bootstrap_js', plugins_url('bootstrap/js/bootstrap.min.js', __FILE__ ),array('jquery'));
		wp_enqueue_script('bootstrap_js');

		
		
}
